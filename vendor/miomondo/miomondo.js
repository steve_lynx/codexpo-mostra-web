/*
Variante con icone:
var myIcon = L.icon({
    iconUrl: 'my-icon.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    shadowUrl: 'my-icon-shadow.png',
    shadowSize: [68, 95],
    shadowAnchor: [22, 94]
});

L.marker([50.505, 30.57], {icon: myIcon}).addTo(map);
*/


function drawDataWithIcons(JSONdata){

    const geodata = JSONdata; 
    geodata.places.forEach(marker => {	    
      MMicon = L.icon({
      iconUrl: 'img/miomondo.png',
      iconSize: [38, 95],
      iconAnchor: [22, 94],
      popupAnchor: [-3, -76],
      //  shadowUrl: 'img/miomondo-shadow.png',
      //  shadowSize: [68*markersData[marker].VALUE, 95*markersData[marker].VALUE],
      // shadowSize: [68, 95],
      //shadowAnchor: [22, 94]
      });
      L.marker([marker.lat,marker.lon], {icon: MMicon}).addTo(map)
      .bindPopup('<span id=\"popup\"><img id=\"popupimg\" src=\"'+marker.img+'\"><br /><a href=\"'+marker.name+'\">'+marker.name+'</a><p>'+marker.text+'</p></span>');    
    })
}


function drawData(JSONdata){
    
    const geodata = JSONdata; 
    geodata.places.forEach(marker => {	
      icon = L.circle([marker.lat,marker.lon], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.8,
      radius: 350
      }).addTo(map)
        .bindPopup('<span><img id=\"popupimg\" src=\"'+marker.img+'\"><br /><a href=\"'+marker.name+'\">'+marker.name+'</a><p>'+marker.text+'</p></span>');
   })
}


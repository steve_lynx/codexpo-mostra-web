<?php
/*
 * @name Router
 * @package codexpo
 * @author Stefano Penge
 * @copyright codexpo
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
/*

Routing :
instead of: 
	codexpo/index.php?room=Linguaggi&panel=Java
we would like to use:
	 codexpo/Linguaggi/Java

Other cases to be managed:
	codexpo/ -> codexpo/index.php
	codexpo/Linguaggi -> codexpo/index.php?room=Linguaggi

We also may want specify ouput format (but only for panels!):
	 codexpo/Linguaggi/Java/pdf
	 codexpo/Linguaggi/Java/html

In future versions, we could have API:	
	codexpo/Linguaggi/Java -> (a JSON with inside) the HTML of panel Java
	codexpo/Rooms/ -> a JSON with all rooms 
	codexpo/Linguaggi/panels -> a JSON with name of panels for room Linguaggi
	
Parameters go into $routes array:
codexpo =>
	$routes[room] = Ingresso
	$routes[panel] = empty
codexpo/Linguaggi =>
	$routes[room] = Linguaggi
	$routes[panel] = empty
codexpo/Linguaggi/Java =>
	$routes[room] = Linguaggi
	$routes[panel] = Java



The same for API


Caution: in this mode we cannot use relative paths for JS, CSS, image since the URL is not the real one

*/

namespace Tools;

class Router {
	
	private Array $route;
	private String $language;
	private Object $room;
	private String $roomTitle;
	private String $roomName;
	private Object $panel;
	private String $panelName;
	private $error;
	
	var String $index; 
	var Object $expo;

	function __construct($expo){

		$defaultRoom =  $GLOBALS['configManager']->getConfigValue('default_room');
		$defaultPanel = $GLOBALS['configManager']->getConfigValue('default_panel');
		$language = $GLOBALS['configManager']->getConfigValue('language');
		$API_default_format = $GLOBALS['configManager']->getConfigValue('api_default_format');
		$export_formats = $GLOBALS['configManager']->getConfigValue('export_formats');
		$default_export_format = $GLOBALS['configManager']->getConfigValue('default_export_format');
		if (isset($_SESSION['language'])) 
			$language = $_SESSION['language'];  // overriding main config value
		// Index: a JSON or an XML file 
		$indexFile =  $GLOBALS['configManager']->getConfigValue('index_file');    
    	$this->index = $indexFile;
    	$this->expo = new \Expo\Expo($indexFile,$language);
		$this->room = new \Expo\Room($this->expo->index);
		$this->panel = new \Expo\Panel($this->expo->index);
		
		if ($GLOBALS['configManager']->getConfigValue('routing')== TRUE){
			// routing active: 
			$base_url = Utils::getCurrentUri(); 
			if (($base_url<>"") && !(strstr($base_url,"index"))){
				$routes = array();
				$items = explode('/', $base_url);
				if ( $items[0] == 'API' && $items[1] == 'v1'){ //  API mode
					$routes['mode'] = 'API';	
					$routes['value'] = "";
					switch (count($items)) {   // API = 0, v1= 1
						case 1:
							$this->error = \Tools\Utils::translate("Percorso errato");
							break;
						case 2: 
							if (strtolower($items[1]) == 'v1') {
								// API/v1 = 3 implicitly
								// list of rooms
								$routes['room'] = '';
								$routes['panel'] = '';
								$routes['action'] = 'rooms';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;
								break;
							} else {
								$this->error = \Tools\Utils::translate("Versione errata");
							}
						case 3: 
							// API/v1/rooms: list of rooms
							if (strtolower($items[2]) == 'rooms') {
								$routes['room'] = '';
								$routes['panel'] = '';
								$routes['action'] = 'rooms';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;										
							} elseif (strtolower($items[2]) == 'randomroom') {
								// API/v1/randomroom: a random room + panel
								$routes['room'] = '';
								$routes['panel'] = '';
								$routes['action'] = 'randomroom';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;										
							} else { // API/v1/aRoom = 4 implicitly
								$roomsAr = $this->room->getAllRooms(); 
								if (in_array(ucfirst($items[2]),$roomsAr)){
									$routes['room'] = $items[2];
									$routes['action'] = 'panels';
									$routes['panel'] = '';
									$routes['parameter'] = 'format';
									$routes['format'] = $API_default_format;
								} else {
									$this->error = \Tools\Utils::translate("Metodo errato o parametri mancanti");
								}
							}	
							break;
						case 4: 
							// API/v1/aRoom/panels: list of panels for room 
							if (strtolower($items[3]) == 'panels'){
								$routes['room'] = $items[2];
								$routes['action'] = 'panels';
								$routes['panel'] = '';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;
							} elseif (strtolower($items[2]) == 'search'){
								// API/v1/search/room: search for a room or panel
								$routes['room'] = $items[3];
								$routes['panel'] = '';
								$routes['action'] = 'search';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;	
							} elseif (strtolower($items[2]) == 'find'){
								// API/v1/find/word: search for panel with 'word' inside text
								$routes['room'] =  $items[3];
								$routes['panel'] = '';
								$routes['action'] = 'find';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;	
							} else {
								// API/v1/aRoom/aPanel: content of panel  
								$panelsAr = $this->panel->getAllPanels($items[2]); 
								if (in_array(ucfirst($items[3]),$panelsAr)){
									$routes['room'] = $items[2];
									$routes['panel'] = $items[3];
									$routes['action'] = 'panel';
									$routes['parameter'] = 'format';
									$routes['format'] = $API_default_format;
								} else {	
									$this->error = \Tools\Utils::translate("Metodo errato o parametri errati");
								}							
							}	
							break;
						case 5:
						   // API/v1/aRoom/aPanel/panel: content of panel  
							if (strtolower($items[4]) == 'panel'){ 
								$routes['room'] = $items[2];
								$routes['panel'] = $items[3];
								$routes['action'] = 'panel';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;
							} elseif (strtolower($items[4]) == 'media'){ 
								$routes['room'] = $items[2];
								$routes['panel'] = $items[3];
								$routes['action'] = 'media';
								$routes['parameter'] = 'format';
								$routes['format'] = $API_default_format;
							} else {
								$this->error = \Tools\Utils::translate("Metodo errato o parametri errati");
							}
							break;
							
					} // end switch
					
				} else {  // web mode	
  					$routes['mode'] = 'web';
					$routes['format']  = 'html';
					$routes['parameter'] = '';
					$routes['action'] = '';
  					$routes['ouput_mode'] = $default_export_format;
					switch(count($items)) {
						case 0:
							$routes['room'] = $defaultRoom;
							$routes['panel'] = 	$defaultPanel;
							break;
						case 1: // /Linguaggi 
							$routes['room'] =  ucfirst($items[0]);  
							$routes['panel'] = '';	
							break;
						case 2: // /Linguaggi/Java or /Linguaggi/pdf
							if (in_array($items[1],$export_formats)){
								$routes['room'] = ucfirst($items[0]);  
								$routes['output_mode'] = $items[1];	
							} else {
								$routes['room'] = ucfirst($items[0]);  
								$routes['panel'] = $items[1];	
							}
							
							break;
						case 3: // /Linguaggi/Java/pdf 
							if (in_array($items[2],$export_formats)){
								$routes['room'] = ucfirst($items[0]);  
								$routes['panel'] = $items[1];
								$routes['output_mode'] = $items[2];	
							} else {
								$routes['room'] = ucfirst($items[0]);  
								$routes['panel'] = $items[1];
							}								
							break;	
						case 4: // /Linguaggi/Java/layout/alternate 
							if ($items[2] == 'layout'){
								$routes['room'] = ucfirst($items[0]);  
								$routes['panel'] = $items[1];
								$routes['layout'] = $items[3];	
							} else {
								$routes['room'] = ucfirst($items[0]);  
								$routes['panel'] = $items[1];
							}								
							break;		
						default:	
							$routes['room'] = $defaultRoom;	
							$routes['panel'] = $defaultPanel;	
					}

				}

				$this->route = $routes; 
				$this->roomName = $routes['room'];
				$this->roomId = $this->room->findRoomId($this->roomName);
				$this->panelName = $routes['panel'];
				$this->output_mode = $routes['ouput_mode'];
				$this->format = $routes['format'];
				$this->parameter = $routes['parameter'];
				$this->action = $routes['action'];
			 } else { // root/
				$routes['room'] =  $defaultRoom;  
				$routes['panel'] = $defaultPanel;	
				$routes['mode'] = 'web';	
				$this->roomName = $defaultRoom;
				$this->roomId = $this->room->findRoomId($defaultRoom); 
				$this->panelName = $defaultPanel;
			 } 	
	  } else {
	  	// routing not active: codexpo/index.php?room=Linguaggi&panel=Java
	  	// API not active!
			if (!isset($_GET['id'])) {
				if (isset($_GET['room'])){
					$this->roomName = $_GET['room']; 
					$this->roomId = $this->room->findRoomId($this->room); 
				} else {
					$this->roomName = $defaultRoom;
					$this->roomId  = $this->room->findRoomId($defaultRoom); 
				}	
			} else {
				$this->roomId  = $_GET['id'];
				$this->roomName  = $this->room->findRoomName($this->roomId);
			}
				
			if (isset($_GET['panel'])) {
				$this->panelName =  $_GET['panel'];
			} else {
				$this->panelName  =  $this->roomName;
			}
			$routes['room'] =  $this->roomName;  
			$routes['panel'] = $this->panelName;	
			$routes['mode'] = 'web';
		}
		if ($routes['mode'] != 'API'){	
			if ($this->roomName == $this->panelName){
				$this->roomTitle = \Tools\Utils::translate($this->roomName); // expanded title
			} else {
				$this->roomTitle = "<span>".\Tools\Utils::translate($this->roomName)."</span><br /><span class='panel-title'> ".str_replace('_',' ',$this->panelName)."</span>"; // expanded title
			}
		}
	} // __construct

	/*
	* Getters
	*
	*******/


	function getRoute(){
		if (isset($this->route))
			return $this->route;
		else
			return '';
	}

	function getRoom(){
		if (isset($this->roomName))
			return $this->roomName;
		else
			return '';
	}
	

	function getRoomId(){
		if (isset($this->roomId))
			return $this->roomId;
		else
			return '';
	}
	
	function getRoomTitle(){
		if (isset($this->roomTitle))
			return urldecode(($this->roomTitle));
		else
			return '';
	}


	function getPanel(){
		if (isset($this->panelName))
			return $this->panelName;
		else // we should always have a panel?
			return '';
	}

	
	function getParameter(){
		if (isset($this->route['parameter']))
			return $this->route['parameter'];
		else
			return '';
	}
	
	function getValue(){
		if (isset($this->route['parameter']))
			return $this->route['value'];
		else
			return '';
	}
	
	function getAction(){
		if (isset($this->route['action']))
			return $this->route['action'];
		else
			return '';
	}

	function getFormat(){
		if (isset($this->route['format']))
			return $this->route['format'];
		else
			return '';
	}
	
	function getLayout(){
		if (isset($this->route['layout']))
			return $this->route['layout'];
		else
			return '';
	}
	
	
	function getMode(){
		if (isset($this->route['mode']))
			return $this->route['mode'];
		else
			return '';
	}

	function getOutputMode(){
		if (isset($this->route['output_mode']))
			return $this->route['output_mode'];
		else
			return '';
	}
	
	function getError(){
		return $this->error;
	}

} // end class Router

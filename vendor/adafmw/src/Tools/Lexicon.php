<?php

/*
*
* @package  ParolaPA
* @version 1.0.0
* @author Stefano Penge <steve@@lynxlab.com>
* @copyright 2020 Stefano Penge
* @license GPL 3.0
*
*/


namespace Tools;

/* Constants: */
define ("INDEX_MIN_CHARS", 6);
define ('MANREPL',TRUE);
define ('STRPTG',TRUE); //FALSE
define ('HTMLSPC',TRUE); //FALSE
define ('PREGCLNCHRS',FALSE); //FALSE
define ('CLNCHRS',TRUE); //FALSE
define ('MINLENGHTWORD',4); //4



class Lexicon {

var $excl_words;
var $cache_file_name;
var $htmlSource;
var $tags;
var $tagsTable;



public function __construct(){
	$excluded_words_file = ADAFMW_CLASSES_DIR.'/Language/'.LANGUAGE.'/excluded_words.txt';
	$excluded_wordsAr = file($excluded_words_file);
	$this->excl_words = $this->setExcludedWords($excluded_wordsAr);

}


function setExcludedWords($excluded_wordsAr){
		$excluded_words = array();
		foreach ($excluded_wordsAr as $excl_row){
			$excl_rowAr = explode(",",$excl_row);
			$excluded_words = array_merge($excl_rowAr,$excluded_words);
		}
		return $excluded_words;
}

function updateIndex($filename,$indexFile){
	// adding a single text file to index
	$text = file_get_contents($filename);
	$res = self::getWordList($text);
	$index = $this->createIndex($res,$filename);
	$count = count($index);
	$result = $this->writeIndex($index, $indexFile,'append');
	if ($result !== FALSE)
		return $count;
	else
		return $result;

}

function spider(Array $fileList,$indexFile){
	// array of file
	$res = array();
	$index = array();
	foreach ($fileList as $fileName){
		$url = explode('/',$fileName);
		$panel = $url[count($url)-1];
		$room = $url[count($url)-2];
		$panelUrl =  HTTP_ROOT_DIR.'/'.$room.'/'.$panel;
		$text = file_get_contents($fileName);
		$res = self::getWordList($text);
		$index = array_merge($index,$this->createIndex($res,$panelUrl));
	}
	asort($index);
	$count = count($index);
	$result = $this->writeIndex($index, $indexFile,'create');
	if ($result !== FALSE)
		return $count;
	else
		return $result;
}

/**
 * getWordList
 *
 * @param  mixed $text
 * @return array
 */
static function getWordList(String $text = NULL){
	/* Ritorna un array con le parole presenti nel testo passato  */
	// cleaning: si possono utilizzare varie strategie, anche insieme


	if ($text == NULL)
		return array();

	if (MANREPL){
	// conversione uno a uno > htmlentities
		$con = array ('"','"',"'","'","'",'à','è','é','ì','ò','ù','À','È','Ì','Ò','Ù','á','é',' í','ó','ú','Á','É','Í','Ó','Ú');
		$dasostituire = array ('&rdquo;','&ldquo;','&quot;','&rsquo;','&lsquo;','&agrave;','&egrave;','&eacute;','&igrave;','&ograve;','&ugrave;','&Agrave;','&Egrave;','&Igrave;','&Ograve;','&Ugrave;','&aacute;','&eacute;',' &iacute;','&oacute;','&uacute;','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;');
		$text = str_replace($dasostituire, $con, $text);
	}
	if (STRPTG) {
		$text = strip_tags($text);
	}
	if (HTMLSPC) {
		$text =  htmlspecialchars($text, ENT_COMPAT,'UTF-8', FAlSE);
	}
	if (PREGCLNCHRS){
		//$pattern = "/[^(\w|\d|\'|\"|\.|\!|\?|;|,|\\|\/|\-\-|:|\&|@)]+/";
		$pattern = "/[^(\w|\d|\-)]+/";
		$text = preg_replace ($pattern, " ", $text);

	}
	if (CLNCHRS){

		$dasostituire = "-";
		$con = " ";
		$text = str_replace($dasostituire, $con, $text);
	}
	// end cleaning
	$addedchars = 'àèéìòù';
	$res = array_count_values(str_word_count(strtolower($text), 1,$addedchars));
	return $res;
}




function createIndex($res,$url){
	$excluded_words = $this->excl_words;
	$tags = array();
	foreach ($res as $key => $value){
		if ((!in_array($key,$excluded_words)) && (strlen($key)>MINLENGHTWORD)){
			$tag = array(
				'tagname' => $key,
			//	'weight'  => $value,
				'url' => $url
				);
			array_push($tags,$tag);
		}
	}
	$this->tags = $tags;
	return $tags;
}

function writeIndex($res,$fileName,$mode='create'){
	$table = $this->getCSVTable($res);
	if ($mode== 'append')
		$result = file_put_contents($fileName,$table,FILE_APPEND | LOCK_EX);
	else
		$result = file_put_contents($fileName,$table, LOCK_EX);
	return $result;
}

function getJson($tags=NULL){
	if ($tags == NULL)
		$tags = $this->tags;
	$Json = "[";
	foreach ($tags as $tag){
		$Json.= "{text: \"".$tag['tagname']."\", weight: ".$tag['weight'].", link: \"".$tag['url']."\"},";
	}
	$Json = substr($Json,0,-1);
	$Json .= "]";
	return $Json;
}

function getHtmlTable($tags=NULL){
	if ($tags == NULL)
		$tags = $this->tags;
	$html = "<table id=\"wordtable\" class=\"dataTable\">";
	$html .= "<thead><tr><th>N</th><th>PAROLA</th><th>FREQUENZA</th></tr></thead>";
	$html .= "<tfoot><tr><th>N</th><th>PAROLA</th><th>FREQUENZA</th></tr></thead>";
	$html .= "<tbody>";
	$n=0;
	array_multisort($tags,SORT_DESC);
	foreach ($tags as $tag){
		$n++;
		$html.= "<tr><td>$n</td><td>".$tag['tagname']."</td><td>".$tag['weight']."</td></tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
	return $html;
}

function getCSVTable($tags=NULL){
	if ($tags == NULL)
		$tags = $this->tags;
	$csv = "";
	$n=0;
	array_multisort($tags,SORT_DESC);
	foreach ($tags as $tag){
		$n++;
		// $csv.= $tag['weight'].';'.$tag['tagname'].';'.$tag['url']."\r\n";
		$csv.= $tag['tagname'].';'.$tag['url']."\r\n";
	}
	return $csv;
}



function getCSVfromTable(){
/* returns a CSV with the same structure of a given HTML table */
	$html = $this->htmlSource;
	$csv = array();
	preg_match('/<table(>| [^>]*>)(.*?)<\/table( |>)/is',$html,$t);
	$table = $t[2];
	preg_match_all('/<tr(>| [^>]*>)(.*?)<\/tr( |>)/is',$table,$r);
	$rows = $r[2];
	foreach ($rows as $row) {
		//cycle through each row
		if(preg_match('/<th(>| [^>]*>)(.*?)<\/th( |>)/is',$row)) {
			//match for table headers
			preg_match_all('/<th(>| [^>]*>)(.*?)<\/th( |>)/is',$row,$b);
			// strip_tags(implode(';',$b[2]));
			$csv[] = array_map('strip_tags',$b[2]);
		} elseif(preg_match('/<td(>| [^>]*>)(.*?)<\/td( |>)/is',$row)) {
			//match for table cells
			preg_match_all('/<td(>| [^>]*>)(.*?)<\/td( |>)/is',$row,$b);
			$csv[] = array_map('strip_tags',$b[2]); //  strip_tags(implode(';',$b[2]));
		}
	}

	return $csv;
}


function searchWord($lexiconFile,$searchTerm,$mode='exactMatch'){

	$CSV = new \Tools\CSVImporter($lexiconFile, $parse_header=false, $delimiter=";", $length=8000);
	$contents = $CSV->get();
	$elenco_valori = array();
	$results = array();
	$n = 0;
	$searchTerm = strToUpper($searchTerm);
	foreach($contents as $onecontent){
			//var_dump($onecontent);
			$value = strToUpper($onecontent[0]);
			if ($mode == 'exactMatch'){
				if (($value == $searchTerm) AND (!in_array($onecontent[1], $results))){
					$n++;
					$results[] = $onecontent[1];
				}
			} else {
				if ((stristr($value,$searchTerm)) AND (!in_array($onecontent[1], $results))){
					$n++;
					$results[] = $onecontent[1];
				}
			}
	}
	return $results;
}

} // end class

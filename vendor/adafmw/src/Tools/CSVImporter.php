<?php

namespace Tools;

class CSVImporter
// @author: Myrddin.myriddin
{
    private $fp;
    private $parse_header;
    private $header;
    private $delimiter;
    private $length;

        
    /**
     * __construct
     *
     * @param  mixed $file_name
     * @param  mixed $parse_header
     * @param  mixed $delimiter
     * @param  mixed $length
     * @return void
     */
    function __construct($file_name, $parse_header=false, $delimiter="\t", $length=8000)
    {
        $this->fp = fopen($file_name, "r");
        $this->parse_header = $parse_header;
        $this->delimiter = $delimiter;
        $this->length = $length;
       

        if ($this->parse_header)
        {
           $this->header = fgetcsv($this->fp, $this->length, $this->delimiter);
          
        }

    }
    
    /**
     * __destruct
     *
     * @return void
     */
    function __destruct()
    {
        if ($this->fp)
        {
            fclose($this->fp);
        }
    }
    
    /**
     * get
     *
     * @param  mixed $max_lines
     * @return array
     */
    function get($max_lines=0)
    {
        //if $max_lines is set to 0, then get all the data

        $data = array();

        if ($max_lines > 0)
            $line_count = 0;
        else
            $line_count = -1; // so loop limit is ignored

        while ($line_count < $max_lines && ($row = fgetcsv($this->fp, $this->length, $this->delimiter)) !== FALSE)
        {
            if ($this->parse_header)
            {
                $labels = $this->header;    
                $i = 0;
                foreach ($labels as $label)
                {
                    $row_new[$label] = $row[$i];
                    $i++;
                }
                $data[] = $row_new;
            }
            else
            {
                $data[] = $row;
            }

            if ($max_lines > 0)
                $line_count++;
        }
        return $data;
    }
}

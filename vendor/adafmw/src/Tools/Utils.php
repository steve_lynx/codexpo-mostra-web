<?php
/**
 * @ Utilities
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
 
namespace Tools;
 
class Utils { 



static function or_null($s){
      if (!$s || $s == "''")
        return "NULL";
      else
        return $s;
}


/*
time and date static functions
*/


static function mytimer ($op=""){
$start_time =  $GLOBALS['start_time'];
if (empty($op)){
   $start_time = time();
   $time_elapsed = 0;
} else {
   $end_time = time();
   $time_elapsed = ($end_time-$start_time);
}
return " $op: ".$time_elapsed;
}

static function today_dateFN(){
$now = time();
return self::ts2dFN($now);
}

static function today_timeFN(){
$now = time();
return self::ts2tmFN($now);
}


static function ts2dFN($timestamp=""){
if (empty($timestamp))
   $timestamp = time();

$dataformattata = strftime(DATE_FORMAT, $timestamp);   
/*
$data = getdate($timestamp);
$dataformattata = $data['mday']."/".$data['mon']."/".$data['year'];
*/
return $dataformattata;

}


static function ts2tmFN($timestamp=""){
if (empty($timestamp))
   $timestamp = time();
$data = getdate($timestamp);
$dataformattata = $data['hours'].":".$data['minutes'].":".$data['seconds'];
return $dataformattata;
}



static function sumDateTimeFN ($arraydate){
// array date is dd/mm/yy,hh:mm:ss
// $date = dt2tsFN($arraydate[0]);
// $time = tm2tsFN($arraydate[1]);
$date = $arraydate[0];
$time = $arraydate[1];

// Data
$date_ar = explode("/",$date);
if (count($date_ar)<3)
    return 0;
$giorno =$date_ar[0];
$mese = $date_ar[1];
$anno =$date_ar[2];

// ORA
$time_ar = explode(":",$time);
switch (count($time_ar)){
      case 0:
           $time_ar[]="00";
           $time_ar[]="00";
           $time_ar[]="00";
      break;
      case 1:
           $time_ar[]="00";
           $time_ar[]="00";
      break;
      case 2:
           $time_ar[]="00";
      break;
}

$ora =$time_ar[0];
$min =$time_ar[1];
$sec =$time_ar[2];


$timestamp = mktime($ora,$min,$sec,$mese,$giorno,$anno);
return ($timestamp);
}




static function dt2tsFN ($date){
    $date_ar = explode('[\\/.-]', $date);
    if (count($date_ar)<3)
        return 0;
    $format_ar = explode ('[/.-]',DATE_FORMAT);
    if ($format_ar[0]=="%d"){
          $giorno = (int)$date_ar[0];
          $mese = (int)$date_ar[1];
    } else {   // english-like format
          $giorno = (int)$date_ar[1];
          $mese = (int)$date_ar[0];
    }

    $anno =(int)$date_ar[2];

    $unix_date = mktime(0,0,0,$mese,$giorno,$anno,-1);
    return $unix_date;

}


static function tm2tsFN ($time){
$time_ar = explode(":",$time);
switch (count($time_ar)){
      case 0:
           $time_ar[]="00";
           $time_ar[]="00";
           $time_ar[]="00";
      break;
      case 1:
           $time_ar[]="00";
           $time_ar[]="00";
      break;
      case 2:
           $time_ar[]="00";
      break;
}

$ora =$time_ar[0];
$min =$time_ar[1];
$sec =$time_ar[2];
$unix_time = mktime($ora,$min,$sec,1,1,1970);
return $unix_time;
}



/*
array static functions
*/

static function aasort(&$array, $args) {
/*
Syntax: aasort($assoc_array, array("+first_key", "-second_key", etc..));
*/
    $args = array_reverse($args);
    if (count($array) > 0) {
            foreach($args as $arg) {
                $temp_array = $array;
                $array = array();
                $order_key = substr($arg, 1, strlen($arg));

                foreach($temp_array as $index => $nirvana) $sort_array[$index] = $temp_array[$index][$order_key];

                ($arg[0] == "+") ? (asort($sort_array)) : (arsort($sort_array));

                foreach($sort_array as $index => $nirvana) $array[$index] = $temp_array[$index];
                }
    }

} //end aasort


static function masort ($array, $arg,$sort_order=1,$sort_method=SORT_STRING) {
// multiple array sort

/* works with typical AMA array, ie:
array (
                array (asd,3f,asdf),
                array (5,asdf,34)
        )

$arg: field to be used as key
$sort_order : 1 (default) or -1 (reverse)
*/
$temparray = array();
$i=0;
foreach ($array as $subarray){
      $key = ucfirst(strtolower($subarray[$arg]));
      if  (!isset($temparray[$key]))
        $temparray[$key]= $i;
      else
        $temparray[$key].=",$i";
      $i++;
      // echo$key.":".$temparray[$key]."<br>";
}

$arraycopy = array();
$max = count ($array);
$i=0;
if ($sort_order==-1)
	krsort($temparray,$sort_method);
else	
	ksort($temparray,$sort_method);
foreach ($temparray as $key=>$value){
         $keyAr = explode(",",$value);
         foreach ($keyAr as $keyElem){
                  $arraycopy[$i] = $array[$keyElem];
                  $i++;
         }
}
return $arraycopy;
}


static function list_array ($array) {
// CONVERTE UN ARRAY IN STRINGA
$str='';
    foreach($array as $key=> $value) {
        $str .= "<b>$key:</b> $value<br>\n";
    }
    return $str;
}


static function in_array_ci($needle, $haystack) {
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

// class utility static functions

static function print_variables($obj) {
        $arr = get_object_vars($obj);
        foreach ($arr as $prop=>$val)
                echo "\t$prop = $val<br>\n";
}

static function print_methods($obj) {
        $arr = get_class_methods(get_class($obj));
        foreach ($arr as $method)
                echo "\tstatic function $method()<br>\n";
}

///////////////////////////////

static function get_param_stringFN(){
/* session id propagation  */

switch (SESSION_MODE)  {
case 'auto':          //  propagation by  URL, nothing to do
case 'cookies':       // uses cookies
    $session_id_par = '';
    break;
case 'manual':           //  we have to propagate it manually in all links
default:
    $session_id_par = "SID&"; //this is the string to add to every link
}
return  $session_id_par;
}


///////////////////////////////

static function whoami(){
         if (isset($_SERVER['PHP_SELF'])){
             $parent = $_SERVER['PHP_SELF'];
         } else {
             // $parent = $SCRIPT_NAME; // not available in PHP>4.2.1
             $parent =  $_SERVER['SCRIPT_NAME'];
         }

    $self = array_shift(explode('.',basename($parent)));  
     return $self;


}


///////////////////////////////
// HTM Entities IN and OUT

static function get_htmlspecialchars( $given, $quote_style = ENT_QUOTES ){
   return htmlentities( self::unhtmlentities(  $given ) , $quote_style  );
}

static function unhtmlentities ($string)  {
   $trans_tbl = get_html_translation_table (HTML_ENTITIES);
   $trans_tbl = array_flip ($trans_tbl);
   $ret = strtr ($string, $trans_tbl);
   return preg_replace('/&#(\d+);/me', 
     "chr('\\1')",$ret);
} 

static function convertDoc2HTML($txt){
       $len = strlen($txt);
       $res = "";
       for($i = 0; $i < $len; ++$i) {
           $ord = ord($txt[$i]);
           // check only non-standard chars          
           if($ord >= 126){ 
               $res .= "&#".$ord.";";
           }
           else {
               // escape ", ' and \ chars
               switch($ord){
                   case 34 : 
                       $res .= "\\\"";
                       break;
                   case 39 : 
                       $res .= "\'";
                       break;
                   case 92 : 
                       $res .= "\\\\";
                       break;                    
                   default : // the rest does not have to be modified
                       $res .= $txt[$i];
               }                    
           }
       }
       return $res;
} 
/*
/ finds the last occurence of a string into a sting(text)
*/
static function lastIndexOf($haystack,$needle) {
       $index = strpos(strrev($haystack), strrev($needle));
       $index = strlen($haystack) - strlen($index) - $index;
       return $index;
   } 
   
   
static function in_multi_array($needle, $haystack)
        {
                $in_multi_array = false;
                if(in_array($needle, $haystack)){
                        $in_multi_array = true;
                }else{
                        for($i = 0; $i < sizeof($haystack); $i++){
                                if(is_array($haystack[$i])){
                                        if(self::in_multi_array($needle, $haystack[$i])) {
                                                $in_multi_array = true;
                                                break;
                                        }
                                }
                        }
                }
                return $in_multi_array;
        }

static function getCurrentUri(){
		$basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath)); 
        if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
        $uri =  trim($uri, '/');
		return  $uri;
	}
	
static function dismount($object) {
	
    $reflectionClass = new \ReflectionClass(get_class($object));
    
    $array = array();
    foreach ($reflectionClass->getProperties() as $property) {
        $property->setAccessible(true);
        $array[$property->getName()] = $property->getValue($object);
        $property->setAccessible(false);
    }
    return $array;
}

static function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars static function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    } else {
        // Return array
        return $d;
    }
}

static function translateFN($strFrom,$language = ""){
	// it's just an alias for backward compatibility; to be cleaned
	$t = new Translator($language);
	return $t->translate($strFrom);
}
	
static function translate($strFrom,$language = ""){
	$t = new Translator($language);
	return $t->translate($strFrom);
}

}

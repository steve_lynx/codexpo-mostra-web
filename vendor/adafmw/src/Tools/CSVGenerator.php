<?php
/**
 * @ CSVGenerator
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace Tools;
 
class CSVGenerator {
/*
Classe per la costruzione di un CSV

*/

	  var $data;
	  var $separator;
	  var $labels;
	  
	  var $CSV;
	  var $error;

	  /**
	   * __construct
	   *
	   * @param  mixed $data
	   * @param  mixed $labels
	   * @param  mixed $separator
	   *
	   * @return void
	   */
	  function __construct($data, $labels, $separator = ','){
		 if ((is_array($data)) && (is_array($labels))) {
			$this->data = $data;
			$this->labels = $labels;
			$this->separator = $separator;
			$this->setCSV();
		  } else 
			$this->setError('Tipo di dato errato');	
 			
      }

      /**
       * setCSV
       *
       * @return void
       */
      function setCSV(){
				 
		$data = array();
		foreach ($this->labels as $key=>$value){
			// urk! @FIXME: should be: _getHiddenColumns !!!
			if ($key <> 'hiddenColumns')	
				$row[]= $key;
		}	
		$data[] = $row;
		
		foreach ($this->data as $id=>$rowObj){
			$row = array();
			foreach ($rowObj as $key=>$value){
				$row[]=$value;
			}	
			$data[] = $row;
		
		}
		$this->CSV = $data; 
	
	  }
    

   
      function getData(){
		 if (is_null($this->error) && $this->CSV <> null)
			 return $this->CSV;
		  else {
			 return $this->getError();    
		 }	 
      }

      function getError(){
             return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }

// end class CSVGenerator
}

<?php
/**
 * @ XMLGenerator
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

namespace Tools;
 
class XMLGenerator {
/*
Classe per la costruzione di un DOM XML

*/

	  var $xmlRoot;
	  var $domTree;
	  var $error;
	 
    
	

/*
 * 
 * name: Header::__construct
 * @param array $data
 * @return
 * 
 */
	  function __construct($data){
 			$this->domtree = new \DOMDocument('1.0', 'UTF-8');
			$this->xmlRoot = $this->domtree->createElement("xml");
			$this->xmlRoot = $this->domtree->appendChild($this->xmlRoot);
			if (is_array($data))
				$this->setXML($data);
			else 
				$this->setError('Tipo di dato errato');	

			
         
      }

/*
 * 
 * name: setXML
 * @param array $data
 * @return
 * 
 */
      function setXML($data){
		//  var_dump($data);
        foreach ($data as $id=>$row){
			   $currentElement= $this->domtree->createElement("record");
				$currentElement= $this->xmlRoot->appendChild($currentElement);     
				if (is_object($row))
					foreach ($row as $column=> $value){
						$currentElement->appendChild($this->domtree->createElement($column,$value));
					}
				else
					$currentElement->appendChild($this->domtree->createElement($row));
		  }
	
     }
    

      function getData(){
             if (empty($this->error) and (!empty($this->domtree)))
                 return $this->domtree->saveXML();
              else {
				
				 return $this->getError();    
			 }	 
      }

      function getError(){
             if (!empty($this->error))
                 return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }

// end class XMLGenerator
}

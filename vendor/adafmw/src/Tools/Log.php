<?php
/*
 * @name Log
 * @package IPOCAD
 * @author Stefano Penge
 * @copyright IPOCAD
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
namespace Tools;

class Log {
	
		
	
	static function getLogLevel(){
		// always returns an ARRAY of log levels (even if of a single element) since LOG_LEVEL could be a product of prime numbers
			
			$logLevelValue = $GLOBALS['configManager']->getConfigValue('log_level'); 
			$logLevelsAr = array();
			$logLevelConstAr =  $GLOBALS['configManager']->getConfigValue('log_level_values');
			  
			foreach($logLevelConstAr as $levelConst ) {
				if (($logLevelValue % $levelConst) == 0)
					$logLevelsAr[] = $levelConst;
			}
		
			return $logLevelsAr;		
	}
	
	static function getLogType(){
		// always returns an ARRAY of log types (even if of a single element) since LOG_TYPE could be  a product of prime numbers
			$logTypeValue  =  $GLOBALS['configManager']->getConfigValue('log_type'); 
			$logTypesAr = array();
			$logTypeConstAr = $GLOBALS['configManager']->getConfigValue('log_type_values'); 
			
			foreach($logTypeConstAr as $typeConst ) {
				if (($logTypeValue % $typeConst) == 0)
					$logTypesAr[] = $typeConst;
			}
			return $logTypesAr;
	}
	
	static function getLogDir(){
		return $GLOBALS['configManager']->getConfigValue('log_dir');
	}
	
	static function getLogAccessFileName(){
			return  $GLOBALS['configManager']->getConfigValue('log_access_filename');
	}	
	
	static function getLogErrorFileName(){
		return  $GLOBALS['configManager']->getConfigValue('log_error_filename');
	}	
	
		
	static function doLog($required_level, $text = "", $className = null){
		$log_levelAr = self::getLogLevel(); 
		if (in_array($required_level, $log_levelAr)){
			
			switch ($required_level){
				case LOG_ACCESS:
					$log =  self::getDebugDate()." ".self::getClientIpAddres(). " ".self::getURL()."\n";
					break;
				case LOG_ERRORS:
					if ($className == null) 
						$className =   self::getModule(); 
					$debugAr = 	debug_backtrace();
					$debugString = "";
					/*
					foreach($debugAr as $debugItem){
						foreach($debugItem as $debugKey =>$debugValue){
							switch(true) {
							case (is_object($debugValue)):
								$debugValueAsString = "";
								break;
							case (is_array($debugValue)):
								$debugValueAsString = "";
								break;	
							default:
								$debugValueAsString = $debugValue;
							}	
							$debugString.= "$debugKey: $debugValueAsString\n"; 
						}				
					}
					*/
					$log =  self::getDebugDate(). ': '.$className. '> '."$text".$debugString."\n";	
					break;
				case LOG_DB:	
					// @todo: add doctrine info 
					$log =  self::getDebugDate(). ': '.$className. '> '."$text\n";
					$logType = "";
					break;
				case NO_LOG:
				default:
					$log = "";	
			}
		self::logThis($log,$required_level);
		}
	}
	
	private static function logThis ($log,$required_level){
		if ($log <> ""){
			$log_dir = self::getLogDir();
			
			$log_typeAr =  self::getLogType(); 
			foreach($log_typeAr as $log_type){
				switch ($log_type) {
					case LOG_TO_DB:
						break; // not implemented
					case LOG_TO_SCREEN:
						$logObj = new \View\HTML\iElement('span','','log');
						$logObj-> setElement($log);
						echo $logObj->getElement();
						break;
					case LOG_TO_FILE:
						switch($required_level){
							case LOG_ERRORS:
								$log_filename = self::getLogErrorFileName();
								break;
							case LOG_ACCESS:
							default								:
								$log_filename = self::getLogAccessFileName();
								break;
						}
						$filename = $log_dir. $log_filename;
						 $res = @file_put_contents($filename, $log, FILE_APPEND);
						 break;
					case LOG_TO_NULL:
					default:
						// ?	 
					}	
			}
	}	  
	}

	protected static function getDebugDate() {
 		return date('d/m/Y H:i:s'); // . substr((string)microtime(), 1, 8);
    }	
	
	
	protected static function getClientIpAddres(){
		return $_SERVER['REMOTE_ADDR'];
	}
	
	protected static function getModule(){
		return $_SERVER['REQUEST_URI'];
	
	}
	
	protected static function getURL(){
		$url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
		return 'http:'.$escaped_url;
	
	}
	

	
} // end class Log

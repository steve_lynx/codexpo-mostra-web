<?php
/**
 * @name Translator
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace Tools;

class Translator {

private $language;
private $translationsAr;
private $error;

/*
 * 
 * name: Translator::__construct
 * @param
 * @return
 * 
 */
	function __construct ($language){
	
		if ($language == "")
			if (isset($_SESSION['language'])) 
				$language = $_SESSION['language'];  // overriding main config value
			else
				$language = LANGUAGE;		
		$this->language = $language;
		$vocabularyClass = '\Language\\'.$this->language.'\Vocabulary'; // one file for language
		$vocabulary =  new $vocabularyClass;
		$this->translationsAr = array($this->language=>$vocabulary->getVocabulary());
	}

/*
 * 
 * name: Translator::translate
 * @param
 * @return
 * 
 */
	public function getTranslation($strFrom){
	   $stringFrom = ucfirst($strFrom);
	   $translation = $stringFrom;
	   
	   if ($stringFrom<>""){
		   $language = $this->language;
		   
		   if ((isset($_SESSION[$language])) AND (array_key_exists($stringFrom,$_SESSION[$language]))){
				$translation =  $_SESSION[$language][$stringFrom];
		   } elseif (array_key_exists($stringFrom,$this->translationsAr[$language]) &&  (!is_null( $this->translationsAr[$language][$stringFrom])))	{	
				$translation =  $this->translationsAr[$language][$stringFrom];
				$_SESSION[$language][$stringFrom] = $translation;
		   } else  {
				$this->error = "Testo non trovato: $strFrom";
				Log::doLog(LOG_ERRORS, 'Warning: '.$this->getError());

			}	
		}
		return $translation;
	}
	
	public function translate($strFrom=""){
		return $this->getTranslation($strFrom);
	}
	
	function getError(){
		return $this->error;
	}

} // end class Translator

<?php
/**
 * @class ConfigManager
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 */

namespace Tools;

class ConfigManager {

public $config;
private $error = '';

function __construct ($definedConstantsAr){
	
	foreach ($definedConstantsAr as $constantName=>$constantValue){
		$variableName = strToLower($constantName);
		/* this version cuts off the hyphens from constanst and apply CamelCase
		$sepPos = true;
		while ($sepPos!=false){
			$sepPos = strpos($variableName,'_');
			if ($sepPos!= false){
				$first = substr($constantName,0,$sepPos);
				$rest = substr($constantName,$sepPos+1);
				$variableName = $first.ucfirst($rest);
			} else 
				$variableName = $constantName;
		}	
		*/
		$configAr[$variableName] = $constantValue; 
	}
	
	$this->config =$configAr; 
	// could be more concise:
//	$this->config = array_map(function($c){return lowercase($c); }, $definedConstantsAr); 
}




public function setConfigValue($param, $value){
	$answer = "";
	if (array_key_exists($param, $this->config)){
  		$this->config[$param] = $value;
		$answer = $this->config[$param];
	}
  	else {
		$this->error = 'Valore non trovato per '.$param;  	
		\Tools\Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(), get_class());
	}	
 	return $answer;
}

public function getConfigValue($param){
	
	$value = 	'';
	if (array_key_exists($param, $this->config))
  		$value =  $this->config[$param];
  	else {
		$this->error = 'Valore non trovato per '.$param;  	
		\Tools\Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(), get_class());
	}	
 	return $value;
}

public function getError(){
		return $this->error; 
}

} // end class ConfigManager

<?php
/*
 * @name CacheManager
 * @package IPOCAD
 * @author Stefano Penge
 * @copyright IPOCAD
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
namespace Tools;
 
class CacheManager {
    var $static_mode;
    var $cache_mode;
    var $static_dir;
    var $static_filename;
    var $error ="";
    var $action;
    var $status = FALSE;
    
    public function __construct($action = null) {
    
        $this->checkCache($action);
    
    }    
    
    function checkCache($action)   {
    // verify constants and GET parameter cachemode
    // sets the properties static_mode and cache_mode 
    // set the error property if cache doesn't exist  
    // also check for protected actions (those that cannot be cached, like search)  

    	$not_cachable_actionsAr = $GLOBALS['configManager']->getConfigValue('not_cachable_actions');
      	if (in_array($action, $not_cachable_actionsAr))
      	 	$static_mode = NO_CACHE;
      	else
                $static_mode = $GLOBALS['configManager']->getConfigValue('cache_mode');

        $this->static_dir = $GLOBALS['configManager']->getConfigValue('root_dir').'/'.$GLOBALS['configManager']->getConfigValue('cache_dir').'/';
	$this->static_filename = md5($_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING']);
	$cached_file = $this->static_dir.$this->static_filename; 
	$file_static_version_ok = file_exists($cached_file) ===TRUE; 
				
                switch ($static_mode){
                        case UPDATE_CACHE:
                                $cache_mode = 'cache'; //rw the content is read from file only if lifetime is > $ic_lifetime
                                                       // otherwise it is  dynamically generated  and then written back to file
                                $this->status = $file_static_version_ok;           
                                break;
                        case FORCE_UPDATE_CACHE:
                                $cache_mode = 'updatecache'; //rw: the content is dynamically generated and then written back to file
                                $this->status = $file_static_version_ok;           
                                break;
                        case READONLY_CACHE:
                                $cache_mode = 'readcache'; //read only: the file is always loaded but never rewritten
                                $this->status = $file_static_version_ok;           
                                break;
                        case NO_CACHE:
                        default:    
                                $cache_mode = 'nocache'; //always dynamically read from DB, do not write to file
                                break;	

                        }		
						
                
        

       $this->static_mode = $static_mode;
       $this->cache_mode = $cache_mode; 

    } // end checkCache


    function getCachedData(){
        
        // read cache file 
          
  
            $file_static_time_ok = FALSE; 
            $file_static_version_ok =  $this->status;
            $cached_file = $this->static_dir.$this->static_filename; 

            if ($this->static_mode > NO_CACHE){
                    //$file_static_version_ok = @file_exists($cached_file);
                    if ($this->static_mode > READONLY_CACHE){
                        if ($this->static_mode < FORCE_UPDATE_CACHE){
                            $file_static_time_ok = (@filemtime($cached_file) > (time()-IC_LIFE_TIME));
                        } else {
                            $file_static_time_ok = TRUE;
                        }    
                    } else {
                            $file_static_time_ok = TRUE;
                    }	
            }	
         
            if ($file_static_version_ok AND $file_static_time_ok) {
                    readfile($cached_file);
                    exit();
                    // returns the file
             } else {
                    return NULL;
                    // if file doesn't exist, or is too old, etc we have to read content from DB
             }
    } //end getCachedData


    function writeCachedData ($html){
       
         if (
                 ($this->static_mode > READONLY_CACHE) OR 
                 ($this->cache_mode == 'cache') OR 
                 ($this->cache_mode == 'updatecache')
                 ){ // we have to (re)write the cache file
                    $cached_file = $this->static_dir.$this->static_filename; 
                    $result = file_put_contents($cached_file, $html); 
                    if  ($result == FALSE) {
			$this->setError("Impossibile scrivere sulla cache.");
			Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(),get_class());
	                }
 		}
     } //end writeCachedData
     
function getErrorCode(){
	return $this->error_code;
}
	
function setError($error){
        $this->error = $error;

}
function getError(){
        return $this->error;
}
 
} //end Cache Manager

?>

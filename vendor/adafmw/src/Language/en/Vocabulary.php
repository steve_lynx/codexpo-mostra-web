<?php

namespace Language\en;

class Vocabulary {

var $base;
var $messages;
var $errorMessages;
var $roomNames;

function __construct(){
		$this->roomNames = array(
				// Room names
				'Ingresso'=>'Enter',
				'Genere'=>'Gender',
				'Linguaggi'=>'Languages',
				'Concetti'=> 'Concepts',
				'Contesti'=>'Contexts',
				'Arte'=>'Art',
				'Attori'=>'Actors',
				'Show'=>'Show',
				'Stili'=>'Styles'	,
				'Regole'=>'Rules',
				'Interfacce'=>'Interfaces',
				'Codici'=>'Codes',
				'Musei'=>'Museums',
				'Geo'=>'Geo',
				'Timeline'=>'Timeline',
				'Libreria'=>'Library',
				'Uscita'=>'Exit'
				);
		$this->base = array(
				// Interface labels
				'Pannelli' => 'Panels',
				'Esplorazione' => 'Exploration',
				'Ricerca'=> 'Search',
				'Scheda'=> 'Card',
				'Mappa' => 'Map',
				'Elenco'=> 'List',
				'Modifica'=>'Edit',
				'Aggiungi'=>'Add',
				'Elimina'=>'Delete',
				'Carica'=>'Load',
				'AreaRiservata'=>'ReservedArea',
				'Crediti'=>'Credits',
				'Aiuto'=>'Help',
				'Cerca'=>'Search',
				'Risultati'=>'Results',
				'Prossimo pannello'=>'Next panel ',
				'Versione'=>'Version ',
				'Stanze'=>'Rooms',
				'Visita una stanza a caso...'=>'Visit a random room...',
				'Prossimo pannello:'=>'Next panel:',
				'Scarica in '=>'Download ',
				'Versione:'=>'Version:',
				'Ci sono %d stanze e %d pannelli'=>'There are %d rooms and %d pannels',
				);
		$this->messages = array(
				' record presenti in archivio'=>' record presenti in archivio',
				' record trovati in archivio'=>' record trovati in archivio',
				'Non ci sono record da mostrare'=>'Non ci sono record da mostrare',
				'Non ci sono record da cancellare'=>'Non ci sono record da cancellare',
				'Non ci sono record da modificare'=>'Non ci sono record da modificare',
				'I dati sono stati cancellati.'=>'I dati sono stati cancellati.',
				'I dati sono stati aggiornati.'=>'I dati sono stati aggiornati.',
				'Il file è stato caricato nel repository.'=>'Il file è stato caricato nel repository.',
				'Alcuni dati erano presenti e non sono stati sovrascritti.'=>'Alcuni dati erano presenti e non sono stati sovrascritti.',
				'Mostra/nascondi colonne: '=>'Mostra/nascondi colonne: ',
				'Ricerca semplice'=>'Ricerca semplice',
				'Ricerca avanzata'=>'Ricerca avanzata',
				'Repositories disponibili:'=>'Repositories disponibili:',
				'Pannello non disponibile'=>'Panel not found',
				'Termine non trovato'=>'Term not found',
				);
		$this->errorMessages = array(
				'Il formato dei dati non è valido'=>'Il formato dei dati non è valido',
				'Il parametro non esiste.'=>'Il parametro non esiste.',
				'Il template non esiste.'=>'Il template non esiste.',
				'Questo tipo di file non è corretto.'=>'Questo tipo di file non è corretto.',
				'Il css non esiste.'=>'Il css non esiste.',
				'Accesso negato.'=>'Accesso negato.',
				'Errore del DB.'=>'Errore del DB.',
				'Bisogna specificare un file'=>'Bisogna specificare un file',
				'Errore nella struttura del file'=>'Errore nella struttura del file',
				'Formato non valido'=>'Formato non valido',
				'Impossibile creare il menu'=>'Impossibile creare il menu',
				'Errore nello spostamento del file'=>'Errore nello spostamento del file',
			);
}

function getVocabulary(){
	return array_merge($this->base,$this->messages, $this->errorMessages,$this->roomNames);
}


}

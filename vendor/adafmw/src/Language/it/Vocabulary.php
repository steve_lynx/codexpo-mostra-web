<?php

namespace Language\it;

class Vocabulary {

var $base;
var $messages;
var $errorMessages;
var $roomNames;

function __construct(){
		$this->roomNames = array(
				// Room names
				'Ingresso'=>'Ingresso',
				'Genere'=>'Genere',
				'Linguaggi'=>'Linguaggi',
				'Concetti'=> 'Concetti',
				'Contesti'=>'Contesti',
				'Arte'=>'Arte',
				'Attori'=>'Attori',
				'Show'=>'Show',
				'Stili'=>'Stili'	,
				'Regole'=>'Regole',
				'Interfacce'=>'Interfacce',
				'Codici'=>'Codici',
				'Musei'=>'Musei',
				'Geo'=>'Geo',
				'Timeline'=>'Timeline',
				'Libreria'=>'Libreria',
				'Uscita'=>'Uscita',
				);
		$this->base = array(
				// Interface labels
				'Pannelli' => 'Pannelli',
				'Esplorazione' => 'Esplorazione',
				'Ricerca'=> 'Ricerca',
				'Scheda'=> 'Scheda',
				'Mappa' => 'Mappa',
				'Elenco'=> 'Elenco',
				'Modifica'=>'Modifica',
				'Aggiungi'=>'Aggiungi',
				'Elimina'=>'Elimina',
				'Carica'=>'Carica',
				'AreaRiservata'=>'AreaRiservata',
				'Crediti'=>'Crediti',
				'Aiuto'=>'Aiuto',
				'Cerca'=>'Cerca',
				'Risultati'=>'Risultati',
				'Prossimo pannello'=>'Prossimo pannello ',
				'Versione'=>'Versione del ',
				'Stanze'=>'Stanze',
				'Visita una stanza a caso...'=>'Visita una stanza a caso...',
				'Prossimo pannello:'=>'Prossimo pannello:',
				'Scarica in '=>'Scarica in ',
				'Versione:'=>'Versione:',
				'Ci sono %d stanze e %d pannelli'=>'Ci sono %d stanze e %d pannelli',
			
				);
		$this->messages = array(
				' record presenti in archivio'=>' record presenti in archivio',
				' record trovati in archivio'=>' record trovati in archivio',
				'Non ci sono record da mostrare'=>'Non ci sono record da mostrare',
				'Non ci sono record da cancellare'=>'Non ci sono record da cancellare',
				'Non ci sono record da modificare'=>'Non ci sono record da modificare',
				'I dati sono stati cancellati.'=>'I dati sono stati cancellati.',
				'I dati sono stati aggiornati.'=>'I dati sono stati aggiornati.',
				'Il file è stato caricato nel repository.'=>'Il file è stato caricato nel repository.',
				'Alcuni dati erano presenti e non sono stati sovrascritti.'=>'Alcuni dati erano presenti e non sono stati sovrascritti.',
				'Mostra/nascondi colonne: '=>'Mostra/nascondi colonne: ',
				'Ricerca semplice'=>'Ricerca semplice',
				'Ricerca avanzata'=>'Ricerca avanzata',
				'Repositories disponibili:'=>'Repositories disponibili:',
				'Pannello non disponibile'=>'Not found',
				'Termine non trovato'=>'Termine non trovato',
				);
		$this->errorMessages = array(
				'Il formato dei dati non è valido'=>'Il formato dei dati non è valido',
				'Il parametro non esiste.'=>'Il parametro non esiste.',
				'Il template non esiste.'=>'Il template non esiste.',
				'Questo tipo di file non è corretto.'=>'Questo tipo di file non è corretto.',
				'Il css non esiste.'=>'Il css non esiste.',
				'Accesso negato.'=>'Accesso negato.',
				'Errore del DB.'=>'Errore del DB.',
				'Bisogna specificare un file'=>'Bisogna specificare un file',
				'Errore nella struttura del file'=>'Errore nella struttura del file',
				'Formato non valido'=>'Formato non valido',
				'Impossibile creare il menu'=>'Impossibile creare il menu',
				'Errore nello spostamento del file'=>'Errore nello spostamento del file',
			);
}

function getVocabulary(){
	return array_merge($this->base,$this->messages, $this->errorMessages,$this->roomNames);
}


}

<?php

namespace Language\fr;

class Vocabulary {

var $base;
var $messages;
var $errorMessages;
var $roomNames;

function __construct(){
		$this->roomNames = array(
				// Room names
				'Ingresso'=>'Entrée',
				'Genere'=>'Genre',
				'Linguaggi'=>'Languages',
				'Concetti'=> 'Concepts',
				'Contesti'=>'Contexts',
				'Arte'=>'Art',
				'Attori'=>'Acteurs',
				'Show'=>'Show',
				'Stili'=>'Styles'	,
				'Regole'=>'Règles',
				'Interfacce'=>'Interfaces',
				'Codici'=>'Codes',
				'Musei'=>'Musées',
				'Geo'=>'Geo',
				'Timeline'=>'Ligne du temps',
				'Libreria'=>'Bibliothèque',
				'Uscita'=>'Sortie'
				);
		$this->base = array(
				// Interface labels
				'Pannelli' => 'Panneaux',
				'Esplorazione' => 'Exploration',
				'Ricerca'=> 'Chercher',
				'Scheda'=> 'Fiche',
				'Mappa' => 'Map',
				'Elenco'=> 'Liste',
				'Modifica'=>'Modifier',
				'Aggiungi'=>'Ajouter',
				'Elimina'=>'Eliminer',
				'Carica'=>'Charger',
				'AreaRiservata'=>'ZonerReservée',
				'Crediti'=>'Credits',
				'Aiuto'=>'Aide',
				'Cerca'=>'Chercher',
				'Risultati'=>'Resultats',
				'Prossimo pannello'=>'Prochain panneau',
				'Versione'=>'Version ',
				'Stanze'=>'Salles',
				'Visita una stanza a caso...'=>'Visiter une salle au hazarad...',
				'Prossimo pannello:'=>'Prochain panneau:',
				'Scarica in '=>'Télécharger ',
				'Versione:'=>'Version:',
				'Ci sono %d stanze e %d pannelli'=>'Il y a %d salles et %d panneaux',
				
				);
		$this->messages = array(
				' record presenti in archivio'=>' record presenti in archivio',
				' record trovati in archivio'=>' record trovati in archivio',
				'Non ci sono record da mostrare'=>'Non ci sono record da mostrare',
				'Non ci sono record da cancellare'=>'Non ci sono record da cancellare',
				'Non ci sono record da modificare'=>'Non ci sono record da modificare',
				'I dati sono stati cancellati.'=>'I dati sono stati cancellati.',
				'I dati sono stati aggiornati.'=>'I dati sono stati aggiornati.',
				'Il file è stato caricato nel repository.'=>'Il file è stato caricato nel repository.',
				'Alcuni dati erano presenti e non sono stati sovrascritti.'=>'Alcuni dati erano presenti e non sono stati sovrascritti.',
				'Mostra/nascondi colonne: '=>'Mostra/nascondi colonne: ',
				'Ricerca semplice'=>'Ricerca semplice',
				'Ricerca avanzata'=>'Ricerca avanzata',
				'Repositories disponibili:'=>'Repositories disponibili:',
				'Pannello non disponibile'=>'Panel not found',
				'Termine non trovato'=>'Term not found',
				);
		$this->errorMessages = array(
				'Il formato dei dati non è valido'=>'Il formato dei dati non è valido',
				'Il parametro non esiste.'=>'Il parametro non esiste.',
				'Il template non esiste.'=>'Il template non esiste.',
				'Questo tipo di file non è corretto.'=>'Questo tipo di file non è corretto.',
				'Il css non esiste.'=>'Il css non esiste.',
				'Accesso negato.'=>'Accesso negato.',
				'Errore del DB.'=>'Errore del DB.',
				'Bisogna specificare un file'=>'Bisogna specificare un file',
				'Errore nella struttura del file'=>'Errore nella struttura del file',
				'Formato non valido'=>'Formato non valido',
				'Impossibile creare il menu'=>'Impossibile creare il menu',
				'Errore nello spostamento del file'=>'Errore nello spostamento del file',
			);
}

function getVocabulary(){
	return array_merge($this->base,$this->messages, $this->errorMessages,$this->roomNames);
}


}

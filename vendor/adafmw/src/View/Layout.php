<?php
 /**
 * @name Layout
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
namespace View;

class Layout {
/**
 * @class Layout
 *
 */


       var $CSS_filename;
       var $JS_filename;
       var $layout;
       var $template;
       var $template_filename;
       var $error;
       var $errorCode;
	
/*
 * 
 * name: Layout::__construct
 * @param
 * @return
 * 
 */
    function  __construct($layout="",$template="",$JSAutoload=1,$CSSAutoload=1){
 		
		 if ($template<>"")                 
			$this->template = $template;
		 else 
			 $this->template = $GLOBALS['configManager']->getConfigValue('template');
		 
		 $template_name = $this->template; // in header ....
			 
		 if ($layout<>"")
			$this->layout = $layout; // overriding the config value
		 else 
			$this->layout = $GLOBALS['configManager']->getConfigValue('layout');
												
		 $this->setTemplate();	     

		 if ($CSSAutoload)
			$this->setCSS();
		 
		 if ($JSAutoload)
			$this->setJS();
		  
	   }
	   
	   function setTemplate(){

		 if (!strstr($this->template,'.tpl'))
			$this->template.='.tpl';  
			          
		 $this->template_filename =  $GLOBALS['configManager']->getConfigValue('root_dir').'/templates/'.$this->layout.'/'.$this->template;
		 if (!file_exists($this->template_filename)){
			$this->template_filename = $GLOBALS['configManager']->getConfigValue('root_dir').'/templates/'.$this->layout.'/default.tpl'; // this one should exist!
		 } 
		 if  (!file_exists($this->template_filename)){
		 	$this->error = "Il template ".$this->template_filename." non esiste.";
			$this->errorcode = 1;
			Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(), get_class());
		 } 
	    }
		
		function setCss(){
			 // Layout CSS autoload
			 $this->CSS_filename = array();
			 $CSS_dir = $GLOBALS['configManager']->getConfigValue('root_dir').'/templates/'.$this->layout.'/css'; 
			 $filesArr = array_diff(scandir($CSS_dir), array('..', '.'));
			 foreach ($filesArr as $key => $value) {
					$this->CSS_filename[] = $value;
			 }
		}
	   
	    function setJS(){
		   // Default: load all existent js file from directory
			$this->JS_filename = array();
			$JS_dir = $GLOBALS['configManager']->getConfigValue('root_dir').'/templates/'.$this->layout.'/js';
			$filesArr = array_diff(scandir($JS_dir), array('..', '.'));
			foreach ($filesArr as $key => $value) {
					$this->JS_filename[] = $value;
			}

        }		
		
		function getError(){
			return $this->error;
		}
  

} // end of Layout class

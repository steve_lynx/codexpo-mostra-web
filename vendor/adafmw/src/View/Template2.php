<?php
/**
 * @ Template
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View;
 
class Template {
/*
Classe che si occupa della sostituzione dei segnaposto con i valori delle variabili

*/

	  var $data;
	  var $templateFileName;
	  var $template;
	 
    
	

/*
 * 
 * name: Template::__construct
 * @param
 * @return
 * 
 */
	  function __construct($templateFileName = ""){
          
          if ($templateFileName<>"") {
              $this->templateFileName = $templateFileName;
          } else {
              $this->setError("Bisogna specificare un template");
          }
      }

      function setTemplate($dataHa = null){
          /*
           riempie i campi del template
           
           Il template è HTML standard con campi,
           Per default i campi sono commenti in stile dreamWeaver 4)
           <!-- #BeginEditable "doctitle" --><!-- #EndEditable -->
           ma il formato può essere cambiato dal file di configurazione
           ed è contenuto nella costante REPLACE_FIELD_CODE
           I dati passati sono in forma di array associativo field=>data
           */
          
          if (
              !is_null($dataHa) && 
              is_array($dataHa)
              ){
                  $this->data = $dataHa;
                  $http_root_dir =   $GLOBALS['configManager']->getConfigValue('http_root_dir');
                  $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
                  $replace_field_code =  REPLACE_FIELD_CODE;
                  
                  // all the html template
                  $tpl = '';
                  $fid = fopen($this->templateFileName,'r');
                  while ($row = fread($fid,4096)){
                      $tpl.=$row;
                  }
                  
                  // only the body part
                  $bodytpl = strstr ($tpl,'<body');
                  $n = strpos($bodytpl, "</body>");
                  //  $tpl = substr ($bodytpl, 6, $n-6);
                  $tpl = substr ($bodytpl,strpos($bodytpl, ">",5)+1,$n-strpos($bodytpl, ">",5)-1);
                  
                  // replacing fields
                  foreach ($this->data as $field=>$data){
                      $ereg = str_replace('%field_name%',$field,$replace_field_code);
                      // in previous versions, $data could be an array ; disabled here
                      /*if (gettype($data)=='array'){
                       $tObj = new Table();
                       $tObj->setTable($data);
                       $tabled_data = $tObj->getTable();
                       $tpl = str_replace($ereg,$tabled_data,$tpl);
                       } else {
                       $tpl = str_replace($ereg,$data,$tpl);
                       }
                       */
                      $tpl = str_replace($ereg,$data,$tpl);
                  }
                  $this->template =  $tpl;
          } else {
              $this->setError("Dati nulli o in formato non corretto");
            
          }
      }
      
      
      /*
       *
       * name: Template::getFieldNames
       * @param
       * @return
       *
       */
      function getFieldNames(){
          /*
           Returns an array of field names; used by automagic template filling
           */
          $fields_array = array();
          $pattern = '/<!-- #BeginEditable ([^ ]*)"([^ \.]*) --><!-- #EndEditable -->/';
          $template = $this->templateFileName;
          
          if (file_exists($template)){
              $tpl = '';
              $fid = fopen($template,'r');
              while (($row = fgets($fid)) !== false) {
                  if (preg_match($pattern, $row, $matches)){
                      $parts = explode('"',$matches[0]);
                      $fields_array[] = $parts[1];
                  }
              }
              fclose($fid);
          } else {
              $this->setError("File ".$this->templateFileName." non trovato");
              Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(), get_class());
          }
          return $fields_array;
      }
      
     
      /*
      *
      * name: Page::verify_templateFN
      * @param Array
      * @return Array
      *
      */
      function verifyTemplate($dataHa){
          /* Debug: verify if template exists and if there is a  match among number and names of fields
           case 0: ok
           case 1: file doesn't exist
           case 2: more field in template than in data array
           (some field are left empty, we want to filter data from code side)
           case 3: more field in data array than in template
           (some data get lost, we want to filter data  from interface side)
           
           
           */
          $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
          $replace_field_code =  REPLACE_FIELD_CODE;
          
          
          $template = $this->templateFileName;
          if (file_exists($template)){
              $tpl = '';
              $fid = fopen($template,'r');
              while ($row = fread($fid,4096)){
                  $tpl.=$row;
              }
              
              $tplOk = array();
              foreach ($dataHa as $field=>$data){
                  $ereg = str_replace('%field_name%',$field,$replace_field_code);
                  $tplOk[$field] = strpos($ereg,$tpl);
              }
              
              
          } else {
              $this->setError("Il template non esiste.");
              $this->setErrorCode(1);
          }
          
          $totalTplFields = count($tplOk);
          $totalDataFields = count($dataHa);
          $matching = ($totalDataFields-$totalTplFields);
          if ($matching>0){
              $this->setError("I campi del template non sono sufficienti.");
              $this->setErrorCode (3);
              Log::doLog(LOG_ERRORS, 'Warning: '.$this->getError(), get_class());
          } elseif ($matching<0){
              $this->setError("Non tutti i campi del template sono stati riempiti.");
              $this->setErrorCode(2);
              Log::doLog(LOG_ERRORS, 'Warning: '.$this->getError(), get_class());
          } else {
              $this->setError('');
              $this->setErrorCode(0);
          }
          return array($this->getErrorCode(),$this->getError());
      }
      
      
      
      
    

      function getTemplate(){
             if (empty($this->error) and (!empty($this->template)))
                 return $this->template;
              else {
                 $this->setError("Impossibile costruire la pagina.");
				 return $this->getError();    
			 }	 
      }

      function getError(){
             if (!empty($this->error))
                 return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }
      
      function getErrorCode(){
          if (!empty($this->errorCode))
              return $this->errorCode;
      }
      
      function setErrorCode($errorCode){
          $this->errorCode = $errorCode;
        
      }

// end class Template
}

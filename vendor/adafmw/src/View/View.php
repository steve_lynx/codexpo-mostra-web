<?php
/**
 * @class View
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 */

namespace View;
class View {
var $error;
var $errorCode;


	   function getError(){
         if (!empty($this->error))
            return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          \Tools\Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }
      
      function getErrorCode(){
          if (!empty($this->errorCode))
              return $this->errorCode;
      }
      
      function setErrorCode($errorCode){
          $this->errorCode = $errorCode;
        
      }
}
?>
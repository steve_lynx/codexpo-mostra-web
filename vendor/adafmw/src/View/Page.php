<?php
/**
 * @Page
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

 
namespace View;
use Dompdf\Dompdf;
use Dompdf\Options;

class  Page extends Output
/**
 * @class Page
 *
 */

{
       var $htmlheader;
       var $htmlbody;
       var $htmlfooter;
       
       var $layout; // be careful:  it is an object, not  a string. The name is in $this->layout->layout_name
       var $template; // be careful: it is an object, not  a string. 
       var $JSCode; // object
       var $CSScode; // object
       var $description;
       var $keywords;
       var $title;

       
		/** 
		* @name: Page::__construct
		* @param unknown $self
		* @param string $layout
		* @param string $template
		* @param number $JSAutoload
		* @param number $CSSAutoload
		* @param number $headerAutoload
		* @param unknown $action
		* @param string $description
		* @param string $title
		* @param string $keywords
		*/
       function  __construct($self, $layout="", $template=""){
			

			$JSAutoload = $GLOBALS['configManager']->getConfigValue('js_autoload') ; 
			$CSSAutoload = $GLOBALS['configManager']->getConfigValue('css_autoload'); 
			$description = $GLOBALS['configManager']->getConfigValue('description');
			$keywords = $GLOBALS['configManager']->getConfigValue('meta');
			$title = $GLOBALS['configManager']->getConfigValue('title');

			$this->layout = new Layout($layout, $template, $JSAutoload, $CSSAutoload);
			$this->setMetaData($description, $title, $keywords);
			$this->JSCode = new JSCode($layout, $this->layout->JS_filename);
			$this->CSSCode = new CSSCode($layout, $this->layout->CSS_filename);
			$this->template = new Template($this->layout->template_filename); 
			$this->setHeader($layout, $JSAutoload, $CSSAutoload);
			
			
	   }
	   
	   
	   /**
	    * setMetaData
	    *
	    * @param  mixed $description
	    * @param  mixed $title
	    * @param  mixed $keywords
		* @param  mixed $expires
		* @param  mixed $pragma
		* @param  mixed $robots
		* @param  mixed $copy
		*
	    * @return void
	    */
	   function setMetaData($description = "", $title="", $keywords=""){
			if (isset($keywords))
				$this->keywords = $keywords;
			if (isset($title))
				$this->title = $title;
			if (isset($description))
				$this->description = $description;
			$this->expires = date("D, d M Y H:i:s GMT");
			$this->pragma = 'NO-CACHE';
			$this->robots = "ALL";
			$this->copy = COPY;
	   }
	   
	   /**
	    * setHeader
	    *
	    * @param  mixed $action
	    * @param  mixed $layout
	    *
	    * @return void
	    */
	   function setHeader ($layout, $JSAutoload, $CSSAutoload){ 
		// <head> is already in template
		// we have only to substitute css and js etc
		 $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
		 $http_root_dir = $GLOBALS['configManager']->getConfigValue('http_root_dir');
		 $ada_fmw_dir =  $GLOBALS['configManager']->getConfigValue('ada_fmw_dir');
		 
		 $template = $this->layout->template;
		 $layout = $this->layout->layout;
		 $description = $this->description;
		 $keywords = $this->keywords;
		 $title = $this->title;
		 $expires = $this->expires;
		 $pragma = $this->pragma;
		 $robots = $this->robots;
		 $copy = $this->copy;

		 $url = $http_root_dir.'/'.\Tools\Utils::getCurrentUri();
		 
		if ($JSAutoload)
			$JS = $this->JSCode->getCode(); 
		else
			$JS = "";	
		if ($CSSAutoload)
			$CSS = $this->CSSCode->getCode(); 
		else	
			$CSS = "";	
			 
		$data = array(
				'field_template'=>$template,
				'layout'=>$layout,
				'field_description'=>$description,
				'field_keywords'=>$keywords,
				'field_title'=>$title,
				'field_expires'=>$expires,
				'field_pragma'=>$pragma,
				'field_robots'=>$robots,
				'field_copy'=>$copy,
				'field_url'=>$url,
				'http_root_dir'=>$http_root_dir,
				'root_dir'=>$root_dir,
				'ada_fmw_dir'=>$ada_fmw_dir,
				'JS'=>$JS,
				'CSS'=>$CSS);
		
		   $this->htmlheader = $this->template->fillTemplate($data,'head');
		   $headObj = new \View\HTML\iElement('head','','');
		   $headObj->setElement($this->template->fillTemplate($data,'head'));
		   $this->htmlheader = $headObj->getElement(); 
		 		
			
		}


	    function setFooter(){ 
			$this->htmlfooter = "\r\n</html>";
		}
		
		function setBody($data){
			
			// this function is called at the end of the process with dynamic data
			$field_footer = $GLOBALS['configManager']->getConfigValue('footer');
			$field_headerline = $GLOBALS['configManager']->getConfigValue('headerline');
			$field_headercontent = $GLOBALS['configManager']->getConfigValue('headercontent');
			$field_credits = $GLOBALS['configManager']->getConfigValue('credits');
			$staticDataAr  = compact('field_footer','field_headerline','field_headercontent','field_credits');
			$allData = array_merge($data,$staticDataAr);

			$bodyObj = new \View\HTML\iElement('body','','');
			$bodyObj->setElement($this->template->fillTemplate($allData,'body'));
		    $this->htmlbody = $bodyObj->getElement();
			//$this->resetImgSrc($this->layout->layout); not needed if there aren't images in template files
			$this->setFooter();
		}

		function getFooter(){
			return $this->htmlfooter;
		}
	
		
		function getFieldNames(){
		    // @todo: set a Page property?
		    return $this->template->getFieldNames();
		}

        /**
         * resetImgSrc
         *
         * @param  mixed $path
         *
         * @return void
         */
        function resetImgSrc($path=""){
         	// we have to substitute  src="images/pippo.gif" (from template) with src="templates/$layout/images/pippo.gif"
    		$http_root_dir =   $GLOBALS['configManager']->getConfigValue('http_root_dir');
			$root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
			$layout = $this->layout;
            $newpath = "$root_dir/templates/$layout/$path";
            $this->htmlbody = str_replace("src=\"images/","src=\"$newpath/images/", $this->htmlbody);
            $this->htmlbody = str_replace("background=\"images/","background=\"$newpath/images/", $this->htmlbody);
        }

 
     


        /**
         * output
         *
         * @param  string $type
		 * @param  string $filename
         *
         * @return string|null
         */
        function output ($type = 'page', $filename=''){

		// manda effettivamente al browser la pagina,  oppure solo i dati (dimensioni, testo, ...))
        switch ($type){
	        case 'page':  // standard
	        default:
	              $data = "<!DOCTYPE HTML>
<html  xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"it\">\r\n";
	              $data.= $this->htmlheader."\r\n";
	              $data.= $this->htmlbody;
	              $data.= $this->htmlfooter;
	              echo $data;
	              break;
	        case 'dimension':
	              $data = $this->htmlheader;
	              $data.= $this->htmlbody;
	              $data.= $this->htmlfooter;
	              $dim_data = strlen($data);
	              echo $dim_data;
	              break;
	        case 'text':
	              $data = $this->htmlheader;
	              $data.= $this->htmlbody;
	              $data.= $this->htmlfooter;
	              $text_data = strip_tags($data);
	              echo $text_data;
	              break;
	        case 'source': // debugging purpose only
	              $data = $this->htmlheader;
	              $data.= $this->htmlbody;
	              $data.= $this->htmlfooter;
	              $source_data = htmlentities($data);
	              echo $source_data;
	              break;
	        case 'error': // debugging purpose only
	              $data = $this->error;
	              echo $data;
	              break;
	        case 'file': // useful for caching pages
			      $data = "<!DOCTYPE HTML>
<html  xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"it\">";
				
	              $data.= $this->htmlheader."\r\n";
	              $data.= $this->htmlbody;
	              $data.= $this->htmlfooter;
                 return $data; 
                 break;
           	case 'pdf': 
				  // should control if extension is loaded?
				  if (in_array(ROOT.'/config/export_config.php',get_included_files())){
					$data = "<!DOCTYPE HTML>\r\n<html  xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"it\">\r\n"; 					
					$data.= $this->htmlheader;
					$data.= $this->htmlbody;
					$data.= $this->htmlfooter;
					$options = new Options();
					$options->set('chroot',ROOT);
					$dompdf = new Dompdf($options);
					$dompdf->setPaper('B3','portrait');
					// instantiate and use the dompdf class
					$dompdf->loadHtml($data);
					// Render the HTML as PDF
					$dompdf->render();
					// writes PDF to PDF cache dir
					$output = $dompdf->output();
					$path_to_pdf = ROOT.'/'.PDF_CACHE_DIR.'/'.$filename;
					file_put_contents($path_to_pdf, $output);
					// Output the generated PDF to Browser
					$dompdf->stream($filename);      	
				  } else {
					  // should rise an error
					$data= $this->htmlheader."\r\n";
					$data.= \Tools\Utils::translate("Estensione PDF non disponibile.");
					$data.= $this->htmlfooter;
					return $data;
				  }	
	        }
        }





        
} //end class Page

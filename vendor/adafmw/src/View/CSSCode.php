<?php
/**
 * @ CSSCode
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

namespace View;
 
class CSSCode {
/*
Classe per la costruzione del codice che include i CSS

*/

	  var $code;

	  var $stylesheetpath;
	  var $httpStylesheetPath;
	  var $relStylesheetPath;
	  var $CSSFileNames;
    
	

/*
 * 
 * name: CSSCode::__construct
 * @param
 * @return
 * 
 */
	  function __construct($layout, $CSSFileNames = null){
     
	      if (empty($CSSFileNames)){
	          $this->CSSFileNames = array("layout"); // ?
	      } else {
	          $this->CSSFileNames = $CSSFileNames;
	      }
	      
	      $http_root_dir =   $GLOBALS['configManager']->getConfigValue('http_root_dir');
	      $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
	      $ada_fmw_dir = $GLOBALS['configManager']->getConfigValue('ada_fmw_dir');
	      $http_layout_dir = $GLOBALS['configManager']->getConfigValue('http_layout_dir');
	      //
	      $this->stylesheetpath = $root_dir."/templates/$layout/css/";
	      $this->httpStylesheetPath = $http_root_dir."/templates/$layout/css/";
	      $this->relStylesheetPath = $ada_fmw_dir."/templates/$layout/css/";

	      $this->setCode();
	    
      }

      
      function setCode(){
          // inserting CSS style related link
          $html_css_code = "<noscript>\n";
          
          foreach ($this->CSSFileNames as $stylesheet){
              if (strstr($stylesheet,'.css')){
                  
                  $stylesheetfile =  $this->stylesheetpath.$stylesheet; // if there is no path, we add it
                  if (
                      //TRUE // no more need to check?
                      (@is_file($stylesheetfile)) AND (file_exists($stylesheetfile)==1)
                      ){
                          //$stylesheetfile = $stylesheetpath.$stylesheet;
                          $stylesheetfile = $this->relStylesheetPath.$stylesheet;
                          $html_css_code .= "<link res=\"stylesheet\" type=\"text/css\" href=\"$stylesheetfile\" media=\"screen\">\n";
                  } else {
                      $this->setError("CSS file ".$stylesheetfile." non trovato.");
                  }
                  
              }
          }
          $html_css_code.= "\n</noscript>\n";
          $this->code = $html_css_code;
      }
          
         
    

      function getCode(){
             if (!empty($this->code))
                 return $this->code;
              else {
				 return $this->getError();    
			 }	 
      }

      function getError(){
             if (!empty($this->error))
                 return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }

// end class CSSCode
}

<?php
/**
 * @Image
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


namespace View;


class  Image
/**
 * @class Image
 *
 */

{

		var $imageName;
		var $defaultImageName;
		var $fontPath =  ROOT."/vendor/adafmw/font/";
		var $defaultFont = "SourceCodePro-Regular.ttf"; 
		var $defaultFontSize = 60;
		var $imageType;
		var $path;
		var $name;
		var $imageExists;
       
       /**
        * __construct
        *
        * @param  mixed $filename
        * @return void
        */
       function  __construct($filename, $type = 'png', $path ='', $default){
			if (file_exists($path.$filename.'.'.$type)){
				$this->imageName = $path.$filename;
				$this->name = $filename;
				$this->imageExists = TRUE;
				$this->imageType = $type;
			} elseif (file_exists($path.$filename.'.'.DEFAULT_IMAGE_TYPE)){ 
				$this->imageName = $path.$filename;
				$this->name = $filename;
				$this->imageExists = TRUE;
				$this->imageType = DEFAULT_IMAGE_TYPE;
			} else {
				$this->imageName =  $path.$default;
				// hack for Codeshow !!!
				if (stristr($filename,'/')){
					$fullFileNameAr = explode('/',$filename);
					$name = $fullFileNameAr[1];
				} else 
					$name = $filename;
				$this->name = $name;
				$this->imageType = $type;
				$this->imageExists = FALSE;
			}
		 }
		 
		 /**
		  * getImage
		  *
		  * @return void
		  */
		 function getImage(){
			$img = $this->loadImage();
			if ($this->imageType == 'jpg'){
				header("Content-Type: image/jpeg");
				imagejpeg($img);
			} elseif ($this->imageType == 'png'){ 
				header("Content-Type: image/png");
				imagepng($img);
			}	
			imagedestroy($img);
	   }
		
		/**
		 * loadImage
		 *
		 * @return GDImage
		 */
		function loadImage(){
			$imgname = $this->imageName;
			$im = FALSE;
			if ($this->imageExists != FALSE){
				if ($this->imageType == 'jpg')
 					$im = @imagecreatefromjpeg($imgname.'.jpg');
 				elseif ($this->imageType == 'png')
 					$im = @imagecreatefrompng($imgname.'.png');
			}	
		    /* See if it failed */
		    if(!$im)
		    {
		        $width = 800;
				$heigth = 450;
				/* Create a black image */
		        // $im  = imagecreatetruecolor($width, $heigth);
				/* Using a default base: */
				$im = @imagecreatefromjpeg($imgname.'.jpg');
				// Create some colors
				$white = imagecolorallocate($im, 255, 255, 255);
				$grey = imagecolorallocate($im, 128, 128, 128);
				$black = imagecolorallocate($im, 0, 0, 0);


				$backgroundcolor = $white;
		        $textcolor  = $black;
				$shadowcolor = $grey;
		        // imagefilledrectangle($im, 10, 10, $width-10, $heigth-10, $backgroundcolor);

				$fontName = $this->fontPath.$this->defaultFont;
				$maxTextSize = $this->defaultFontSize * 12; 
				
				$name = str_replace("_"," ",$this->name,);
				$textsize = strlen($name);

				$fontSize = min($maxTextSize / $textsize, $this->defaultFontSize);
				$tb = imagettfbbox($fontSize, 0, $fontName, $this->name);

				$x = ceil(($width - $tb[2]) / 2); // lower left X coordinate for text
				$y =  ceil(($heigth - $tb[3]) / 4); // lower left Y coordinate for text
				
				// Add some shadow to the text
				imagettftext($im, $fontSize, 0, $x-3, $y-3, $shadowcolor, $fontName, $name);

				// Add the text
				imagettftext($im, $fontSize, 0, $x, $y, $textcolor, $fontName, $name);

				// imagestring($im, 5, 150, 145, $this->name, $textcolor);


		    }

		    return $im;
		}


} //end class Image

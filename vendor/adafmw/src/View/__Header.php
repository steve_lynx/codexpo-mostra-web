<?php
/**
 * @ Header
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View;
 
class Header {
/*
Classe per la costruzione di un header HTML

*/

	  var $header;
	 
    
	

/*
 * 
 * name: Header::__construct
 * @param
 * @return
 * 
 */
	  function __construct($action, $layout, $template, $description, $keywords, $title, $url){
          $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
          $http_root_dir = $GLOBALS['configManager']->getConfigValue('http_root_dir');
          $default = ucfirst($GLOBALS['configManager']->getConfigValue('template'));
          $this->setHeader(ucfirst($action), $layout, ucfirst($default), $template, $description, $keywords, $title, $url, $root_dir, $http_root_dir);
         
      }

     function setHeader($action, $layout, $default, $template, $description, $keywords, $title, $url, $root_dir, $http_root_dir){
        
              
              if ($action=="") {
                  $headerFile = "$root_dir/templates/$layout/headers/$default".'_header.inc.php';
                  if (file_exists($headerFile)){
                      $this->header = include($headerFile);
                  }else {
                     $this->setError("Header file ".$headerFile." non trovato.");
                    
                  }    
              } else {
                  $headerFile = "$root_dir/templates/$layout/headers/$action".'_header.inc.php';
                  if (file_exists( $headerFile)) {
                      $this->header = include($headerFile);
                   } else {
                         // $this->setError("Header file ".$headerFile." non trovato.");
                          $headerFile = "$root_dir/templates/$layout/headers/$default".'_header.inc.php';
                          $this->header = include($headerFile);
                   }
              }
          }
    /*
			function setHeader($action, $layout, $default, $template, $description, $keywords, $title, $url, $root_dir, $http_root_dir){
        
              
              if ($action=="") {
                  $headerFile = "$root_dir/templates/$layout/headers/$default".'_header.tpl';
                  if (file_exists($headerFile)){
                      //$this->header = include($headerFile);
                      $this->header = self::getStaticContent($headerFile);
                  }else {
                     $this->setError("Header file ".$headerFile." non trovato.");
                    
                  }    
              } else {
                  $headerFile = "$root_dir/templates/$layout/headers/$action".'_header.tpl';
                  if (file_exists( $headerFile)) {
                      $this->header = include($headerFile);
                   } else {
                         // $this->setError("Header file ".$headerFile." non trovato.");
                          $headerFile = "$root_dir/templates/$layout/headers/$default".'_header.tpl';
                          $this->header = include($headerFile);
                   }
              }
          }
          */
          
      function getStaticContent($templateFileName, $opts){
			// uses ADAFMW template engine
		
			if (file_exists($templateFileName)){
				$tplObj = new \View\Template($templateFileName);
				$tplObj->setTemplate($opts);
				return $tplObj->getTemplate();
			}	
			else {
				return $tplObj->getError();
			}
		}
      function getHeader(){
             if (empty($this->error) and (!empty($this->header)))
                 return $this->header;
              else {
				
				 return $this->getError();    
			 }	 
      }

      function getError(){
             if (!empty($this->error))
                 return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          \Tools\Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }

// end class Header
}

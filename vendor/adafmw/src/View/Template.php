<?php
/**
 * @ Template
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View;
 
class Template extends View{
/*
Classe che si occupa della sostituzione dei segnaposto con i valori delle variabili

*/

	  var $data;
	  var $templateFileName;
	  var $template;
	 
    
	


	  /**
	   * __construct
	   *
	   * @param  mixed $templateFileName
	   *
	   * @return void
	   */
	  function __construct($templateFileName = ""){
          
          if ($templateFileName<>"") {
              $this->templateFileName = $templateFileName;
              $this->loadTemplate($this->templateFileName);
          } else {
              $this->setError("Bisogna specificare un template");
          }
      }

		/**
		 * loadTemplate
		 *
		 * @param  mixed $template
		 *
		 * @return void
		 */
		function loadTemplate($template = null){
			if (TRUE) // @todo: $GLOBALS['configManager']->getConfigValue(load_template_from_file) == TRUE
				$this->loadTemplateFromFile($template);
			else 
                $this->loadTemplateFromDB($template);
			
		}

		/**
		 * loadTemplateFromFile
		 *
		 * @param  mixed $template
		 *
		 * @return void
		 */
		function loadTemplateFromFile ($template = null){
			 				
 				if (is_null($template))
 					$template = $this->templateFileName;
 				 
            $tpl = '';
            if (file_exists($template)){
            	$fid = fopen($this->templateFileName,'r');
            	while ($row = fread($fid,4096)){
               	 $tpl.=$row;
            	}
            	$this->template =  $tpl;
            } else {	
             	$this->setError("File non trovato");
            }	
		}
			
		static function loadTemplateFromDB ($template = null){
            //@todo
		}	
		
      /**
       * fillTemplate
       *
       * @param  mixed $dataHa
       * @param  mixed $element
       *
       * @return void
       */
      function fillTemplate ($dataHa = null, $element = 'body'){
          // fills-in the fields in template contained in $this->template property  (already loaded from file or from anywhere)
          // with data from array $dataHa
          // ONLY in the $element part
          
          if (
              !is_null($dataHa) && 
              is_array($dataHa)
              ){
                  $this->data = $dataHa;
                  $replace_field_code = $GLOBALS['configManager']->getConfigValue('replace_field_code');
                  
                  $tpl = $this->template; 
                  // only the $element part
                  $elementTpl = strstr ($tpl,'<'.$element.'>'); 
                  $n = strpos($elementTpl, '</'.$element.'>'); 
                  $elementDim = strlen($element)+1; 
                  $tpl = substr ($elementTpl,strpos($elementTpl, '>', $elementDim)+1,$n-strpos($elementTpl, '>', $elementDim)-1);
                 
                  
                  // replacing fields with data from parameter
                  foreach ($this->data as $field=>$data){
                      $ereg = str_replace('field_name',$field,$replace_field_code);
                      $tpl = str_replace($ereg,$data,$tpl);
                  }

                  // hiding extra fields if any
                  $field_array = $this->getFieldNames();
                  foreach ( $field_array as $field){
                      $ereg = str_replace('field_name',$field,$replace_field_code);
                    //  $tpl = str_replace($ereg,"",$tpl);
                  }
                  
                  return  $tpl;
          } else {
          	
              $this->setError("Dati nulli o in formato non corretto");
              return $this->getError();
          }
      }
      
      
      /*
       *
       * name: Template::getFieldNames
       * @param string $template
       * @return array
       *
       */
      function getFieldNames($template = null){
          /*
           Returns an array of field names; used by automagic template filling
           */
          $fields_array = array();
          $pattern = $GLOBALS['configManager']->getConfigValue('regex_pattern');
          if (is_null($template))
          	$template = $this->templateFileName;
          
          if (file_exists($template)){
              $tpl = '';
              $fid = fopen($template,'r');
              while (($row = fgets($fid)) !== false) {
                   if (preg_match($pattern, $row, $matches)){
						$fields_array[] = $matches[1];
                  }
              }
              fclose($fid);
          } else {
              $this->setError("File ".$template." non trovato");
              \Tools\Log::doLog(LOG_ERRORS, 'Error: '.$this->getError(), get_class());
          }
         

          return $fields_array;
      }
      
     

      /**
       * verifyTemplate
       *
       * @param  mixed $dataHa
       * @param  mixed $template
       *
       * @return void
       */
      function verifyTemplate($dataHa, $template = null){
          /* Debug: verify if template exists and if there is a  match among number and names of fields
           case 0: ok
           case 1: file doesn't exist
           case 2: more field in template than in data array
           (some field are left empty, we want to filter data from code side)
           case 3: more field in data array than in template
           (some data get lost, we want to filter data  from interface side)
           
           
           */
          $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
          $replace_field_code =   $GLOBALS['configManager']->getConfigValue('replace_field_code');
          
           if (is_null($template))
          		$template = $this->templateFileName; 
          if (file_exists($template)){
              $tpl = '';
              $fid = fopen($template,'r');
              while ($row = fread($fid,4096)){
                  $tpl.=$row;
              }
              
              $tplOk = array();
              foreach ($dataHa as $field=>$data){
                  $ereg = str_replace('field_name',$field,$replace_field_code);
                  $tplOk[$field] = strpos($ereg,$tpl);
              }
              
              
          } else {
              $this->setError("Il template non esiste.");
              $this->setErrorCode(1);
          }
          
          $totalTplFields = count($tplOk);
          $totalDataFields = count($dataHa);
          $matching = ($totalDataFields-$totalTplFields);
          if ($matching>0){
              $this->setError("I campi del template non sono sufficienti.");
              $this->setErrorCode (3);
              \Tools\Log::doLog(LOG_ERRORS, 'Warning: '.$this->getError(), get_class());
          } elseif ($matching<0){
              $this->setError("Non tutti i campi del template sono stati riempiti.");
              $this->setErrorCode(2);
              \Tools\Log::doLog(LOG_ERRORS, 'Warning: '.$this->getError(), get_class());
          } else {
              $this->setError('');
              $this->setErrorCode(0);
          }
          return array($this->getErrorCode(),$this->getError());
      }
      
      

      /**
       * getTemplate
       *
       * @return void
       */
      function getTemplate(){
             if (empty($this->error) and (!empty($this->template)))
                 return $this->template;
              else {
                 $this->setError("Impossibile costruire la pagina.");
				 	  return $this->getError();    
			 }	 
      }

   

// end class Template
}

<?php
/**
 * @ JSCode
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View;
 
class JSCode {
/*
Classe per la costruzione del codice che include i JS

*/

	  var $code;
	  var $httpJSPath;
	  var $relJSPath;
	  var $httpCSSPath;
	  var $relCSSPath;
	  var $JSFileNames;
    
	

/*
 * 
 * name: JSCode::__construct
 * @param
 * @return
 * 
 */
	  function __construct($layout, $JSFileNames = null){
	     
	      if (empty( $JSFileNames)){
	          $this->JSFileNames = array("z_init.js"); // urk!
	      } else {
	          $this->JSFileNames = $JSFileNames;
	      }
	      $http_root_dir =   $GLOBALS['configManager']->getConfigValue('http_root_dir');
	      $root_dir = $GLOBALS['configManager']->getConfigValue('root_dir');
	      $ada_fmw_dir = $GLOBALS['configManager']->getConfigValue('ada_fmw_dir');
	      $http_layout_dir = $GLOBALS['configManager']->getConfigValue('http_layout_dir');
	      //
	      $jsPath = $root_dir."/templates/$layout/js/";
	      $this->httpJSPath= $http_layout_dir."/templates/$layout/js/";
	      $this->httpCSSPath =  $http_layout_dir."/templates/$layout/css";
	      $this->relJSPath = $ada_fmw_dir."/templates/$layout/js/";
	      $this->relCSSPath =  $ada_fmw_dir."/templates/$layout/css";

	      $this->setCode();
	      //var_dump($this); exit();
      }

      /*
       *
       * name: Page::apply_JSFN
       * @param string $layout
       * @return string
       *
       */
      function setCode(){
          // inserting JS style related link
          //$jsAr = explode(";",$this->JS_filename);
          $jsAr  = $this->JSFileNames;
          $JSpath = $this->httpJSPath;
          $relJSPath = $this->relJSPath;
          $relCSSpath = $this->relCSSPath;
          $CSSpath = $this->httpCSSPath;
          $JS_code = "";
       
          foreach ($jsAr as $js){
              if (!strstr($js,'.js')){
                    $js.='js';
              }
              $jsFile =  $relJSPath.$js; // to check file 

              if (
                  (is_file($jsFile)) AND (file_exists($jsFile)==1)
                  ){
                      $JS_code .= "<script type=\"text/javascript\" src=\"$JSpath$js\"></script>\n";
              } else {
                  $this->setError("File ".$jsFile." non trovato.");
              }
          }
          // hack: we have to insert this manually
          $JS_code.="	\n
<noscript>
<link rel=\"stylesheet\" href=\"$CSSpath/skel-noscript.css\" />
<link rel=\"stylesheet\" href=\"$CSSpath/style.css\" />
</noscript>
<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"$CSSpath/ie/v8.css\" /><![endif]-->
<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"$CSSpath/ie/v9.css\" /><![endif]-->\n";
          
          $this->code = $JS_code;
      } // end JS
      
    

      function getCode(){
             if (!empty($this->code))
                 return $this->code;
              else {
				 return $this->getError();    
			 }	 
      }

      function getError(){
             if (!empty($this->error))
                 return $this->error;
      }
      
      function setError($errorMsg){
          $this->error = $errorMsg;
          \Tools\Log::doLog(LOG_ERRORS, 'Error: '.$errorMsg, get_class());
      }

// end class JSCode
}

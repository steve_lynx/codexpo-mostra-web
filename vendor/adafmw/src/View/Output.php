<?php
/**
 * @class Output
 * @package ADAFmw
 *  @author Stefano Penge
 * @copyright Lynx s.r.l.
 */

namespace View;
class  Output
// Classe generica di output

{
//vars:
       var $interface;
       var $content;
       var $error;
       var $errorCode;
}

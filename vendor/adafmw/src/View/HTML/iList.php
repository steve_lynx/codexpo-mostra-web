<?php
/**
 * @ IList
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View\HTML;
 
class IList extends HTMLelement {
/*
Classe per la costruzione di liste HTML
Il parametro $data dev'essere un array anche multiplo
Se i dati non sono corretti restituisce null e setta la variabile error.

Esempio di chiamata:
  $data = array(
             'pippo',
             'pluto',
             $nipotiniAr,
             'paperino'
              );

$lObj = new IList('myList','aClass','1','a',3);
$lObj->setList($data);
$lObj->printList();

oppure:
$var = $lObj->getList();

*/

	  var $id;
      var $listClass; // a css class
      var $ordered;
      var $type; // disc, square, circle
      var $startvalue;

      var $start_tag;
      var $end_tag;
	

/*
 * 
 * name: IList::__construct
 * @param
 * @return
 * 
 */
      function __construct($listId='', $listclass='',$ordered='0',$type='',$startvalue=1){
				$this->id = $listId;              
                $this->listClass = $listclass;
                $this->ordered = $ordered;
                $this->startvalue = $startvalue;
               
                if ($ordered){ 
                       $this->start_tag = "<ol";
                       $this->end_tag = "</ol>\n";
                } else {
                       $this->start_tag = "<ul";
                       $this->end_tag = "</ul>\n";
                }
  					 if ($startvalue && $ordered){
                		$this->start_tag .= " start=$startvalue";
                }          
             
                if (!empty($type))
                       $this->type = $type;
                /*  else 
                       if ($ordered)
                          $this->type = '1';
                       else
                          $this->type = 'disc';
                */
                if ($this->listClass <>'')	// @todo: if no id and no class
                    $this->start_tag .= " class='".$this->listClass."'";
						
				if ($this->id <>'')	
					$this->start_tag .= " id='".$this->id."'";
					
				$this->start_tag.= ">";

      }

      function setList($data){
          if (gettype($data)!='array'){
               $this->setError("Il formato dei dati non è valido");
          } else {
             $str = $this->start_tag;
             foreach ($data as $riga){
                if (is_array($riga)){
                    $lObj = new Ilist($this->id, $this->listClass, $this->ordered,$this->type,$this->startvalue);
                    $lObj->setList($riga);
                    $str.= $lObj->getList();
                } else  {
                    if ($this->type)
                       $str .= "<li type=".$this->type.">$riga</li>\n";
                    else
                        $str .= "<li>$riga</li>\n";
                }
             }
             $str .= $this->end_tag;
             $this->data = $str;
          }

      }


      function getList(){
             return self::getData();
      }

   
// end class IList
}

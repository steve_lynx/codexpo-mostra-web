<?php
/**
 * @class IElement
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

namespace View\HTML;
 
class IElement extends HTMLelement {
/*
Classe per la costruzione di semplici testi  HTML all'interno di generici elementi (span, div, p)
Il parametro $data dev'essere un array
Se i dati non sono corretti restituisce null e setta la variabile error.

Esempio di chiamata:
  $data = "Sito web";

$lObj = new IElement('a','myUrl','aClass','href','www.sito.it');
$lObj->setElement($data);
$var = $lObj->getElement();

*/

   var $elementType; // p, span, div, a, script, ...
	var $id;
	var $class;		
	var $attrName;
	var $attrValue;

	var $startTag;
	var $endTag;
		

/*
 * 
 * name: IElement::__construct
 * @param
 * @return
 * 
 */
      function __construct($elementType='span', $elementId='', $elementClass='', $attrName='', $attrValue='',$attrName2='', $attrValue2=''){
          
            $this->elementType = $elementType;
			if ($elementId<>'') 
               	$this->id = $elementId;
            if ($elementClass<>'') 
               	$this->elementClass = $elementClass;	
			if ($attrName<>'') {
		        $this->attrName = $attrName;
		    if ($attrValue<>'') 
				$this->attrValue = $attrValue;				
			}  
			if ($attrName2<>'') {
		        $this->attrName2 = $attrName2;
		    if ($attrValue2<>'') 
				$this->attrValue2 = $attrValue2;				
			}                

				$this->startTag = '<'.$this->elementType.' ';
				if (isset($this->id))
					$this->startTag.= 'id = "'.$this->id.'" ';
				if (isset($this->elementClass))
					$this->startTag.= 'class = "'.$this->elementClass.'" ';     
				if (isset($this->attrName))
					$this->startTag.= $this->attrName.'="'.$this->attrValue.'" ';
				if (isset($this->attrName2))
					$this->startTag.= $this->attrName2.'="'.$this->attrValue2.'" ';	
   	            $this->startTag.=  ' >';   
                $this->endTag = '</'.$this->elementType.'>';
                   
      }

/*
 * 
 * name: IElement::setElement
 * @param
 * @return
 * 
 */
      function setElement($data){
          if (gettype($data)!='string'){
               $this->error = "Il formato dei dati non è valido";
            
          } else {
             $str = $this->startTag;
             $str .= $data;
             $str .= $this->endTag;
             $this->data = $str;
          }

      }



      function getElement(){
            return self::getData();
      }



// end class IElement
}

<?php
/**
 * @ Menu
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

namespace View\HTML;
 
class Menu  extends HTMLelement {
/*
Classe per la costruzione di menu come lista di link a URL a partire dalla struttura di una directory
Il parametro $pages dev'essere un percorso esistente.
Questa versione verifica che i file sian nell'array passato come filtro 
Se i dati non sono corretti restituisce null e setta la variabile error.

Esempio di chiamata:
  

$lObj = new Menu('myList','aClass','pages');


oppure:
$var = $lObj->getMenu();

*/

	  var $id;
      var $listClass; // a css class
      var $level = 0;
      var $data;
      var $error;
      var $errorCode;
      
	

/*
 * 
 * name: Menu::__construct
 * @param
 * @return
 * 
 */
      function __construct($listId='', $listclass='',$pages, $filter=null){
			$this->id = $listId;              
            $this->listClass = $listclass;
            $level = $this->level;
  			$this->data = $this->buildMenu($pages,$level,$filter);

      }

    
	 function buildMenu($pages,$level,$filter){
		if (is_string($pages) && is_dir($pages)) {
			$filesAr = array_diff(scandir($pages), array('..', '.'));
			$pagesAr = Array();
			$menuAr = Array();
			foreach ($filesAr as $key => $fileName) { // main menu
				if (is_dir($fileName)){
					$menuAr[$key] = $this->buildMenu($fileName,$level+1, $filter);
				} else {
					// to define a fixed alphabetical order use a single letter on front of page name:
					$page = trim(substr($fileName,0,(strlen($fileName)-5)),'_'); // page1, page2, page3 ....
					$label = \Tools\Utils::translate($page);	
					if (!is_null($filter) && in_array(ucfirst($page),$filter)) {
						$pagesAr[$page] = $fileName; 
						$url =  \View\HTML\HTMLelement::getLink(array('repository'=>$page));
/*
						if ($GLOBALS['configManager']->getConfigValue('routing')== TRUE){
							$url = $GLOBALS['configManager']->getConfigValue('http_root_dir').'/'.$page;
						} else {
							$url = $GLOBALS['configManager']->getConfigValue('http_root_dir').'/index.php?repository='.ucfirst($page);
						}	
*/						$aObj = new iElement('a','','','href',$url);
						$aObj->setElement($label);
						$menuAr[$key] = $aObj->getElement();
					}
				}	
			}
		} elseif (is_array($pages)) { // sub menu
			foreach ($pages as $page){
			/*
				if ($GLOBALS['configManager']->getConfigValue('routing')== TRUE){
					$url =  $GLOBALS['configManager']->getConfigValue('http_root_dir').'/'.$GLOBALS['routes']['repository'].'/'.$page;
				} else {
					$url =  $GLOBALS['configManager']->getConfigValue('http_root_dir').'/index.php?repository='.$GLOBALS['routes']['repository'].'&action='.$page;
				}
				*/
				$url =  \View\HTML\HTMLelement::getLink(array('action'=>$page));
				$aObj = new iElement('a','','','href',$url);
				$aObj->setElement(\Tools\Utils::translate($page));
				$menuAr[] = $aObj->getElement();
			}
		} else {
			$this->error = "Impossibile creare il menu.";
			return null;
		}	
		$idMenu = $this->id;
		$lObj = new iList($idMenu.$level);
		$lObj->setList($menuAr);
		return $lObj->getList();
	 } 
	  

      function getMenu(){
		 return self::getData();
      }

   
// end class Menu
}

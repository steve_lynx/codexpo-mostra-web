<?php
/**
 * @class HTMLelement
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

namespace View\HTML;
 
Abstract class HTMLelement {


 function printData(){
        if (empty($this->error) and (!empty($this->data)))
           print $this->data;
 }

 function getData(){
        if (empty($this->error) and (!empty($this->data)))
            return $this->data;
        else {
            return $this->getError();	
       }	     
 }

function setError($errorMessage, $errorCode = 0){
    // errorCode is not used
    \Tools\Log::doLog(LOG_ERRORS, 'Error: '.$errorMessage,get_class());
	if (!empty($this->error))
		$this->error = $errorMessage;
	else
		$this->error.= "\r\n$errorMessage"; // @FIXME should be an array

}
function getError(){
	if (!empty($this->error)){
		$s = new \View\HTML\IElement('span','error');
		$s->setElement(\Tools\Utils::translate($this->error));
		return $s->getElement(); 
	} else
		return null;
}

/**
 * @param array $params
 * @return string
 */
public static function getLink ($params){
	// simple url construction function but aware of routing mode
	extract($params); 
	if ($repository == '') $repository = $GLOBALS['routes']['repository'];
	if ($action == '') $action = DEFAULT_ACTION;
	$url =  $GLOBALS['configManager']->getConfigValue('http_root_dir').'/';

	if ($GLOBALS['configManager']->getConfigValue('routing')== TRUE){
		$url .=   $repository .'/'.$action;
		foreach ($params as $key=>$value){
			if (!in_array($key,array('action','repository')))
				$url .=   '/'.$key.'/'.$value;
		}

	} else {
		$url .=  'index.php?repository='.ucfirst($repository).'&action='.ucfirst($action);
		foreach ($params as $key=>$value){
			if (!in_array($key,array('action','repository')))
				$url .=   '&'.$key.'='.$value;
		}
	}
	return $url;

}

}

<?php
/**
 * @class Form
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View\HTML;

class Form  extends HTMLelement {
/*
Classe per la costruzione di form HTML.
Il parametro $data dev'essere un array associativo con chiavi type(necessaria, prima chiave!),id, name,label,size,rows,col,wrap,maxlength,value
Se i dati non sono corretti restituisce null e setta la variabile error.

Esempio di chiamata:
$data = array(
                     array(
                          'type'=>'text',
                          'id'=>'username',
                          'name'=>'username',
                          'size'=>'20',
                          'maxlenght'=>'40'
                          ),
                     array(
                          'type'=>'select',
                          'id'=>'mySelect',
                          'name'=>'Scegli',
                          'value'=>array('op1'=>20, 'op2' =>30)
                          ),
                     array(
                          'type'=>'submit',
                          'id'=>'submitBtn',
                          'name'=>'Submit',
                          'value'=>'Clicca qui'
                          )

                    );
$f = new Form('myForm','search.php','aClass','Ricerca','post');
$f-> setForm($data);
$f->printForm();
*/

    var $data;
    var $error;
    var $action;
    var $method;
    var $enctype ;
    var $formClass;
    var $formId;
    var $formName;
    
    function __construct($formId='', $action, $formClass='', $formName='', $method='post',$enctype= "application/x-www-form-urlencoded") {
       if (!empty($action)) {
            $this->action = $action;
       }
       $this->formId = $formId;
       $this->formClass = $formClass;
       $this->formName = $formName;
       $this->method = $method;
       $this->enctype = $enctype;
      }

/*
 * 
 * name: Form::setForm
 * @param
 * @return
 * 
 */
    function setForm($dataHa){

      if ((empty($dataHa)) or (gettype($dataHa)!='array')){
            $this->error = \Tools\Utils::translate("I dati non sono validi");
      } else {
        $formStr = "<form id='".$this->formId."' 
        method='".$this->method."' 
        action='".$this->action."' 
        class='".$this->formClass."' 
        enctype='".$this->enctype."'>\r\n";
        $formStr.= "<fieldset>\n<legend>".$this->formName."</legend>\n";

        foreach ($dataHa as $riga){
          
            $valore = $riga['type'];
            switch ($valore){                              
                  case 'textarea':
                        $str = " <textarea";
                        $state = 'textarea';
                        break;
                  case 'text':
                  case 'password':
                  case 'hidden':
                  case 'radio':
                  case 'checkbox':
                  case 'file':
                        $state = 'input';
                        $str = "   <input type =\"$valore\"";
                        break;
                  case 'submit':
                        $state = 'submit';
                        $str = "  </fieldset>\n<input type =\"$valore\"";
                  break;      
                  case 'select':
                        $state = 'select';
                        $str = " <select ";
            }
            foreach ($riga as $campo=>$valore){
                    switch ($campo){
                        case 'id':
                              $id = $valore;
                              $str .= " id =\"$id\"";
                              break;
                        case 'type':
                              break;
                        case 'name':
                              $str .= " name =\"$valore\"";
                              if ($state == 'select')
                                   $str .= ">\n";
                        break;
                        case 'checked':
                              if  ($valore!="")
                                  $str .= " checked=\"$valore\"";
                        break;
                        case 'value':
                              switch ($state){
                              case 'textarea':
                                   $textarea_value= $valore;
                                   break;
                              case 'select':
                                   
                                   foreach ($valore as $row){
                                         foreach ($row as $key=>$val){
                                              $str .= "<option value='$val'>$val</option>\n";
                                        }
                                    }
                                   $str .= " </select>\n ";
                                   break;
                              default:
                                  $str .= " value =\"$valore\"";
                              }
                        break;
                        case 'size':
                              $str .= " size =\"$valore\"";
                        break;
                        case 'maxlength':
                              $str .= " maxlength =\"$valore\"";
                        break;
                        case 'rows':
                              $str .= " rows =\"$valore\"";
                        break;
                        case 'cols':
                              $str .= " cols =\"$valore\"";
                        break;
                        case 'wrap':
                              $str .= " wrap =\"$valore\"";
                        break;
                        case 'readonly':
                              $str .= " readonly=\"true\"";
                     

                }

            }
          
           
           switch ($state){
                   case 'textarea':
                          $str .=">$textarea_value</textarea>\r\n";
                          break;
                   case 'select':
                          $str = "<label for='$id'>$id</label>\r\n".$str."</select>\r\n"; 
                          break;
                   case 'input':
                          //$str = $str.">\r\n"; 
                         
                          $str = "<label for='$id'>$id</label>\r\n".$str.">\r\n"; 
                          break;     
                          
                  case 'submit':
                        $str = $str." class='button'>\r\n"; 
                        break;              
                   default:
                          $str .=">\r\n";
            }
            $formStr .= $str;
        }
        $formStr .= "</form>\r\n" ;
        
        $this->data = $formStr;
       }
    }

    function printForm(){
       self::printData();
    }

    function getForm(){
          return self::getData();
    }

   

// end class Form
}

<?php
/**
 * @name Table
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View\HTML;

class Table  extends HTMLelement {
/*
Classe per la costruzione di tabelle HTML
Il parametro $data dev'essere un array associativo con chiavi  uguali ai nomi delle colonne
Se i dati non sono corretti restituisce null e setta la variabile error.
Questa versione richiede come unico parametro obbligatorio l'id della tabella; si può anche passare una classe
Esempio di chiamata:
  $data = array(
              array('nome'=>'fghj','cognome'=>'sdfg','età'=>'11'),
              array('nome'=>'sdfj','cognome'=>'ghj','età'=>'22'),
              array('nome'=>'fghj','cognome'=>'hjk','età'=>'33')
              );

$t = new Table();
$t->initTable('myTable');
$t->setTable($data);
$t->printTable();
*/

      var $id;
      var $class;
      var $error;
      var $labelcol;
      var $labelrow;
      var $rules;

/*
 * 
 * name: Table::__construct
 * @param
 * @return
 * 
 */
      function __construct($tableId,$tableClass='',$labelCol = '1',$labelRow='1',$rules=''){
 			$this->tableId = $tableId;       
		  	$this->tableClass = $tableClass;
		    $this->labelCol = $labelCol;
		    $this->labelRow= $labelRow;
		    if (!empty($rules)) {
		           $this->rules = $rules;
		    } else {
		           $this->rules ='groups';
		    }
      }

/*
 * 
 * name: Table::setTable
 * @param
 * @return
 * 
 */
      function setTable($data,$caption="Tabella",$summary="Tabella"){
          if (gettype($data)!='array'){
               $this->error =translate("Il formato dei dati non è valido");
          } else {
            if (count($data)){
             $firstKey = key($data);
             $riga = $data[$firstKey];
//             $riga = $data[0];
             $totcol= count($riga);

             $str = '<table id="'.$this->tableId.'" class="'.$this->tableClass.'" rules="'.$this->rules.'" summary="'.$summary.'">';
             $str.="<caption>$caption</caption>\r\n";
              if ($this->labelCol) {
                     // Colgroups
                     $str.= "<colgroup>\r\n\t";
                     for ($c=0;$c<=$totcol;$c++){
                       $str.="<col id=\"c$c\" />";
                      // $str.="<col>";
                     }
                     $str.="\r\n</colgroup>\r\n";
                        // Headers
                     $str.="<thead>\r\n";
                     $str.="\t<tr>\r\n";

                     reset($data);
                     $firstKey = key($data);
                     $riga = $data[$firstKey];
                      // $riga = $data[0];
                     $str.= "\t<th>&nbsp;</th>";
                     $h=0;
                     if (is_array($riga)){
                       foreach ($riga as $key=>$value){
                          $h++;
                          if (!empty($this->labelCol)){
                             // $str .= "<th id=a$h>$key</th>";
                              $str .= "<th>$key</th>";
                          } else {
                             // $str .= "<th id=a$h>&nbsp;</th>";
                              $str .= "<th>&nbsp;</th>";
                          }
                       }
                     }
                     $str.="\t</tr>\r\n";
                     $str .= "\r\n</thead>\r\n";
             }
             $str .="<tbody>\r\n";
             reset($data);
             $r=0;

             foreach ($data as $riga){
                      $r++;
                      if (gettype($r/2)== 'integer'){
                             if (!empty($this->col1)){
                                  $str .= "\t<tr>";
                             }else {
                                  $str .= "\t<tr class=\"tr_odd\">";
                             }

                      } else {
                             if (!empty($this->col2)){
                                  $str .= "\t<tr>";
                             }else {
                                  $str .= "\t<tr class=\"tr_even\">";
                             }
                      }
                      $str .= "\r\n\t\t";
                      if ($this->labelrow){
                         $str .= "<td>$r</td>";
                      } else {
                         $str .= "<td>&nbsp;</td>";
                      }

                      $h=0;
                      if (is_array($riga)){
                         foreach ($riga as $key=>$value){
                               $h++;
                              // $str .= "<td id=a$h>$value</td>";
                               $str .= "<td>$value</td>";
                         }
                      } else {
                         $str .= "<td>&nbsp;</td>";
                      }
                      $str .= "\r\n\t</tr>\r\n";
                 }
                 $str .="</tbody>\r\n";
                 $str .= "</table>\r\n";
                 $this->data = $str;
             }

        }

      }

      function printTable(){
             self::printData();
      }

      function getTable(){
            return self::getData();    
      }

  
// end class Table
}

<?php
/**
 * @ Submenu
 * @package ADAFmw
 * @author Stefano Penge
 * @copyright Lynx s.r.l.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
namespace View\HTML;
 
class Submenu extends HTMLelement {
/*
Classe per la costruzione di menu come lista di link a URL a partire da un array di nomi di file
Il parametro $pages dev'essere un percorso esistente.
Se i dati non sono corretti restituisce null e setta la variabile error.

Esempio di chiamata:
$lObj = new Menu('myList','aClass','pages');
$var = $lObj->getMenu();

*/

	   var $id;
      var $listClass; // a css class
     
      var $data;
      var $error;
      var $errorCode;
      
	

/*
 * 
 * name: Menu::__construct
 * @param
 * @return
 * 
 */
      function __construct($listId='', $listclass='',$pages){
			$this->id = $listId;              
            $this->listClass = $listclass;
  			$this->data = $this->buildMenu($pages);

      }

    
	 function buildMenu($pages){
		if (is_array($pages)) { // sub menu
			foreach ($pages as $page){
				/*
				if ($GLOBALS['configManager']->getConfigValue('routing')== TRUE){
					$url =  $GLOBALS['configManager']->getConfigValue('http_root_dir').'/'.$GLOBALS['routes']['repository'].'/'.$page;
				} else {
					$url =  $GLOBALS['configManager']->getConfigValue('http_root_dir').'/index.php?repository='.$GLOBALS['routes']['repository'].'&action='.$page;
				}	
				*/
				$url =  \View\HTML\HTMLelement::getLink (array('action'=>$page));
				$aObj = new iElement('a','','','href',$url);
				$aObj->setElement(\Tools\Utils::translate($page));
				$menuAr[] = $aObj->getElement();
			}
		} else {
			$this->error = "Impossibile creare il menu.";
			return null;
		}	
		$idMenu = $this->id;
		$lObj = new iList($idMenu);
		$lObj->setList($menuAr);
		return $lObj->getList();
	 } 
	  

      function getMenu(){
            return self::getData();
      }

    
      
      

// end class SubMenu
}

<!DOCTYPE HTML>
<!--
	Monochromed by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="it">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="powered_by" content="ADA Framework">
	<meta name="URL" content="{{url}}">
	<meta name="template" content="{{template}}">
	<meta name="layout" content="{{layout}}">
	<meta name="description" content="{{description}}">
	<meta name="keywords" content="{{keywords}}">
	<title>{{title}}</title>
	<link rel="stylesheet" media="print" href="http://localhost/codexpo/vendor/adafmw/templates/monochromed-pdf/css/style.css" />
	<!-- link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oxygen:400,300,700" -->
	<!--link rel="stylesheet" type="text/css" href="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/css/oxygen_font.css" -->
	{{CSS}}
	<!--[if lte IE 8]><script src="{{http_root_dir}}/{{ada_fmw_dir}}/js/c_html5shiv.js"></script><![endif]-->
	<script src="{{http_root_dir}}/vendor/jQuery/jquery.js" type="text/javascript"></script>
	<!-- this is to configure skel-min: -->
	<script type="text/javascript">
	var layout_dir="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/";
	</script>
	{{JS}}	
	</head>
	<body>
	<!-- Header -->
		<div id="header">
			<div class="container">
				<!-- Logo -->
					<div id="logo">
					</div>
			</div>
		</div>
	<!-- Header -->
			
	<!-- Main -->
	<div id="main">
		<!-- Content -->
		<div id="content">
			<h2>
				Stanza: {{content_header}} 
			</h2>
			{{image}}
			{{body}} 
		</div>
		<!-- /Content -->
	</div>
	<!-- /Main -->
<hr>
	<!-- Footer -->
	<div id="footer">
		<p>URL: 
		{{url}}
		<br>
		{{file_version}}
		{{copy}}
		</p>
		{{credits}} 
	</div>
	<!-- /Footer -->

	</body>
</html>

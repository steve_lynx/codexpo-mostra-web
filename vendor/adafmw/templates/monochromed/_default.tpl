<!DOCTYPE HTML>
<!--
	Monochromed by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="it">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="powered_by" content="ADA Framework">
	<meta name="URL" content="{{url}}">
	<meta name="template" content="{{template}}">
	<meta name="layout" content="{{layout}}">
	<meta name="description" content="{{description}}">
	<meta name="keywords" content="{{keywords}}">
	<title>{{title}}</title>
	<!--link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Oxygen:400,300,700" -->
	<link rel="stylesheet" type="text/css" href="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/css/oxygen_font.css" >
	{{CSS}}
	<!--[if lte IE 8]><script src="{{http_root_dir}}/{{ada_fmw_dir}}/js/c_html5shiv.js"></script><![endif]-->
	<script src="{{http_root_dir}}/vendor/recline/js/jquery/3.3.1/jquery.js" type="text/javascript"></script>
	<!-- this is to configure skel-min: -->
	<script type="text/javascript">
	var layout_dir="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/";
	</script>
	{{JS}}	
	</head>
	<body>

	<!-- Header -->
		<div id="header">
			<div class="container">
					
				<!-- Logo -->
					<div id="logo">
						<h1><a href='{{home}}'>
							{{title}}
						</a>
						</h1>
						<span>
							{{subtitle}} 
						</span>
					</div>
				
				<!-- Nav -->
					<nav id="nav">
						{{nav}} 
					</nav>

			</div>
		</div>
	<!-- Header -->
			
	<!-- Main -->
		<div id="main">
			<div class="container">
				<div class="row">

					<!-- Sidebar -->
						<div id="sidebar" class="2u">
							<section>
							<header>
									<h2>
									{{side_header}} 
									</h2>
									<a class="image">
										{{image}}
										</a>
								</header>
	
								{{side_content}}

							</section>
						</div>
					<!-- Sidebar -->
				
					<!-- Content -->
						<div id="content" class="8u skel-cell-important">
							<section>
								<header>
									<h2>
									{{content_header}} 
									</h2>
									<span class="byline">
											{{byline1}} 
									</span>
								</header>
								
							
								{{body}} 
								
							</section>
						</div>
					<!-- /Content -->
						
				</div>
			
			</div>
		</div>
	<!-- Main -->

	<!-- Footer -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="3u">
						<section>
							{{pics1Col}} 
						</section>
					</div>
					<div class="3u">
						<section>
							{{pics2Col}} 
						</section>				
					</div>
					<div class="6u">
						<section>
							<header>
								<h3>
 								{{byline2}} 								
								</h3>
							</header>
							<p>
							{{byline3}} 
							</p>
							{{sites_list}} 
						</section>
					</div>
				</div>
			</div>
		</div>
	<!-- Footer -->

	<!-- Copyright -->
		<div id="copyright">
			<div class="container">
				{{footer}}
			</div>
		</div>

	</body>
</html>

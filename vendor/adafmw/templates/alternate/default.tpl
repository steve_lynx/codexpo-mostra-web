<!DOCTYPE HTML>
<!--
	Monochromed by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="it">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="powered_by" content="ADA Framework">
	<meta name="URL" content="{{url}}">
	<meta name="template" content="{{template}}">
	<meta name="layout" content="{{layout}}">
	<meta name="description" content="{{description}}">
	<meta name="keywords" content="{{keywords}}">
	<meta HTTP-EQUIV="expires" content="{{expires}}">
	<meta HTTP-EQUIV="cache-control" content="{{pragma}}">
	<meta name="pragma" content="{{pragma}}">
	<meta name="robots" content="{{robots}}">
	<meta name="copyright" content="{{copy}}">
	<title>{{title}}</title>
	<!--link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oxygen:400,300,700" -->
	<!--link rel="stylesheet" type="text/css" href="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/css/oxygen_font.css"-->
	{{CSS}}
	<!--[if lte IE 8]><script src="{{http_root_dir}}/{{ada_fmw_dir}}/js/c_html5shiv.js"></script><![endif]-->
	<script src="{{http_root_dir}}/vendor/jQuery/jquery.js" type="text/javascript"></script>
	<!-- this is to configure skel-min: -->
	<script type="text/javascript">
	var layout_dir="{{http_root_dir}}/{{ada_fmw_dir}}/templates/{{layout}}/";
	</script>
	{{JS}}
	</head>
	<body>

	<!-- Header -->
		<div id="header">
			<div class="container">
				<!-- Logo -->
					<div id="logo">
						<h1><a href='{{home}}'>
							{{title}}
						</a>
						</h1>
						<span>
							{{subtitle}}
						</span>
					</div>

				<!-- Nav -->
					<nav id="nav">
						{{nav}}
					</nav>

			</div>
		</div>
	<!-- Header -->

	<!-- Main -->
		<div id="main">
			<div class="container">

				<div class="row">

						<!-- Sidebar  -->
						<div id="sidebarL" class="3u">
							<!-- Map -->
							<section>
								<header>
									<h2>
									{{rooms_header}}
									</h2>
									<span class="byline">
									{{rooms}}
									</span>
								</header>

							</section>
							<!-- map -->
							<section>
								<!-- Panels -->
								<header>
									<h2>
									{{panels_header}}
									</h2>
								</header>
								<p>
								{{panels}}
								</p>
								<p>
								{{searchForm}}
								</p>
							</section>

							<!-- Panels -->
					<!-- Sidebar -->
					</div>
					<!-- Content -->
					<div id="content" class="8u skel-cell-important">
							<section id="head">
								<header>
									<h2>
										{{content_header}}
									</h2>
								</header>
								{{image}}
								<p>
								{{arrows}}
								</p> URL:
								{{url}}
								<br>
								{{file_version}}
								{{print}}
							</section>

							<section id="body">
								{{timeline}}
								{{geo}}
								{{body}}
								{{copy}}
							</section>
							<section>
							<span class="bottom_panels">
								{{panels}}
							</span>
							</section>
					</div>
					<!-- /Content -->


			</div>
		</div>
	<!-- Main -->

	<!-- Footer -->
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="3u">
						<section>
						<header>
								<h3>
								{{randomPanelsTitle}}
								</h3>
							</header>
						{{randomPanels}}
						</section>
					</div>
					<div class="6u">
						<section>
							<header>
								<h3>
								{{headercontent}}
								</h3>
							</header>
							<p>

							</p>
							{{credits}}
						</section>
					</div>
					<div class="3u">
						<!-- section>
						{{searchForm}}
						</section -->
					</div>
				</div>
			</div>
		</div>
	<!-- Footer -->

	<!-- Copyright -->
		<div id="copyright">
			<div class="container">
				{{footer}}
			</div>
		</div>

	</body>
</html>

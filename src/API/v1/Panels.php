<?php
/*
 * @name Panel
 * @package CodeSHOW
 * @author Stefano Penge
 * @copyright Codexpo.org
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
namespace API\v1;

/* returns a list of field for selected repository */
class Panels extends API {



var $format;
var $expo;
var $data;



/**
 * getData
 *
 * @param  string $format
 * @param  string $currentRoom
 * @param  string $currentPanel
 * @return void
 */
function getData($format, $currentRoom = '', $currentPanel = ''){
    // Returns all panels for a room
    $this->format = $format;
    $neighbours = array();
    $rooms = $this->expo->getAdjacencyList(); 
    if (array_key_exists(ucfirst($currentRoom),$rooms)){ 
        $neighbours = $rooms[ucFirst($currentRoom)];
        $panels = $this->expo->getAllPanels($currentRoom);
        $data = array('results'=> array('room'=>$currentRoom, 'neighbours'=>$neighbours,'panels'=>$panels));
    }   else {
        $this->setError('Missing room parameter');
        $data = array('error'=>$this->getError());
    }        
    $data = array_merge($this->data,$data); 
	die($this->formatResult($data)); 
}

}// end class Panel

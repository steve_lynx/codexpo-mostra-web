<?php
/*
 * @name SearchDataset
 * @package IPOCAD
 * @author Stefano Penge
 * @copyright IPOCAD
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
namespace API\v1;
use Tools;

class Find extends API {

var $action = 'find';
var $format = "";
var $data;	


/**
 * getData
 *
 * @param  string $format
 * @param  string $currentRoom
 * @param  string $currentPanel
 * @return void
 */
function getData($format, $currentRoom, $currentPanel=''){
    // search the lexicon (if exists) for a term

    $lexiconFile = ADAFMW_CLASSES_DIR.'Language/'.LANGUAGE."/Lexicon/Index.csv";
    if (file_exists($lexiconFile)){
        $this->format = $format;
        $lexicon = new Tools\Lexicon();
        $found = $lexicon->searchWord($lexiconFile,$currentRoom,'partialMatch'); 
        if (count($found)>0){
               $data = array('results'=>$found);
        }   else {
            $this->setError('Termine non trovato');
            $data = array('error'=>$this->getError());
        }
    } else {
        $this->setError('File indice non trovato');
        $data = array('error'=>$this->getError());
    }
    $data = array_merge($this->data,$data); 
	die($this->formatResult($data)); 
}

} // end class Find

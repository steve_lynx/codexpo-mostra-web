<?php
/*
 * @name Dataset
 * @package CodeSHOW
 * @author Stefano Penge
 * @copyright Codexpo.org
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
namespace API\v1;

class Room extends API {

var $format;
var $expo;
	


/**
 */
function __construct(){
    parent::__construct();
}


/**
 * getData
 *
 * @param  mixed $format
 * @param  mixed $currentRoom
 * @param  mixed $currentPanel
 * @return void
 */
function getData($format, $currentRoom='', $currentPanel = ''){
	
	$data = $this->expo->getRooms();		
	/*
		$label = \Tools\Utils::translate("Messaggio:");
		$content = \Tools\Utils::translate("Formato non riconosciuto");
		$data = array(0, array($label,$content));
	*/
	die($this->formatResult($data)); 
} // getData function

}// end class Rooms

<?php
/*
 * @name API
 * @package CodeSHOW
 * @author Stefano Penge
 * @copyright Codexpo.org
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

namespace API\v1;

/* add a single record */
class API {

var $index;
var $expo;
var $room;
var $panel;
var $language = 'it';
var $error;
var $data = Array();


const NO_ERROR = 0;
const GENERAL_ERROR = 3;



/**
 * __construct
 */
function __construct(){
	$definedConstantsAr = get_defined_constants(true); 

	// Index: a JSON or an XML file 
	$indexFile =  $GLOBALS['configManager']->getConfigValue('index_file');    
    $this->index = $indexFile;
    $this->expo = new \Expo\Expo($indexFile,$this->language);
	$this->data = array('version'=>VERSION,'host'=>HTTP_ROOT_DIR);

}

/* Not used (yet)
function checkPassword($pwd){
	$answer = '';
	if (is_null($pwd) OR ($pwd <> $GLOBALS['configManager']->getConfigValue('author_password'))){
		$errorCode = self::WRONG_PASSWORD;
		$this->setError("Accesso negato.", $errorCode);
		$answer =  $this->getError(); 
	}
	return $answer;
}
*/

/**
 * getResult
 *
 * @param  string $message
 *
 * @return string
 */
function getResult($message){
// just a translater&formatter
	$s = new \View\HTML\IElement('span');
	$s->setElement(\Tools\Utils::translate( $message));
	return $s->getElement();
}


/**
 * setError
 *
 * @param  string $errorMessage
 * @param  int $errorCode
 *
 * @return void
 */
function setError($errorMessage, $errorCode = 0){
	// errorCode is not used
	if (!empty($this->error))
		$this->error = $errorMessage;
	else
		$this->error.= $errorMessage; // @FIXME should be an array

}

/**
 * getError
 *
 * @return array
 */
function getError(){
	if (!empty($this->error)){
		$answer = array('error'=> \Tools\Utils::translate($this->error));
		return $answer; 
	} else
		return null; 
}

function formatResult($data) {

	switch ($this->format) {
		case 'XML':
			//var_dump($data); exit();
			$expObj = new \Tools\XMLGenerator($data);
			echo(header('Content-type: text/xml'));
			$answer = $expObj->getData();
			break;

		case 'JSON': // pay attention to the fact that private properties cannot be encoded !!!
		default:
			$expObj = new \Tools\JSONGenerator($data); 
			echo (header('Content-Type: application/json'));
			$answer = $expObj->getData();
	}		
			 
	return $answer;

}

function formatError(){
	$data = array_merge($this->data,$this->getError());
	die($this->formatResult($data));
}


} // end class API
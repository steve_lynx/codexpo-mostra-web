<?php
/*
 * @name Panel
 * @package CodeSHOW
 * @author Stefano Penge
 * @copyright Codexpo.org
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
namespace API\v1;

/* returns a list of field for selected repository */
class Panel extends API {



var $format;
var $expo;
var $data;





/**
 * getData
 *
 * @param  mixed $format
 * @param  mixed $currentRoom
 * @param  mixed $currentPanel
 * @return void
 */
function getData($format, $currentRoom ='', $currentPanel =''){
    
 // return html data inside a JSON dictionary
    $this->format = $format;
    
    if (($currentPanel!='') && ($currentRoom!='')){
        // This versione returns the  HTML rendered version
        // $text = $this->expo->getPanel($currentRoom,$currentPanel,TRUE);
        // This version returns the original HTML text
        $text = $this->expo->getPanel($currentRoom,$currentPanel,FALSE);
        $data = array('results'=>array('room'=>$currentRoom, 'panel'=>$currentPanel,'text'=>$text));
    }  else {
        $this->setError('Missing parameters');
        $data = array('error'=>$this->getError());
    }        
    $data = array_merge($this->data,$data); 
	die($this->formatResult($data)); 

}

}// end class Panel

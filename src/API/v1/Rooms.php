<?php
/*
 * @name Dataset
 * @package CodeSHOW
 * @author Stefano Penge
 * @copyright Codexpo.org
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
namespace API\v1;

class Rooms extends API {

var $format;
var $expo;
var $data;	




/**
 * getData
 *
 * @param  string $format
 * @param  mixed $currentRoom
 * @param  mixed $currentPanel
 * @return void
 */
function getData($format, $currentRoom='', $currentPanel = ''){
	// Returns all the rooms and panels
	// = the index 
	// $rooms = $this->expo->getAllRoomsAndPanel(); 
	// Returns only rooms, but with adiacent
	$rooms = $this->expo->getAdjacencyList(); 
	$data = array('results'=>array('index'=>$rooms));	
	$data = array_merge($this->data,$data);
	die($this->formatResult($data)); 
} // getData function

}// end class Rooms

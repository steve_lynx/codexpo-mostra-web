<?php
namespace Plugin;

class ImageZExpand extends Plugin  {
// this ugly name is needed to ensure that this filter is applied AFTER ImageRemap filter
    public $filters = array(
        'pattern' => '/<img src="([:\/a-zA-Z0-9\-_. ]+)" alt="([a-zA-ZZàèéìòù00-9\', ]+)" title="([:\/a-zA-Z0-9=?_\-. ]+)">/',
        'replacement' => '<figure><a href="${1}" target="_blank"><img src="${1}" alt="${2}" title="${3}"></a><figcaption>Source: <a href="${3}">${3}</a></figcaption></figure>'
    );
    public function filterText($aText){
        return preg_replace($this->filters['pattern'], $this->filters['replacement'], $aText);
    }
  
}


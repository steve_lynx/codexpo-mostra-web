<?php
namespace Plugin;

class Youtube extends Plugin  {

    public $filters = array(
        'pattern' => '/<youtube>([a-zA-Z0-9\-]+)<\/youtube>/',
        'replacement' => '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/${1}" title="Youtube" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
    );
    public function filterText($aText){
        return preg_replace($this->filters['pattern'], $this->filters['replacement'], $aText);
    }
  
}


<?php
namespace Plugin;

class Wikipedia extends Plugin  { 

    public $filters  = array(
        array(
            'pattern'=>'/<wikipedia>([a-zA-Z_ ]+)<\/wikipedia>/',
            'replacement'=>'<a href="https://it.wikipedia.org/wiki/$1" target="_blank">${1}</a>',
        ),
        array (
            'pattern'=>'/<wikipediaEn>([a-zA-Z0-9_ ()]+)<\/wikipediaEn>/',
            'replacement'=>'<a href="https://en.wikipedia.org/wiki/$1" target="_blank">${1}</a>',
        ),
       /*  and so on....
       array (
            'pattern'=>'/<wikipediaFr>([a-zA-Z0-9_ ]+)<\/wikipediaFr>/',
            'replacement'=>'<a href="https://fr.wikipedia.org/wiki/$1" target="_blank">${1}</a>',
        ),
        */
    );

    public function filterText($aText){
        foreach ($this->filters as $filter){
            $aText = preg_replace($filter['pattern'], $filter['replacement'], $aText);
        }
        return $aText;
    }
  
}


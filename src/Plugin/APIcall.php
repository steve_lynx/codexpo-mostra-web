<?php
namespace Plugin;

class APIcall extends Plugin  {

    public $filters = array(
        'pattern'=>'/<API>([a-zA-Z_ ]+)<\/API>/',
        'replacement' => '',
    );

    public function filterText($aText){
        /*
        In this case we perform three actions:
			1. Build of the call to the API
			2. Execution of the call to the endpoint
			3. Substitution with the result JSONdecoded 
        */           
        $result = $aText;
        preg_match($this->filters['pattern'], $aText,$matches);
		if ($matches[0]){
			$replacement = json_decode(file_get_contents(API_ROOT.'/'.$matches[0]));
			$result = preg_replace($this->filters['pattern'], $replacement, $aText);
		}			

        return $result;

    }
  
  
}


<?php
namespace Plugin;

class Plugin {
/*
    Parent class for element substitution in texts. 
    Example:
	    <codeshow>room</codeshow>
	    <wikipedia>term</wikipedia> (italian version)
	    (also <wikipediaEn>term</wikipediaEn> (english version))
	    <youtube>yt code</youtube> (just the id, not the entire URL...)
    These elements are substitued using a regular expression:
        '/<youtube>([a-zA-Z0-9\-]+)<\/youtube>/' => '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/${1}" title="Youtube" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
	    '/<wikipedia>([a-zA-Z_ ]+)<\/wikipedia>/' => '<a href="https://it.wikipedia.org/wiki/$1" target="_blank">${1}</a>',
	    '/<wikipediaEn>([a-zA-Z0-9_ ]+)<\/wikipediaEn>/' => '<a href="https://en.wikipedia.org/wiki/$1" target="_blank">${1}</a>',
	
    The filter(s) is/are defined as array (of array) of 'pattern' and 'replacement' in classes extended from this one.
    Every class may have a filterText() function.
    */
	
    public $filters = array();

    public function getFilter(){
        return $this->filters;
    }

    public function filterText($aText){
       return preg_replace($this->filters['pattern'], $this->filters['replacement'], $aText);
    }
}

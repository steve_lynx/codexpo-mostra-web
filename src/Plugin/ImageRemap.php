<?php
namespace Plugin;

class ImageRemap extends Plugin  {

    public $filters  = array(
        array(
            'pattern'=>'/<img src="([a-zA-Z0-9_\-. ]+)"/',
            'replacement' => '<img src="' . HTTP_ROOT_DIR . '/' . HTML_DIR . '/img/{{currentRoom}}/${1}"',
        ),
        array(
            'pattern'=>'/<img alt="([a-zA-Z0-9, ]+)" src="([a-zA-Z0-9_\-. ]+)"/',
            'replacement' => '<img src="' . HTTP_ROOT_DIR . '/' . HTML_DIR . '/img/{{currentRoom}}/${2}" alt="${1}"'
        )
    );

    public function filterText($aText){
        foreach ($this->filters as $filter){
            $filter['replacement'] = str_replace('{{currentRoom}}',$_SESSION['room'], $filter['replacement']);
            $aText = preg_replace($filter['pattern'], $filter['replacement'], $aText);
        }
        return $aText;
    }
  
}


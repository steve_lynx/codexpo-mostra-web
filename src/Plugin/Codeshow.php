<?php
namespace Plugin;

class Codeshow extends Plugin  {
    public $filters = array(
        'pattern' => '/<codeshow>([a-zA-Z_ ]+)<\/codeshow>/',
        'replacement' => '<a class="codexpo" href="' . HTTP_ROOT_DIR . '/search.php?key=${1}">${1}</a>'
    );

    function filterText($aText){
        // in this case we should translate labels
        return preg_replace_callback(
            $this->filters['pattern'],
            function ($matches) {
                return '<a class="codexpo" href="' . HTTP_ROOT_DIR . '/search.php?key='.$matches[1].'">'.\Tools\Utils::translate($matches[1]).'</a>';
            },
            $aText);
    }
}


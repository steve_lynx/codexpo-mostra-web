<?php
namespace Plugin;

class SWHStories extends Plugin  {

    public $filters = array(
        'pattern'=>'/<swhstories>([a-zA-Z0-9\-]+)<\/swhstories>/',
        'replacement' => '<iframe width="800" height="600" src="https://swh.stories.k2.services/stories/${1}" title="SWH Story" frameborder="0" allowfullscreen><a href="https://swh.stories.k2.services/stories/${1}" target="_blank"></iframe>',
    );
    public function filterText($aText){
        return preg_replace($this->filters['pattern'], $this->filters['replacement'], $aText);
    }
  
  
}


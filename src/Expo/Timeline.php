<?php
/*
 * Timeline.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 *
 */ 
namespace Expo;

class Timeline {
/* Class che si occupa di costruire la timeline dei pannelli */

 var $panels;
 var $timeline;
 var $room;
 var $panel;
 
 /**
  * __construct
  *
  * @author  Stefano Penge stefano@stefanopenge.it
  *
  * @param  Object $expo
  * @param  String $currentRoom
  * @param  String $currentPanel
  * @return void
  */
 function __construct($expo, $currentRoom = "", $currentPanel = "") {
    $index = $expo->index; 
    $panel = new \Expo\Panel($index,$currentRoom, $currentPanel);
    if ($currentRoom == "")
        $panels = $panel->getAllPanels('',TRUE);
    elseif  ($currentPanel == "")
        $panels = $panel->getAllPanels($currentRoom,TRUE);
    else        
        $panels = array($currentRoom.'/'.$currentPanel);
        

    $this->room = $currentRoom;
    $this->panel = $currentPanel;
    $this->buildTimeline($expo,$panels);
 }  
 
 /**
  * buildTimeline
  *
  * @param  Object $expo
  * @param  Array $panels
  * @return void
  */
 function buildTimeline($expo,$panels){
    $tl = array();
    $tl = $expo->getDataAttributesFromPanels(array('time'), 0, $panels);
    $this->timeline = $tl;
 }
 
 /**
  * getTimeLineAsJSON
  *
  * @return String
  */
 function getTimeLineAsJSON(){
    /* 
    From:
        [1][time] = '1951'
        [1][panel] = 'Hopper'
        [1][id] = '22'
        
    To:
        {
            "text":{
                 "headline": "Hopper",
                 "text":""
            },
            "media": {
                "url": "//image.php?panel=Attori/Hopper",
                "caption": "Hopper"
            },
            "start_date": {
                "year":"1951"
            } 
        }    
               
    */
    $JSONdata = '';
    $tl = $this->timeline;

    switch (sizeof($tl)){
        case 0:
            break;
        case 1: {
            $event = $tl[0];
            $JSONdata = '[{name: "Hopper",	date:"1906", img: "'.HTTP_ROOT_DIR.'/image.php?panel=Attori/Hopper"},';
            $JSONdata.= '{name: "'.$event['panel'].'", date: "'.$event['time'].'", img: "'.HTTP_ROOT_DIR.'/image.php?panel='.$event['panel'].'"}]';
            
        }  
            break;
        default: {
            $JSONdata = '{
            "title": {
                "media": {
                    "url": "'.HTTP_ROOT_DIR.'/image.php?panel=Ingresso/Ingresso'.'",
                    "caption": "Timeline",
                    "credit": "&copy; Codexpo.org - KnightsLab"
                },
                "text": {
                    "headiline": "Codeshow Timeline 1900-2020",
                    "text": "A timeline for the Codeshow events"
                }
            },
            "events": [';
        
            // body
            foreach  ($tl as $event){
                $JSONdata.= '{
                "media": {
                    "url": "'.HTTP_ROOT_DIR.'/image.php?panel='.$event['panel'].'",
                    "caption": "",
                    "credit": ""
                },
                "start_date": {
                    "year": "'.$event['time'].'"
                }, 
                "text": {
                    "headline": "<a href=\"'.HTTP_ROOT_DIR.'/'.$event['panel'].'\" target=_self>'.$event['panel'].'</a>",
                    "text": "'.$event['text'].'"
                }
                },';
            // end
            }
            $JSONdata = substr($JSONdata, 0, -1); // get rid of last comma
            $JSONdata.=']}';
        }
    }
    return $JSONdata;
 }
 
 /**
  * getTimeline
  *
  * @return String
  */
 function getTimeline(){
    $html = '';
    $JSONdata = $this->getTimeLineAsJSON(); 
    if ($JSONdata != ''){
        $html ='<div id="timeline1" style="width:500px;"></div>'."\n";
        $html.='<script type="text/javascript">'."\n";
        $html.='var data ='.$JSONdata.';'."\n";
        $html.='TimeKnots.draw("#timeline1", data, {dateFormat: "%B %Y", color: "#696", width:500, showLabels: true, labelFormat: "%Y"});'."\n";
        $html.='</script>'."\n";
    }
    return $html;
 }

}
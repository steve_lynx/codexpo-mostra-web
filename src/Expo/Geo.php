<?php
/*
 * Map.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 *
 */ 
namespace Expo;

class Geo {
/* Class che si occupa di costruire mappa georeferenziata dei pannelli */

 var $panels;
 var $geo;
 var $room;
 var $panel;
 var $centerMapX;
 var $centerMapY;
 
 /**
  * __construct
  *
  * @author  Stefano Penge stefano@stefanopenge.it
  *
  * @param  Object $expo
  * @param  String $currentRoom
  * @param  String $currentPanel
  * @return void
  */
 function __construct($expo, $currentRoom = "", $currentPanel = "") {
    $index = $expo->index; 
    $panel = new \Expo\Panel($index,$currentRoom, $currentPanel);
    if ($currentRoom == "")
        $panels = $panel->getAllPanels('',TRUE);
    elseif  ($currentPanel == "")
        $panels = $panel->getAllPanels($currentRoom,TRUE);
    else        
        $panels = array($currentRoom.'/'.$currentPanel);
        

    $this->room = $currentRoom;
    $this->panel = $currentPanel;
    $this->buildGeo($expo,$panels);
 }  
 
 /**
  * buildGeo
  *
  * @param  Object $expo
  * @param  Array $panels
  * @return void
  */
 function buildGeo($expo,$panels){
    $geo = array();
    $geo = $expo->getDataAttributesFromPanels(array('lat','lon'), 0, $panels);
    $this->geo = $geo;
 }
 
 /**
  * getGeoAsJSON
  *
  * @return String
  */
 function getGeoAsJSON(){
    /* 
    From:
        [1][lat] = '34.23452345'
        [1][lon] = '123.463465'
        [1][panel] = 'Hopper'
        [1][id] = '22'
        
    To:
    [
       [{"name":"Hopper","lat":"34.23452345","lon":"123.463465","img":"Hopper.jpg",},
    ]
    */
    $JSONdata = '';
    
    if (sizeof($this->geo)>0){
        
        $JSONdata = '{ "title": {
            "media": {
                "url": "Codexpo.jpg",
                "caption": "Geo",
                "credit": "&copy; Codexpo.org - Stefano Penge"
            },
            "text": {
                "headline": "Codeshow Map",
                "text": "A map for georeferentiated panels of the Codeshow"
            }
        },
        "places":';
        
        $JSONdata.= "[";
        foreach  ($this->geo as $place){
            $JSONdata.= '{
                "name": "'.$place['panel'].'", ';
            $JSONdata.= '"lat": '.$place['lat'].', ';
            $JSONdata.= '"lon": '.$place['lon'].', ';
            $JSONdata.= '"img": "'.HTTP_ROOT_DIR.'/image.php?panel='.$place['panel'].'",'; 
            $JSONdata.= '"text": "'.$place['text'].'"},';
            $this->centerMapX = $place['lat']; // we will use the last ones
            $this->centerMapY = $place['lon'];
        }
        $JSONdata = substr($JSONdata, 0, -1); // get rid of last comma
        $JSONdata.=']}';
        
    }
     return  $JSONdata;
 }
 
 /**
  * getGeo
  *
  * @return String
  */
 function getGeo($size = 'large'){
    switch ($size){
        case 'small':
            $heigth = '200px';
            $width = '300px';
            break;
        case 'large':
        default:    
            $heigth = '600px';
            $width = '800px';
            break;
    } 
    $html = '
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<!--script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"-->
</script>
<script src="'.HTTP_ROOT_DIR.'/vendor/miomondo/miomondo.js"></script>
   
<style>
    #geomap { 
        height: '.$heigth.'; 
        width: '.$width.';
        display: block;
        position: relative;
       }
    #popupimg { 
        height: 100px;  
       }  
    #popup {
        min-width: 200px;
    }
       
</style>'."\n";
    $JSONdata = $this->getGeoAsJSON(); 
    if ($JSONdata != ''){
        $x =  $this->centerMapX;
        $y =  $this->centerMapY;

      $html.="<script type='text/javascript'>
          
    var geodata = $JSONdata;   
    $(document).ready(function() {
        drawData(geodata); 
    });
</script>";
      $html.="<div id=\"geomap\"></div>";
      $html.="<script type='text/javascript'>
    OSM_URL = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    OSM_ATTRIB = '&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors';
    var osmUrl = OSM_URL,
    osmAttrib = OSM_ATTRIB,
    osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
    var map = L.map('geomap').setView([$x, $y], 2).addLayer(osm);	
</script>";
       
    }
    return $html;
 }

}
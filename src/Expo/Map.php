<?php
/*
 * Map.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 *
 */ 
namespace Expo;

class Map {
/* Class che si occupa di costruire la mappa delle stanze */

 var $rooms;
 var $table;
 
 /**
  * __construct
  *
  * @author  Stefano Penge stefano@stefanopenge.it
  *
  * @param  object $expo
  * @param  string $currentRoomName
  * @param  string $renderingMode
  * @return void
  */
 function __construct($expo, $currentRoomName, $renderingMode = 'table') {

    $index = $expo->index;
    $this->rooms = $expo->roomTable; 
    $routing = $GLOBALS['configManager']->getConfigValue('routing');
    $http_root_dir =  $GLOBALS['configManager']->getConfigValue('http_root_dir');
    $elevatorButtonAr = Array();
    foreach ($this->rooms as $row => $buttonsAr){
        foreach ($buttonsAr as $button){
            if ($button!= ''){
                $label =  ucfirst(\Tools\Utils::translate($button));
                if ($routing == TRUE){
                    if (strpos($currentRoomName,$button) !== false) {
                        $elevatorButtonAr[$row][] = "<a class='elevator-label-current' href='#'><div><span>".$label."</span></div></a>";
                    } else {
                        $elevatorButtonAr[$row][] = "<a class='elevator-label' href='".$http_root_dir."/".ucfirst($button)."'><div><span>".$label."</span></div></a>";
                    }
                } else {
                    $elevatorButtonAr[$row][] = "<a class='elevator-label' href='".$http_root_dir."/index.php?room=".ucfirst($button)."'><div><span>$label</span></div></a>";
                }
            } else {
                $elevatorButtonAr[$row][] = "";
            }
        }
    }
 
    $rowCount = count($this->rooms)-1; 
    $columnCount = count($this->rooms[0])-1; 

    if ($renderingMode == 'table'){
        $this->table =  "<table id='map'>\n";
        $this->table.= "<thead>\n";
        $this->table.= "</thead>\n";
        $this->table.= "<tbody>\n";

        foreach (range(0,$rowCount) as $row){
            $this->table.= "<tr id='$row'>\n";
            foreach (range(0,$columnCount) as $column){
                $this->table.= "<td>".$elevatorButtonAr[$row][$column]."</td>\n";    
            }
        }
  //
        $this->table.= "</tbody>\n";
        $this->table.= "</table>\n";
    } else { // 'div' or whatever else

        $this->table.= "<div class='table'>";
        foreach (range(1,$rowCount) as $row){
            $this->table.= "<div class='column first'>".$elevatorButtonAr[$row][0]."</div>\n";
            foreach (range(0,$columnCount) as $column){
                $this->table.= "<div class='column'>".$elevatorButtonAr[$row][$column]."</div>\n";    
            }
        }
        $this->table.="</div>";
    }
 }

/**
 * getTable
 *
 * @return array
 */
function getTable(){
    return $this->table; 
}

}
<?php
/* 
 * expo.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 * 
 */

namespace Expo;

use Tools\Lexicon;
use Plugin;

class Expo
{

	var $index;
	var $language;
	var $error;
	var $roomTable;
	var $roomsCount;
	var $panelsCount;
	var $filename;
	var $room;
	var $panel;

	/**
	 * __construct
	 *
	 * @author  Stefano Penge stefano@stefanopenge.it
	 *
	 * @param string $indexFile   
	 * @param string $language
	 */
	function __construct($indexFile, $language = 'it')
	{
		$this->language = $language;
		if (file_exists(($indexFile))) {
			$json = file_get_contents($indexFile);
			$this->index = json_decode($json, true);
			$this->room = new Room($this->index);
			$this->panel = new Panel($this->index);
			$adjacencyList = $this->room->getAdjacencyList();
			$axis =  $this->room->getOrderedRooms($adjacencyList, 'sud');
			$this->roomTable = $this->room->getRoomTable($adjacencyList, $axis);
			$this->roomsCount = count($this->room->getAllRooms());
			$this->panelsCount = count($this->panel->getAllPanels());
		} else {
			// FIXME: can't continue! but should rise an error
			$this->setError("Impossibile trovare il file di indice: " . $indexFile);
			\Tools\Log::doLog(LOG_ERRORS, 'Error: ' . $this->getError(), get_class());
			exit();
		}
	}

	

		
	/**
	 * getImageElement
	 *
	 * @param  String $currentRoomName
	 * @param  String $currentPanel
	 * @return String
	 */
	function getImageElement($currentRoomName, $currentPanel)	{
		$filename = "$currentRoomName/$currentPanel";
		$http_root_dir = HTTP_ROOT_DIR;
		/*
		$root = ROOT;
		$path = "$root/$html_dir/img";
		if (file_exists("$path/$currentRoomName/$currentPanel.".DEFAULT_IMAGE_TYPE))
			$filename = "$currentRoomName/$currentPanel";
		else
			$filename = "$currentRoomName/$currentRoomName";
		return "<img class='image' src=\"$path/$filename\">";
		
		*/
		return "<img class='image' src=\"$http_root_dir/image.php?panel=$filename\">";
	}

	/**
	 * getSearchForm
	 *
	 * @param  string $path
	 * @return string
	 */
	function getSearchForm($path)
	{
		$data = array(
			array(
				'id' => 'key',
				'type' => 'text',
				'name' => 'key',
				'size' => '12',
				'maxlenght' => '20'
			),
			array(
				'id' => 'submitbutton',
				'type' => 'submit',
				'name' => 'Submit',
				'value' => \Tools\Utils::translate('Cerca')
			)

		);
		
		$label = \Tools\Utils::translate('Ricerca');
		$method = 'post';
		$target = '/index.php';
		$name = 'search';
		$formName = 'searchForm';
		$f = new \View\HTML\Form($formName, $path . $target, $name, $label, $method);
		$f->setForm($data);
		return $f->getForm();
	}


	
	/**
	 * getSearchResults
	 *
	 * @param  mixed $key
	 * @return void
	 */
	function getSearchResults($key){
		$method = '/Find/'.$key;
		$APIURL = API_ROOT;
		$response = json_decode(file_get_contents($APIURL.$method),true);
		$output = '<ul>';
		if ((!isset($response['error']))AND (!is_null($response['results']))){
			foreach($response['results'] as $result){
				$result = substr($result,0,-5);// strips off .html
				$fileUrl = explode('/',$result);
				$room = $fileUrl[count($fileUrl)-2];
				$panel = $fileUrl[count($fileUrl)-1];
				$output.= "<li><a href='$result'>$room > $panel</a></li>\n";
			}
		} else
			$output.= '<li><a>'.\Tools\Utils::translate("Termine non trovato").'</a></li>';
		$output.= '</ul>';
		
		return $output;
	}

	
	/**
	 * getChangeLanguageLink
	 *
	 * @return void
	 */
	function getChangeLanguageLink(){

		// Change language link
		$languageList = array();
		$dirObj = new \DirectoryIterator(ROOT.'/html/');
		foreach ( $dirObj as $fileInfo) {
			if ($fileInfo->isDir()) {
				if ((!$fileInfo->isDot()) AND ($fileInfo->getFilename()!='img'))
					$languageList[] = $fileInfo->getFilename();
				}
		}

		
		$changeLanguageLink = "<ul>";
		foreach ($languageList as $language){
			$changeLanguageLink.= "<li><a href='".HTTP_ROOT_DIR."/language.php?language=$language'>$language</a></li>";
		}
		$changeLanguageLink.= "</ul>";
		
		return $changeLanguageLink;

	}
	/**
	 * expandElements
	 *
	 * @param  string $htmlCode
	 * @param  string $path
	 * @param  string $currentRoomName
	 * @param  string $currentPanel
	 * @return string
	 */
	static function expandElements(String $htmlCode, String $path, String $currentRoomName, $currentPanel = null)
	{
		// $htmlCode = expandAllPagesAndPanels($htmlCode, $currentRoomName, $currentPanel);
		$htmlCode = self::applyFilters($htmlCode, $path);
		// $htmlCode = self::remapImages($htmlCode);   // it is a filter now
		// $htmlCode = self::expandImages($htmlCode); // it is a filter now
		return $htmlCode;
	}

	/**
	 * expandProprietaryElement
	 *
	 * @param  string $htmlCode
	 * @param  string $path
	 * @return string
	 */
	static function applyFilters($htmlCode, $path)
	{
		/* 
		Substitution of proprietary element with HTML corresponent
		Use the src/Plugin classes
		*/
		// $filters = array('/<codeshow>([a-zA-Z_ ]+)<\/codeshow>/' => '<a class="codexpo" href="' . $path . '/search.php?key=${1}">${1}</a>');
		$filters = array();
		// $pluginList = array('Plugin/Youtube','Plugin/Wikipedia','Plugin/SWHStories');
		$pluginList = array();
		foreach (new \DirectoryIterator(ROOT.'/src/Plugin') as $fileInfo) {
			if (!($fileInfo->isDot())) 
				$pluginList[] = $fileInfo->getBasename('.php');
		}
		/* example:
		$filterObj = new Plugin\Codeshow();
		$filters[] = $filterObj->filterText();
		*/

		foreach ($pluginList as $plugin){
			switch ($plugin) {
				case 'Plugin':
					// $className = $plugin;
					break;
				default:
					$className = 'Plugin\\'.$plugin;	
					$filterObj = new $className();
					$htmlCode = $filterObj->filterText($htmlCode);
			}
			
			
		}
		return $htmlCode;
	}

	/**
	 * executeAPIcall
	 *
	 * @param  string $htmlCode
	 * @return string
	 */
	function executeAPIcall($htmlCode)
	{
		// we don't need a separate function, since now it is a filter 
		
	/*
	
		$pattern = '/<API>([a-zA-Z_ ]+)<\/API>/';
		preg_match($pattern, $htmlCode,$matches);
		if ($matches[0]){
			$replacement = json_decode(file_get_contents(API_ROOT.'/'.$matches[0]));
			$htmlCode = preg_replace($pattern, $replacement, $htmlCode);
		}			
	*/	
		return $htmlCode;
	}

	/**
	 * remapImages
	 *
	 * @param  string $htmlCode
	 * @return string
	 */
	static function remapImages($htmlCode)
	{
	// we don't need a separate function, since now it is a filter 
		/* 
	Remaps local images: from relative path to absolute (eg. html/img/room/)
	
	From: 
	<img src="bug.jpg" .. 
	or:
	<img alt="blabla" src="bug.jpg" ..	
	to:
	<img src="www.codeshow.it/html/img/Attori/bug.jpg" alt="blabla" title="blabla"...
	
	It doesn't affect remote images.
	
	*/

		$dataHa = array(
			'/<img src="([a-zA-Z0-9_\-. ]+)"/' => '<img src="' . HTTP_ROOT_DIR . '/' . HTML_DIR . "/img/" . $_SESSION['room'] . '/${1}"',
			'/<img alt="([a-zA-Z0-9, ]+)" src="([a-zA-Z0-9_\-. ]+)"/' => '<img src="' . HTTP_ROOT_DIR . '/' . HTML_DIR . "/img/" . $_SESSION['room'] . '/${2}" alt="${1}"',
		);
		foreach ($dataHa as $pattern => $replacement) {
			$htmlCode = preg_replace($pattern, $replacement, $htmlCode);
		}

		return $htmlCode;
	}
	/**
	 * expandImages
	 *
	 * @param  string $htmlCode
	 * @return string
	 */
	static function expandImages($htmlCode)
	{
		// we don't need a separate function, since now it is a filter 
		/* Expand <img element to <figure> element.
			Has to be called AFTER remapping of images path 
			Deprecated: it has been substituted by a filter plugin
		*/
		$dataHa = array(
			'/<img src="([:\/a-zA-Z0-9\-_. ]+)" alt="([a-zA-ZZàèéìòù00-9\', ]+)" title="([:\/a-zA-Z0-9=?_\-. ]+)">/' => '<figure><a href="${1}" target="_blank"><img src="${1}" alt="${2}" title="${3}"></a><figcaption>Source: <a href="${3}">${3}</a></figcaption></figure>'

		);
		foreach ($dataHa as $pattern => $replacement) {
			$htmlCode = preg_replace($pattern, $replacement, $htmlCode);
		}
		return $htmlCode;
	}



	/**
	 * expandAllPagesAndPanels
	 *
	 * @param  string $htmlCode
	 * @param  string $currentRoomName
	 * @param  string $currentPanel
	 * @return string
	 */
	function expandAllPagesAndPanels($htmlCode, $currentRoomName, $currentPanel = null)
	{
		/* Finds ALL words that could be a Room title or a Panel title
	* and substitute with <codeshow></codeshow> elements
	* that subsequently will be substitute with a ?search link
	* This implements a sort of autolink function
	* It is switched on by AUTOLINK constant
	* BEWARE: it has unexpected results when names of rooms or panel were found inside names of image file!
	*/
		$autolink =  $GLOBALS['configManager']->getConfigValue('autolink');
		if ($autolink) {
			$index = $this->getAllRoomsAndPanel();
			$myRoom = new Room($index,$currentRoomName);
			$myPanel = new Panel($index,$currentPanel);
			$rooms = $myRoom->getAllRooms();
			$panels = $myPanel->getAllPanels();
			$roomsAndPanels = array_merge($rooms, $panels);
			// var_dump($roomsAndPanels);
			$dataHa = array();
			foreach ($roomsAndPanels as $roomOrPanel) {
				$dataHa['/\b' . $roomOrPanel . '\b\s/i'] = '<codeshow>${0}</codeshow>';
			};
			foreach ($dataHa as $pattern => $replacement) {
				if (($pattern != "/$currentRoomName/") and ($pattern != "/$currentPanel/"))
					$htmlCode = preg_replace($pattern, $replacement, $htmlCode);
			}
		}
		return $htmlCode;
	}


	/**
	 * getIndex
	 *
	 * @return array
	 */
	function getIndex()
	{
		return $this->index;
	}

	/**
	 * setError
	 *
	 * @param  mixed $error
	 * @return void
	 */
	function setError($error)
	{
		$this->error = $error;
	}
	/**
	 * getError
	 *
	 * @return void
	 */
	function getError()
	{
		return $this->error;
	}

	
	
	
	/**
	 * getDataAttributesFromPanels
	 *
	 * @param  Array $types
	 * @param  Int $n
	 * @param  Array $panels
	 * @return Array | Boolean
	 */
	function getDataAttributesFromPanels(Array $types, Int $n = 0, Array $panels = array()){
	/* Generic extractor of data-? attributes
	  $types is an array of one or more of 
	  q questions, a answers, lat, lon, time, swhid, ...
	  Returns an array with this form:
			[1]['q'] = 'Which is the nick name of Grace Hopper?';
			[1]['panel'] = 'Hopper';
			[1]['id'] = 22;
	 It return also the first line of the panel that is used for preview, for example in Timeline	
	*/
		if (count($panels)>0)
			$panelsAr = $panels; 
		elseif ($n > 0)
			// not all panels have data-? attributes, so we get some more panels ($n*3) and cross our fingers...
			$panelsAr = $this->getRandomPanelsAsArray($n*3);
		else 
			return FALSE;

		$results = array();
		$i=0;
		foreach($panelsAr as $panel){
			if (($n ==0) OR (count($results)<$n)){
				$filename = ROOT.'/'.HTML_DIR.'/'.LANGUAGE.'/'.$panel.'.html';
				$panelTextLines = @file($filename); 
				if (($panelTextLines!=FALSE) AND (!is_null($panelTextLines))){
					$heading = $panelTextLines[0]; 
					$firstLne = trim(str_replace('"','',strip_tags($panelTextLines[1])));
					$text = substr($firstLne,0,120)."..."; 
					foreach ($types as $type){
						$result = self::getDataAttribute($type,$heading);
						if ($result!==FALSE){
							$results[$i][$type] = $result;
							$results[$i]['panel'] = $panel;
							$results[$i]['id'] = $i;
							$results[$i]['text'] = $text; // FIXME! it is returned for EVERY attribute
						}
					}
					/*
					// adding excerpt just one time
					$results[$i+1]['text'] = $text;
					$results[$i+1]['panel'] = $panel;
					$results[$i+1]['id'] = $i;
					*/
				}
				
				$i++;
			}
		}
		return $results;
	}
	
	/**
	 * getDataAttribute
	 *
	 * @param  String $type
	 * @param  String $text
	 * @return Array | Boolean
	 */
	static function getDataAttribute(String $type, String $text){
		// extract data-? from text

		$patternQ = '/data-'.$type.'="(.*?)"/';
		if (preg_match($patternQ, $text, $matches))
			return $matches[1];
		return FALSE;	
	}
	
	/**
	 * getQuestionsAndAnswers
	 *
	 * @param  Int $n
	 * @param  Array $panels
	 * @return Array
	 */
	function getQuestionsAndAnswers($n = 10, Array $panels = array()){
		$panels = $this->panel->getRandomPanelsAsArray($n*3); // we should use the SAME panels for Q and A but we want separate arrays
		$type = array('q');
		$questions = $this->getDataAttributesFromPanels($type, $n, $panels);
		$type = array('a');
		$answers =  $this->getDataAttributesFromPanels($type, $n, $panels);
		return array($questions,$answers);
	}

	/**
	 * getCoordinates
	 *
	 * @param  Int $n
	 * @param  Array $panels
	 * @return Array | Boolean
	 */	

	function getCoordinates($n = 0, Array $panels = array()){
		$types = array('lat','lon');
		return $this->getDataAttributesFromPanels($types, $n, $panels);
	}

	function getYears($n = 0, Array $panels = array()){
		$types = array('time');
		return $this->getDataAttributesFromPanels($types, $n, $panels);
	}

	function getSWHid($n = 0, Array $panels = array()){
		$types = array('swhid');
		return $this->getDataAttributesFromPanels($types, $n, $panels);
	}

	/**
	 * getQuestionsAndAnswers
	 *
	 * @return array
	 */
/*
	 function getQuestionsAndAnswers($n = 10, Array $panels = array()){
		// questions and answers are attributes data-q and data-a of h1 element of panels
		if ($n > 0)
		// not all panels have data-q attributes, so we get some more panels and cross our fingers...
			$panelsAr = $this->getRandomPanelsAsArray($n*3);
		elseif (count($panels)>0)
			$panelsAr = $panels; 
		else 
			return FALSE;

		$answers = array();
		$questions = array();
	
		$i=0;
		foreach($panelsAr as $panel){
			if (($n ==0) OR (count($questions)<$n)){
				$filename = ROOT.'/'.HTML_DIR.'/'.LANGUAGE.'/'.$panel.'.html';
				$panelTextLines = file($filename); 
				$heading = $panelTextLines[0]; 
				$question = self::getQuestionFromAttribute($heading);
				if ($question!==FALSE){
					$questions[$i]['question'] = $question;
					$questions[$i]['panel'] = $panel;
					$questions[$i]['id'] = $i;

				}
					
				$answer = self::getAnswerFromAttribute($heading);
				if ($answer!==FALSE){
					$answers[$i]['answer'] = $answer;
					$answers[$i]['panel'] = $panel;
					$answers[$i]['id'] = $i;
				}
					
				$i++;
			}
		}
		return array($questions,$answers);
	
	}
	*/

	/*
	static function getQuestionFromAttribute($text){
		$patternQ = '/data-q="(.*?)"/';
		if (preg_match($patternQ, $text, $matches))
			return $matches[1];
		return FALSE;	
	}

	static function getAnswerFromAttribute($text){
		$patternA = '/data-a="(.*?)"/';
		if (preg_match($patternA, $text, $matches))
			return $matches[1];
		return FALSE;	
	}
	*/

	/**
	 * getStaticQuestionsAndAnswers
	 *
	 * @return array
	 */
	/*
	function getStaticQuestionsAndAnswers($n = 10){
		// this is an old style way, with two separated CSV files
		$questionFile = ROOT."/".HTML_DIR.'/'.LANGUAGE.'/questions.csv';
		$Qobj = new \Tools\CSVImporter($questionFile, $parse_header=false, $delimiter=",", $length=8000);
		$questionsAr = $Qobj->get();
		$questions = array();
		$i = 0;
		foreach($questionsAr as $quest){
			if ($i<$n)
				$questions[$quest[0]] = $quest[1];
			$i++;	
		}

		$answerFile = ROOT."/".HTML_DIR.'/'.LANGUAGE.'/answers.csv';
		$Aobj = new \Tools\CSVImporter($answerFile, $parse_header=false, $delimiter=",", $length=8000);
		$answersAr = $Aobj->get();
		$answers = array();
		$i = 0;
		foreach($answersAr as $ans){
			if ($i<$n)
				$answers[$ans[0]] = $ans[1];
			$i++;
		}
		return array($questions,$answers) ;
	}
	*/
}

<?php
/* 
 * navigator.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 * 
 * This class is used to manage movement among Rooms
 */

namespace Expo;
 
class Navigator {

/**
 * __construct
 *
 * @author  Stefano Penge stefano@stefanopenge.it
 * 
 * @param  object $expoObject
 * @return void
 */
function __construct(Object $expoObject)
{
	$this->expo = $expoObject;
	$this->index = $expoObject->getIndex();
	$this->routing = $GLOBALS['configManager']->getConfigValue('routing');
	
}

/**
 * getPanels
 *
 * @param  array $panels
 * @param  string $roomName
 * @param  string $currentPanel
 * @return string
 */
function getPanels(Array $panels, String $roomName,String $currentPanel){
	// return an OL list

    $http_root_dir =  $GLOBALS['configManager']->getConfigValue('http_root_dir');
	$panelsMenu =  "<ol>\n";
	foreach ($panels as $panelFile){
		$panelLabel = str_replace('_',' ',$panelFile); 
		if ($this->routing == TRUE) {
	
			if ($currentPanel == $panelFile)  
				$panelsMenu.= "<li class='panel-current'><a class='codexpo' href='#'>$panelLabel</a>";
			else				
				$panelsMenu.= "<li class='panel codexpo'><a class='codexpo' href='$http_root_dir/$roomName/$panelFile'>$panelLabel</a>";
			// panel title should be the HTML file name
		} else {
			$panelsMenu.= "<li class='panel'><a class='codexpo' href='index.php?room=$roomName&panel=$panelFile'>$panelLabel</a>";
		}
		$panelsMenu.="</li>\n";

	}
	
	$panelsMenu.= "</ol>";
	return $panelsMenu;	
}


/**
 * getDirections
 *
 * @param  array $links
 * @param  string $currentRoom
 * @return void
 */
function getDirections(Array $links,String $currentRoom){
	return $this->getDirectionsTable($links,$currentRoom);
}

/**
 * getDirectionsTable
 *
 * @param  array $links
 * @param  string $currentRoom
 * @return void
 */
function getDirectionsTable(Array $links,String $currentRoom){
// returns an HTML table 
	$http_root_dir =  $GLOBALS['configManager']->getConfigValue('http_root_dir');
	$nullcell = "<td style = 'visibility:hidden'>_______</td>";
	$currentRoomCell = "<td class='currentRoom rooms'>".\Tools\Utils::translate($currentRoom)."</td>";
	$allDirections = Array('nord','est','sud','ovest');
	$windRose = Array();
	foreach ($allDirections as $direction){
		if (isset ($links[$direction])) {
			$roomName = $links[$direction];
			$label =  \Tools\Utils::translate($roomName);
			if ($this->routing == TRUE) {
				if ($direction == 'est'){ // arrow after link
					$windRose[$direction] = "<td>&nbsp;<a  class='codexpo rooms' href=$http_root_dir/$roomName>$label</a>&nbsp;".Navigator::getUnicodeSymbolForDirection($direction)."</td>";
				} else { // arrow before link
					$windRose[$direction] = "<td>".Navigator::getUnicodeSymbolForDirection($direction)."&nbsp;<a  class='codexpo rooms' href=$http_root_dir/$roomName>$label</a>&nbsp;</td>";
				}
			} else {
				if ($direction == 'est'){ // arrow after link
					$windRose[$direction] = "<td>&nbsp;<a  class='codexpo rooms' href='index.php?room=$roomName'>$label</a>&nbsp;".Navigator::getUnicodeSymbolForDirection($direction)."</td>";
				} else { // arrow before link
					$windRose[$direction] = "<td>".Navigator::getUnicodeSymbolForDirection($direction)."&nbsp;<a  class='codexpo rooms' href='index.php?room=$roomName'>$label</a>&nbsp;</td>";
				}
			}
		} else {
			$windRose[$direction]= $nullcell;
		}
	}
	
	$directionsTable = "<div id='arrows-table'><table>";
	$directionsTable.= "<tr>".$nullcell.$windRose['nord'].$nullcell."</tr>";
	$directionsTable.= "<tr>".$windRose['ovest'].$currentRoomCell.$windRose['est']."</tr>";
	$directionsTable.= "<tr>".$nullcell.$windRose['sud'].$nullcell."</tr>";
	$directionsTable.= "</table></div>";

	return $directionsTable;

}


/**
 * getDirectionsDiv
 *
 * @param  array $links
 * @return string
 */
function getDirectionsDiv(Array $links){
	/* Returns one or more div elements */
    $directions = "<div>\n";
	foreach ($links as $link=>$roomName){
		if ($this->routing == TRUE) 
			$directions.= "<div class='directions' id='$link'>".Navigator::getUnicodeSymbolForDirection($link)." <a class='codexpo rooms' href=".$this->expo->findRoomId($roomName) ."'>".$roomName."</a></div>\n";
		else
			$directions.= "<div class='directions' id='$link'>".Navigator::getUnicodeSymbolForDirection($link)." <a class='codexpo rooms' href='index.php?id=".$this->expo->findRoomId($roomName) ."'>".$roomName."</a></div>\n";
	}
	$directions.= "</div>\n";
    return $directions;
}


/**
 * getUnicodeSymbolForDirection
 *
 * @param  string $link
 * @return string
 */
static function getUnicodeSymbolForDirection(String $link){
	switch    ($link) {
		case 'est': 
		   return "&#10145;";
		   break;
	   case 'ovest': 
		   return "&#11013;";
		   break;
	   case 'nord':
		   return "&#11014;";
		   break;
	   case 'sud':
		   return "&#11015;";
		   break;
	   default : 
		   return "&#10062;";
   
	}
}

/**
 * MainMenu
 *
 * @param  string $currentRoom
 * @param  int $level
 * @return string
 */
function MainMenu(String $currentRoom,Int $level=0){
	/* returns an Unorderd List */
	$http_root_dir = $GLOBALS['configManager']->getConfigValue('http_root_dir');
	$nav = "<ul>";
	foreach ($this->index as $room){
		if ($currentRoom == $room['Name']) 
			$elementClass = " class ='currentRoom' ";
		else
			$elementClass = "";
		if ($this->routing == TRUE) {
			$nav.= "<li><a class='codexpo' href='$http_root_dir/".$room['Name'] ."' $elementClass>".$room['Name']."</a></li>";
		} else {
			$nav.= "<li><a class='codexpo' href='index.php?room=".$room['Name'] ."' $elementClass>".$room['Name']."</a></li>";
		}
		
		if ($level>1){
			$nav.= "<ul>";
			foreach ($room['Link'] as $link=>$value){
				$nav.= "<li>".$link.": ".$room['Link'][$link]."</li>";
			}
			$nav.= "</ul>";
		}
	}	
	$nav.= "</ul>";
	return $nav;
}


} // end class Navigator
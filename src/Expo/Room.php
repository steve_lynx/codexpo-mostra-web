<?php
/* 
 * Room.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 * 
 */

namespace Expo;


class Room {
    
    var $index;
    var $roomId;
    var $roomName;
    var $error;
    
	

	
	function __construct($index, $currentRoom = '')
	{
		$this->index = $index;
		$this->roomName = $currentRoom;
	}

	function getRoom(){
       return  $this->roomName;
    }

	/**
	 * getAllRoomsAndPanel
	 *
	 * @return array
	 */
	function getAllRoomsAndPanel()
	{
		return $this->index; 
	}

/**
	 * getAdjacencyList
	 *
	 * @return array $adjacencyList
	 */
	function getAdjacencyList()
	{
		// E' una versione semplificata dell'indice, che contiene solo le stanze e non pannelli, id, etc.
		// Per ogni stanza sono riportate le stanze adiacenti
		$adjacencyList = array();
		foreach ($this->index as $room) {
			foreach (array('nord', 'sud', 'est', 'ovest') as $direction) {
				if (isset($room['Link'][$direction]))
					$r =  $room['Link'][$direction];
				else
					$r = '';
				$adjacencyList[$room['Name']][$direction] = $r;
			}
		}
		return $adjacencyList;
	}

	/**
	 * getOrderedRooms
	 *
	 * @param  mixed $roomArray
	 * @param  string $direction
	 * @param  string $firstRoom
	 * @return array
	 */
	function getOrderedRooms($roomArray, $direction = 'sud', $firstRoom = '')
	{
		// Restituisce una lista di stanze che sono adiacenti in una sola direzione
		// a partire da una lista di adiacenze
		// Se non è passata la prima stanza, usa la stanza di default 
		if ($firstRoom == '')
			$firstRoom = DEFAULT_ROOM; // $this->index[0]['Name'];
		$orderedRooms = array($firstRoom);
		$room = $firstRoom;
		while ($roomArray[$room][$direction] != null) {
			$orderedRooms[] = $roomArray[$room][$direction];
			$room = $roomArray[$room][$direction];
		}
		return $orderedRooms;
	}

	/**
	 * getRoomTable
	 *
	 * @param  array $roomArray
	 * @param  array $axis
	 * @return array
	 */
	function getRoomTable($roomArray, $axis)
	{
		// Restituisce un array bidimensionale che corrisponde alle posizione fisica delle stanze
		// a partire dall'asse centrale
		$table = array();
		$n = 0;
		foreach ($axis as $room) {
			$n++;
			$table[] =  array($this->getNextRooms($room, $roomArray, 'ovest'), $room, $this->getNextRooms($room, $roomArray, 'est'));
		}
		return $table;
	}
	/**
	 * getNextRooms
	 *
	 * @param  string $room
	 * @param  array $roomArray
	 * @param  string $direction
	 * @return array
	 */
	function getNextRooms($room, $roomArray, $direction = 'est')
	{
		// restituisce (ricorsivamente) '' oppure un array di stanza adiacenti nella stessa direzione
		$nextRoom = $roomArray[$room][$direction];
		if ($nextRoom == null)
			return '';
		else {
			$rooms = $this->getNextRooms($nextRoom, $roomArray, $direction);
			if ($rooms == '')
				return $nextRoom;
			else
				return array($nextRoom, $rooms);
		}
	}

	/**
	 * getNextRoom
	 *
	 * @param  int $roomId
	 * @return array|''
	 */
	function getNextRoom($roomId)
	{
		// restituisce la prossima stanza dell'indice
		$nextId = $roomId + 1;
		if ($this->index[$nextId]['Name'] != null)
			return $this->index[$nextId]['Name'];
		else
			return '';
		// FIXME: should rise an error and return an array 

	}


	/**
	 * getAllRooms
	 *
	 * @return array
	 */
	function getAllRooms()
	{

		// Implicitamente ordinate per id perché così è fatto l'indice
		// ma NON viene effettuato un controllo
		$rooomsAr = array();
		foreach ($this->index as $room) {
			$roomsAr[] = $room['Name'];
		}
		return $roomsAr;
	}
	

	/**
	 * findRoomName
	 *
	 * @param  int $roomId
	 * @return string|null
	 */
	function findRoomName($roomId)
	{
		foreach ($this->index as $room) {
			if ($room['Id'] == $roomId) {
				return $room['Name'];
			}
		}
		return null;
	}

	/**
	 * findRoomId
	 *
	 * @param  string $roomName
	 * @return int|null
	 */
	function findRoomId($roomName)
	{
		foreach ($this->index as $room) {
			if ($room['Name'] == $roomName) {
				return $room['Id'];
			}
		}
		return null;
	}

	/**
	 * findRoomLinks
	 *
	 * @param  int $roomId
	 * @return array|null
	 */
	function findRoomLinks($roomId)
	{
		foreach ($this->index as $room) {
			if ($room['Id'] == $roomId) {
				return $room['Link'];
			}
		}
		return null;
	}
	

	/**
	 * findAdjacentRoom
	 *
	 * @param  mixed $roomId
	 * @param  mixed $direction
	 * @return void
	 */
	function findAdjacentRoom($roomId, String $direction)
	{
		foreach ($this->index as $room) {
			if ($room['Id'] == $roomId) {
				return $room['Link'][$direction];
			}
		}
		return null;
	}


	/**
	 * setError
	 *
	 * @param  mixed $error
	 * @return void
	 */
	function setError($error)
	{
		$this->error = $error;
	}
	/**
	 * getError
	 *
	 * @return void
	 */
	function getError()
	{
		return $this->error;
	}


}

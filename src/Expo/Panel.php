<?php
/* 
 * Room.php
 * 
 * Package: Codeshow
 * Copyright 2021 Stefano Penge <stefano@stefanopenge.it>
 * 
 */

namespace Expo;
use Tools\Lexicon;

class Panel {
    
    var $index;
    var $roomId;
    var $roomName;
    var $panelId;
    var $panelName;
    var $error;
    
	

	
	function __construct($index, $currentRoom = '',$currentPanel ='')
	{
		$this->index = $index;
		$this->roomName = $currentRoom;
        $this->panelName = $currentPanel;
        $this->room = new Room($index,$currentRoom);
	}

/**
	 * findPanels
	 *
	 * @param  int $roomId
	 * @return array|null
	 */
	function findPanels($roomId)
	{
		foreach ($this->index as $room) {
			if ($room['Id'] == $roomId) {
				return $room['Panels'];
			}
		}
		return null;
	}

	/**
	 * getNextPanel
	 *
	 * @param  int $roomId
	 * @param  string $thisPanel
	 * @return array|null
	 */
	function getNextPanel($roomId, $thisPanel)
	{
		// Restitusce un array con il nome della stanza e il pannello seguente nell'ordine dell'indice se esiste
		// oppure un arrray con la prossima stanza e il nome del pannello di default
		// oppure null
		$roomName = $this->room->findRoomName($roomId);
		$panels = $this->findPanels($roomId);
		if ($thisPanel == null)
			$thisPanel = $roomName;
		$m = count($panels);
		for ($n = 0; $n < $m; $n++) {
			$panel = $panels[$n];
			if ($panel == $thisPanel) {
				if ($panels[$n + 1] != null) {
					return array($roomName, $panels[$n + 1]);
				} else { // era l'ultimo...
					$nextRoom = $this->room->getNextRoom($roomId);
					if ($nextRoom !== '')
						return array($nextRoom, $nextRoom);
					else	
						return null; // era l'ultima stanza	
				}
			}
		}
		return  null; // non ha trovato il pannello
	}


	function getNextPanelLink ($roomId, $currentPanel){
		$nextPanelAr = $this->getNextPanel($roomId, $currentPanel);
		$roomName = $this->room->findRoomName($roomId);
		$nextPanelLink = '';
		if ($nextPanelAr!=null){
			if ($nextPanelAr[0]== $roomName)
				$nextPanelLink =  "<br>".\Tools\Utils::translate("Prossimo pannello:")."<a class='codexpo' href='".HTTP_ROOT_DIR."/$roomName/$nextPanelAr[1]'>". \Tools\Utils::translate($nextPanelAr[1])."</a>";
			else
				$nextPanelLink =  "<br>".\Tools\Utils::translate("Prossima stanza:")."<a class='codexpo' href='".HTTP_ROOT_DIR."/$nextPanelAr[0]/$nextPanelAr[1]'>".\Tools\Utils::translate($nextPanelAr[0])."</a>";
		}
		return $nextPanelLink;
	}
	/**
	 * getAllPanels
	 *
	 * @param  string $thisRoom
	 * @param  boolean $withRoom
	 * @return array
	 */
	function getAllPanels($thisRoom = '', $withRoom = FALSE)
	{
		// se non è passata una stanza li ritorna tutti
		// se $withRoom aggiunge la stanza 
		$panelsAr = array();
		foreach ($this->index as $room) {
			if (($thisRoom == '') or ($room['Name'] == $thisRoom))
				if (count($room['Panels']) > 1)
					foreach ($room['Panels'] as $panel) {
						if ($withRoom)
							$panel = $room['Name'].'/'.$panel;
						$panelsAr[] = $panel;
					}
		}
		return $panelsAr;
	}

	/**
	 * findRoomAndPanels
	 *
	 * @param  string $key
	 * @return array|null 
	 */
	function findRoomAndPanels($key) {
		
		// This version returns only 1 results
		// 1. Search for an exact match
		foreach ($this->index as $room) {
			if (strtoupper($room['Name']) == strtoupper($key)) {
				return array($room['Name'], null);
			} else {
				foreach ($room['Panels'] as $panel)
					if (strtoupper($panel) == strtoupper($key)) {
						return array($room['Name'], $panel);
					}
			}
		}
		// 2. If not found, try with a partial match
		foreach ($this->index as $room) {
			if (stristr($room['Name'], $key)) {
				return array($room['Name'], null);
			} else {
				foreach ($room['Panels'] as $panel)
					if (stristr($panel, $key)) {
						return array($room['Name'], $panel);
					}
			}
		}
		return null;
	}
	
/**
	 * findAllRoomAndPanels
	 *
	 * @param  string $key
	 * @return array 
	 */
	function findAllRoomAndPanels($key) {
		
		// This version returns all results
		// Used by API
		$results = Array();
		foreach ($this->index as $room) {
			if (stristr($room['Name'], $key)) 
				$results[] = array('room'=>$room['Name']);
			foreach ($room['Panels'] as $panel){
				if (stristr($panel, $key)) {
					$results[] = array('room'=>$room['Name'],'panel'=>$panel);
				}
			}
		}
		return $results;
	}
	

	/**
	 * getPanel
	 *
	 * @param  string $room
	 * @param  string  $panel
	 * @return array
	 */
	function getPanel(String $room, String $panel, $render){
		// used by API
		// Beware: this functions returns panel CONTENT, not ADDRESS!
		return $this->getText(ROOT, HTML_DIR, HTTP_ROOT_DIR, LANGUAGE, $room, $panel, GIT_ENABLED , GIT_PATH, $render);

	}

	/**
	 * getRandomPanel
	 *
	 * @return string
	 */
	function getRandomPanel()
	{
		$rooms = $this->room->getAllRooms();
		$roomName = $rooms[rand(0, count($rooms) - 1)];
		$roomId = $this->room->findRoomId($roomName);
		$panels = $this->findPanels($roomId);
		$panel = $panels[rand(0, count($panels) - 1)];
		return  "$roomName/$panel";
	}

	/**
	 * getRandomPanels
	 *
	 * @param  int $n
	 * @return array
	 */
	function getRandomPanelsAsArray(Int $n = 10)
	{
		$f = 0;
		$randomPanelsAr = array();
		
		do {
			$roomAndPanel = $this->getRandomPanel();
			$roomAndPanelAr = explode('/',$roomAndPanel);
			$room = $roomAndPanelAr[0];
			$panel = $roomAndPanelAr[1];
			if (file_exists(ROOT . "/".HTML_DIR."/".LANGUAGE."/".$room."/".$panel.".html") and (!in_array($roomAndPanel, $randomPanelsAr))) {
				$randomPanelsAr[] = $roomAndPanel;
				$f++;
			}
		} while ($f < $n);
		return $randomPanelsAr;
	
	}

	function getRandomPanels($n = 6){
		$randomPanelsAr = $this->getRandomPanelsAsArray($n);
		$randomPanels = '<div class="randomPanels">';
		foreach ($randomPanelsAr as $randomPanel) {
			$label =  \Tools\Utils::translate($randomPanel);
			//$randomPanels .= "<a href='" . HTTP_ROOT_DIR . "/$randomPanel'><img src='" . HTTP_ROOT_DIR . "/html/img/$randomPanel.jpg' title='$randomPanel' alt='$randomPanel'></a>";
			$randomPanels .= "<a href='" . HTTP_ROOT_DIR . "/$randomPanel'><img src='" . HTTP_ROOT_DIR . "/image.php?panel=$randomPanel' title='$label' alt='$label'></a>";

		}
		$randomPanels .= '</div>';
		return $randomPanels;
	}



	/**
	 * getText
	 *
	 * @param  string $currentRoomName
	 * @param  string $currentPanel
	 * @param  string $render
	 * @return string
	 */
	

	function getText( $currentRoomName, $currentPanel, $render = TRUE){
		$root = ROOT;
		$html_dir = HTML_DIR;
		$http_root_dir = HTTP_ROOT_DIR;
		$language = LANGUAGE;
		if (isset($_SESSION['language'])) 
			$language = $_SESSION['language'];  // overriding main config value

		$git_enabled = GIT_ENABLED;
		$git_path = GIT_PATH;
		$content = '';
		$result = FALSE;
		if (($git_enabled) AND ($git_path!='')) { // files are remotely pulled from Git Repo
			$panelName = "$currentRoomName/$currentPanel";
			$filename = $git_path . "html/$language/$panelName.html";
			$result = @fopen($filename, 'r');
		} else { // files are taken from local filesystem
			// is it a panel ?
			$panelName = "$currentRoomName/$currentPanel";
			$filename = "$root/$html_dir/$language/$panelName.html";
			$result = @fopen($filename, 'r');
			$this->filename = $filename; // used to get dimension and version 				
		}		
		if ($result != FALSE){
			$content = fread($result, 10000);
			fclose($result);
		} else {
			// try with default language and translate
			$filename = "$root/$html_dir/".LANGUAGE."/$panelName.html";
			$result = @fopen($filename, 'r');
			if ($result != FALSE){
				$content = fread($result, 10000);
				fclose($result);
				$content = 	$this->getExternalTranslation($language,$panelName,$content);
				$this->filename = $filename; // used to get dimension and version 	
			} else {
				$this->setError("Impossibile trovare il file: " . $filename);
				\Tools\Log::doLog(LOG_ERRORS, 'Error: ' . $this->getError(), get_class());
				$text = \Tools\Utils::translate("Pannello non disponibile");
			}	
		} 
		if ($render)
			$text = Expo::expandElements($content, $http_root_dir, $currentRoomName, $currentPanel);
		else
			$text = $content;
		return '<div id="textContent">'.$text.'</div>';
	}


	function getExternalTranslation($language,$panelName,$content){
		$root = ROOT;
		$html_dir = HTML_DIR;
		// Using DeepL API
		require './vendor/deepL_autoload.php';
		$authKey = DEEPL_AUTHKEY;
		$languageFrom = LANGUAGE;
		$languageTo = $language;
		$deepl   = new \BabyMarkt\DeepL\DeepL ($authKey);
		$tagHandling = 'xml';
		$ignoreTags = DEEPL_IGNORE_TAGS;
		$translatedAr = $deepl->translate($content, $languageFrom, $languageTo, $tagHandling,$ignoreTags);
		$translatedText = '';
		$message = '';
		foreach ($translatedAr as $translation){
			if  (!isset($translation['message']))
				$translatedText.= $translation['text'];
			else 
				$message = $translation['message'];    
		} 
		if ($message!=''){
			$filename = "$root/$html_dir/$languageFrom/$panelName.html";
			$this->setError("Impossibile tradurre il file: " . $filename);
			\Tools\Log::doLog(LOG_ERRORS, 'Error: ' . $this->getError(), get_class());
		} else {
			$filename = "$root/$html_dir/$languageTo/$panelName.html";
			$result = file_put_contents($filename,$translatedText);
			if ($result===FALSE){
				$this->setError("Impossibile scrivere il file: " . $filename);
				\Tools\Log::doLog(LOG_ERRORS, 'Error: ' . $this->getError(), get_class());
			}
		}
		return $translatedText;
	}
	function getFileVersion(){
		if (isset($this->filename))
			return \Tools\Utils::translate("Versione:") ."&nbsp;".date("d/m/Y - H:i:s", filemtime($this->filename));
		else
			return '';	
	}


	function getTextDimension($text){
	// returns the dimension in words of the clean text
		return count(Lexicon::getWordList($text));
	}	
	
	

	/**
	 * setError
	 *
	 * @param  mixed $error
	 * @return void
	 */
	function setError($error)
	{
		$this->error = $error;
	}
	/**
	 * getError
	 *
	 * @return void
	 */
	function getError()
	{
		return $this->error;
	}


}

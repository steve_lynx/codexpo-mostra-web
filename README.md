# Welcome to CodeShow Web

© 2021 [Stefano Penge - Codexpo.org](http://codexpo.org/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

CodeShow Web includes:

* [ADA Framework](http://www.lynxlab.com)
* [jQuery](http://jquery.com)



Use and modifications of these libraries must comply with their respective licenses.

## Descrizione

CodeShow WEB è una semplice applicazione web per costruire il sito della Mostra di Codexpo.
Le stanze sono definite nel file *config/json/mostra.json*, insieme ai percorsi possibili e ai pannelli mostrati.


Ogni pannello è composto da: 
- un testo che ha lo stesso nome del pannello
- un'immagine che ha lo stesso nome del pannello
- eventuali altre immagini

I contenuti testuali dei pannelli devono essere scritti in HTML, senza intestazioni.

Per default, ogni stanza mostra l'immagine che ha lo stesso nome e il testo che ha lo stesso nome.

## Caratteristiche


L'applicazione, non usando un db, è molto veloce di per sé, ma consente di attivare una cache, che velocizza ancora di più la navigazione.

Per semplificare la scrittura dei contenuti, è attiva un'espansione automatica di alcuni elementi, come `<codexpo>pagina</codexpo>`, `<wikipedia>pagina</wikipedia>`, `<img="immagine.jpg">`

## Installazione

Per installare:

1. clonare o  scaricare il repository
1. modificare il file *config/path_config.php* con l'indicazione della root directory e del dominio base
2. modificare, se necessario, *config/*label_config.php*


Se si desidera utilizzare la modalità di routing, occorre inserire un file .htaccess nella root dell'applicazione.

Se è attivo il log, nella cartella */log* si trova un file con gli errori e i warning che si verificano.


## Personalizzazione

L'interfaccia è basata sul motore ADAFMW e utilizza template HTML con i campi definiti da {{nome del campo}}. 

Il template predefinito a Monochromed. Si possono modificare i CSS relativi, che si trovano nella cartella *vendor/ada_fmw/template/monochromed/css*

E' possibile attivare altre lingue oltre all'italiano, traducendo le etichette che si trovano in */vendor/adafmw/src/language/it/Vocabulary.php* e traducendo i contenuti dei pannelli.

## Inserimento contenuti

Per modificare le stanze e i pannelli:

1. editare il file *config/json/mostra.json*
1. aggiungere o modificare i file *.html dentro /html/it/nome della stanza
1. aggiungere o modificare i file *.jpg dentro /html/img/nome della stanza)

Altre informazioni sono contenute nel file *tutorial.md*

<?php
/*
* @name Spider
* @package Codexpo
* @author Stefano Penge
* @copyright Codexpo.org
*
*/
use Tools;
use Expo;


/*
 *  1. SETUP
 *
 * */

session_start();

// All configuration files are included from a single meta-config file
require_once 'config/main_config.php';

// Display errors
if (DEVELOP_MODE == TRUE){
    ini_set('display_errors',  "1");
    error_reporting(E_ALL & ~E_NOTICE);
} else  {
    ini_set('display_errors',  "0");
    error_reporting(E_ERROR);
}


// Autoload classes
// ADA FMW
require 'autoload.php';

// DOMPDF
require ROOT.'/vendor/dompdf/autoload.inc.php';


// Load configuration data

// Create the Config Manager object so we can use properties (settable at runtime) instead of constants if we need to
$definedConstantsAr = get_defined_constants(true);
$configManager = new Tools\ConfigManager($definedConstantsAr['user']);
// We get ALL constants as variable using:
extract ($configManager->config);

// Index: a JSON or an XML file
$indexFile =  $configManager->getConfigValue('index_file');
// Source from Git repo?
$git_enabled = $configManager->getConfigValue('git_enabled');
$git_path = $configManager->getConfigValue('git_path');



/*************
 * 2. CONTENT
 *
 * */
$myExpo = new Expo\Expo($indexFile,$language);




// Log

Tools\Log::doLog(LOG_ACCESS,'spider','');


if (
	(isset($_GET['room']) AND ($_GET['room']!='')) AND
	(isset($_GET['panel']) AND ($_GET['panel']!=''))
	)
	{ 
        $panelList[] =  $_GET['room'].'/'.$_GET['panel'];

} else {
	// building ore re-building index from scratch
	$fileList = array();
	$index = $myExpo->index;
	foreach ($index as $room){
	    $roomName = $room['Name'];
	    $panels = $room['Panels'];
	    foreach ($panels as $panelName){
	        $panelList[] =  $roomName.'/'.$panelName;
	    }
	}

	$res = $myExpo->getQuestionsAndAnswers(0,$panelList);
}
if ($res!=FALSE){
    echo "There are ".count($res[0])." questions and ".count($res[1])." answers.<br>";
    foreach ($res[0] as $question){
        $id = $question['id'];
        $found = FALSE;
        foreach ($res[1] as $answer){
            if ($answer['id'] == $id){
                echo "Q: ".$question['question']." A: ".$answer['answer']." -> ".$question['panel']."<br>";
                $found = TRUE;
            }
        }
        if (!$found)
          $missing[] = $question['panel'];
    }
    echo "There were some missing answers from panels:<br>";
    foreach ($missing as $panel)
        echo $panel."<br>";
} else {
    echo "There were errors in getting questions and answer from panels.";
}
	
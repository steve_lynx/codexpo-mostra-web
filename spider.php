<?php
/*
* @name Spider
* @package Codexpo
* @author Stefano Penge
* @copyright Codexpo.org
*
*/
use Tools;
use Expo;


/*
 *  1. SETUP
 *
 * */

session_start();

// All configuration files are included from a single meta-config file
require_once 'config/main_config.php';

// Display errors
if (DEVELOP_MODE == TRUE){
    ini_set('display_errors',  "1");
    error_reporting(E_ALL & ~E_NOTICE);
} else  {
    ini_set('display_errors',  "0");
    error_reporting(E_ERROR);
}


// Autoload classes
// ADA FMW
require 'autoload.php';

// DOMPDF
require ROOT.'/vendor/dompdf/autoload.inc.php';


// Load configuration data

// Create the Config Manager object so we can use properties (settable at runtime) instead of constants if we need to
$definedConstantsAr = get_defined_constants(true);
$configManager = new Tools\ConfigManager($definedConstantsAr['user']);
// We get ALL constants as variable using:
extract ($configManager->config);

// Index: a JSON or an XML file
$indexFile =  $configManager->getConfigValue('index_file');
// Source from Git repo?
$git_enabled = $configManager->getConfigValue('git_enabled');
$git_path = $configManager->getConfigValue('git_path');



/*************
 * 2. CONTENT
 *
 * */
$myExpo = new Expo\Expo($indexFile,$language);




// Log

Tools\Log::doLog(LOG_ACCESS,'spider','');
$lexicon = new Tools\Lexicon();
if (isset($_SESSION['language'])) 
	$language = $_SESSION['language'];  // overriding main config value

$lexiconFile = ADAFMW_CLASSES_DIR."Language/$language/Lexicon/Index.csv";

if (
	(isset($_GET['room']) AND ($_GET['room']!='')) AND
	(isset($_GET['panel']) AND ($_GET['panel']!=''))
	)
	{ // after adding a single page: new rows are added to exixting index
	$filename = ROOT.'/'.HTML_DIR.'/'.LANGUAGE.'/'.$_GET['room'].'/'.$_GET['panel'].'.html';
	
	$res = $lexicon->updateIndex($filename, $lexiconFile);

} else {
	// building ore re-building index from scratch
	$fileList = array();
	$index = $myExpo->index;
	foreach ($index as $room){
	    $roomName = $room['Name'];
	    $panels = $room['Panels'];
	    foreach ($panels as $panelName){
	        $fileList[] =  ROOT.'/'.HTML_DIR.'/'.LANGUAGE.'/'.$roomName.'/'.$panelName.'.html';
	    }
	}

	$res = $lexicon->spider($fileList,	$lexiconFile);
}


// echo file_get_contents($lexiconFile);
if ($res!=FALSE)
	echo "<span>Success: added $res row to index.</span><br><span>Return to <a href=".HOME.">home</a>";
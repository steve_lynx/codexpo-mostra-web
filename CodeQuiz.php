<?php
/* Codexpo
 * quiz.php
 *
 *
 * Copyright 2021 stefano <steve@lynxlab.com>
 *
 */
use API\v1\Search;
use Tools;
use Expo;
use JetBrains\PhpStorm\Language;
use View;

$path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); 
if ($path == "/") {
	header("Location: https://www.codeshow.it/index.html",307);
	exit();
}

session_start();
// Configuration files are included from a single meta-config file
require 'config/main_config.php';

// Display errors
if (DEVELOP_MODE){
    ini_set('display_errors',  "1");
    error_reporting(E_ALL & ~E_NOTICE);
} else  {
    ini_set('display_errors',  "0");
    error_reporting(E_ERROR);
}
// Autoload classes
require 'autoload.php';
// Create the Config Manager object so we can use properties (settable at runtime) instead of constants
$definedConstantsAr = get_defined_constants(true);
$configManager = new \Tools\ConfigManager($definedConstantsAr['user']);
// We get ALL constants as variable using:
extract ($configManager->config);
 
// Index: a JSON or an XML file 
$indexFile =  $configManager->getConfigValue('index_file');


if (isset($_SESSION['language'])) 
	$language = $_SESSION['language'];  // overriding main config value

$myTranslator = new \Tools\Translator($language);

$myExpo = new Expo\Expo($indexFile,$language);
// Routing (if active: codexpo.org/languages/Java)
$myRoute = new Tools\Router($myExpo);

// Caution: these are hardcoded !!! Should you move the quiz panel you should also modify the eoom name
$currentRoomName =  "Uscita";
$currentPanel = "Quiz";

// Here starts the content section (QUIZ)

$numQuestions = 10; // we  would like to have numQuestions questions

if (!isset($_POST['answer'])){

  //$qa =$myExpo->getQuestionsAndAnswers($numQuestions);
  $qa = $myExpo->getQuestionsAndAnswers($numQuestions);  
  if ($qa!= FALSE){
 
    $questions = $qa[0];
    $answers = $qa[1];
    $_SESSION['questions'] = $questions;
    $_SESSION['answers'] = $answers;
    
    $numQuestions = count($questions); //... but finally we have m

    $message =  $myTranslator->getTranslation("Prova a rispondere a queste ");
    $message.= $numQuestions;
    $message.=  $myTranslator->getTranslation(" domande");

    // building the form

    $submitLabel = $myTranslator->getTranslation("Verifica");
    $n = 1;

    $content = "
    <form id=\"quizForm\" action=\"CodeQuiz.php\" method=\"post\">
    <fieldset>
    <legend>$message:</legend>\n";
    foreach ($questions as $id=>$question){
      $content.="<p><label for=\"$id\">$n.". $question['q']."</label>
      <input id=\"$id\" type=\"text\" name=\"$id\" value=\"\"></p>\n";
      $n++;
    }
    $content.="</fieldset>
    <br>
    <input type=\"submit\" class=\"button\" name=\"answer\" value=\"$submitLabel\">
    </form>";
  } else
    $content =  $myTranslator->getTranslation("Errore nel recuperare le domande");
} else {
  
  // control of answers
  $questions = $_SESSION['questions'];
  $answers = $_SESSION['answers'];
  if (count($questions)>0){
    $numQuestions = count($questions); 
    $numAnsweredQuestions = 0;
    $numCorrectAnswers = 0;
    $failedQuestions = array();
    $correctAnswers = array();
    foreach([$_POST][0] as $key=>$value) {
      $numAnsweredQuestions++;
      if (strtoupper($value) == strtoupper($answers[$key]['a'])){
        $numCorrectAnswers++;
      } else {
        $failedQuestions[] = $questions[$key];
        $correctAnswers[] = $answers[$key];
      }

    }
    $numAnsweredQuestions--;
    $rating = $numCorrectAnswers/$numQuestions ; // Classes are 5:  0, 0<r<33%, 33%<r<60%, 60%<r<90%, r>90%
    if ($rating == 0){
      $res =  $myTranslator->getTranslation("Mi dispiace, oggi non sei proprio in forma. Torna domani!");
    } elseif (($rating >0) AND ($rating <= 0.3)){
      $res =  $myTranslator->getTranslation("Forse è il caso che ti fai un altro giro...");
    } elseif (($rating>0.3) AND ($rating<=0.6)){
      $res =  $myTranslator->getTranslation("Bene! Hai esplorato molte stanze con attenzione (non tutte, però...) ");
    } elseif (($rating>0.6) AND ($rating<=0.9)){  
      $res =  $myTranslator->getTranslation("Molto bene! Sei un guru, potresti fare da guida nella mostra");
    } else {  
      $res =  $myTranslator->getTranslation("Ma sei un hacker...Come hai fatto a indovinarle tutte?");
    }

    $content = "<h2>$res</h3>";
    $content.= "<h3>".$myTranslator->getTranslation("Punteggio: "). $numCorrectAnswers. $myTranslator->getTranslation(" su ").$numQuestions."</h3>";
    $stop = count($failedQuestions); 
    if ($stop > 1){
      $content.=  $myTranslator->getTranslation("Non hai risposto esattamente a ");
      $content.= $stop-1;
      $content.=  $myTranslator->getTranslation(" domande. Le risposte corrette erano queste:");
      $content.= "<ul>";
      for ($i = 0; $i<$stop-1; $i++){
        $question = $failedQuestions[$i]['q'];
        $url = $failedQuestions[$i]['panel'];
        $answer = strtoupper($correctAnswers[$i]['a']);
        $content.= "<li><a href='$url'>$question $answer</a></li>";
      }
      $content.= "</ul>";

    } else {
      $content.=   $myTranslator->getTranslation("Non hai sbagliato niente.");
    }
    $content.=  $myTranslator->getTranslation("Torna nella mostra");
    $content.= "&nbsp;<a href=".HOME.">Home</a>";
    unset($_SESSION['questions']);
    unset($_SESSION['answers']);
  } else
    $content =  $myTranslator->getTranslation("Errore nel recuperare le domande");
}

// VIEW

$body = $content;
$content_header =  $myTranslator->getTranslation($currentPanel);

// $mode = $myRoute->getMode();
$routeError = $myRoute->getError(); 
$routes = $myRoute->getRoute(); 
$altLayout =  $myRoute->getLayout();	

$currentRoomId = $myExpo->findRoomId($currentRoomName);

$url =  HTTP_ROOT_DIR.'/'.$currentRoomName.'/'.$currentPanel;
$copy = "<p>".COPY."</p>";
$image = $myExpo->getImageElement($currentRoomName,$currentPanel);

$print = "";
$file_version = "<br>";

// Random rooms
$formatString =$myTranslator->getTranslation("Ci sono %d stanze e %d pannelli");
$randomPanelsTitle = sprintf($formatString,$myExpo->roomsCount,$myExpo->panelsCount)."<br>";
$randomPanelsTitle.=$myTranslator->getTranslation("Visita una stanza a caso...");
$randomPanels = $myExpo->getRandomPanels(8);

// Search form:
$searchForm = $myExpo->getSearchForm($http_root_dir);


$menuMode = Array('Menu','Map','Arrows');

$myNav = new Expo\Navigator($myExpo);
$myMap = new Expo\Map($myExpo,$currentRoomName,'table'); // could also be 'div'

// Menu (also for mobile style!)

if (in_array('Menu',$menuMode))
	$menu = $myNav->MainMenu($currentRoomName,0); 

// Map (=Elevator buttons)
if (in_array('Map',$menuMode))
	$elevator = $myMap->getTable();

// Arrows
if (in_array('Arrows',$menuMode))
	$arrows = $myNav->getDirections($myExpo->findRoomLinks($currentRoomId),$currentRoomName);

$nav = $menu; // these are field names!
$map = $elevator; 
$directions = ""; 

// $arrows = "";

$rooms_header = $myTranslator->getTranslation("Stanze"); 
$rooms = $map;


$panels_header =$myTranslator->getTranslation("Pannelli");
$panels = $myNav->getPanels($myExpo->findPanels($currentRoomId),$currentRoomName,$currentPanel);

// LAYOUT
$self = 'index';

if  ($altLayout == ''){
  if (isset($_SESSION['layout']))
    $layout = $_SESSION['layout'];
  else	
    $layout =  $configManager->getConfigValue('layout');		
} else {
  $layout =  $altLayout ;
  $_SESSION['layout'] = $layout;
}

 
$htmlObj = new View\Page($self, $layout, $template);

// "Automagic" mapping: fields name in template are the same as variable names, so we have to add some 
$fields_array = $htmlObj->getFieldNames();
if (!empty($fields_array)){
	$page_data = @compact($fields_array);
}	     

$htmlObj->setBody($page_data);

// Rendering: 
$htmlObj->output('page');
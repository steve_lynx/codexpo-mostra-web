<?php
/* Codexpo
 * Geo.php
 *
 *
 * Copyright 2021 stefano <steve@lynxlab.com>
 *
 */
use API\v1\Search;
use Tools;
use Expo;
use JetBrains\PhpStorm\Language;
use View;

$path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); 
if ($path == '/') {
	header('Location: https://www.codeshow.it/index.html',307);
	exit();
}

session_start();
// Configuration files are included from a single meta-config file
require 'config/main_config.php';

// Display errors
if (DEVELOP_MODE){
    ini_set('display_errors',  '1');
    error_reporting(E_ALL & ~E_NOTICE);
} else  {
    ini_set('display_errors',  '0');
    error_reporting(E_ERROR);
}
// Autoload classes
require 'autoload.php';
// Create the Config Manager object so we can use properties (settable at runtime) instead of constants
$definedConstantsAr = get_defined_constants(true);
$configManager = new \Tools\ConfigManager($definedConstantsAr['user']);
// We get ALL constants as variable using:
extract ($configManager->config);
 
// Index: a JSON or an XML file 
$indexFile =  $configManager->getConfigValue('index_file');

$myExpo = new Expo\Expo($indexFile,$language);
// Routing (if active: codexpo.org/languages/Java)
$myRoute = new Tools\Router($myExpo);

// Caution: these are hardcoded !!! Should you move the quiz panel you should also modify the eoom name
$currentRoomName =  'Ingresso';
$currentPanel = 'Geo';

$index = $myExpo->index;
$myRoom = new Expo\Room($index,$currentRoom,$currentPanel);
$myPanel = new Expo\Panel($index,$currentRoom,$currentPanel);

// Here starts the content section (Geo)
$geoObj = new Expo\Geo($myExpo,'','');
$content = $geoObj->getGeo('large'); 

// VIEW

$timeline = ''; // no timeline in geopage
$geo  = ''; // no smap geo map in geopage


$body = $content;
$content_header =  \Tools\Utils::translate($currentPanel);

// $mode = $myRoute->getMode();
$routeError = $myRoute->getError(); 
$routes = $myRoute->getRoute(); 
$altLayout =  $myRoute->getLayout();	

$currentRoomId = $myRoom->findRoomId($currentRoomName);

$url =  HTTP_ROOT_DIR.'/'.$currentRoomName.'/'.$currentPanel;
$copy = '<p>'.COPY.'</p>';
$image = $myExpo->getImageElement($currentRoomName,$currentPanel);

$print = '';
$file_version = '<br>';

// Random rooms
$formatString = Tools\Utils::translate('Ci sono %d stanze e %d pannelli');
$randomPanelsTitle = sprintf($formatString,$myExpo->roomsCount,$myExpo->panelsCount).'<br>';
$randomPanelsTitle.= Tools\Utils::translate('Visita una stanza a caso...');
$randomPanels = $myPanel->getRandomPanels(8);

// Search form:
$searchForm = $myExpo->getSearchForm($http_root_dir);


$menuMode = Array('Menu','Map','Arrows');

$myNav = new Expo\Navigator($myExpo);
$myMap = new Expo\Map($myExpo,$currentRoomName,'table'); // could also be 'div'

// Menu (also for mobile style!)

if (in_array('Menu',$menuMode))
	$menu = $myNav->MainMenu($currentRoomName,0); 

// Map (=Elevator buttons)
if (in_array('Map',$menuMode))
	$elevator = $myMap->getTable();

// Arrows
if (in_array('Arrows',$menuMode))
	$arrows = $myNav->getDirections($myRoom->findRoomLinks($currentRoomId),$currentRoomName);

$nav = $menu; // these are field names!
$map = $elevator; 
$directions = ''; 

// $arrows = '';

$rooms_header =  Tools\Utils::translate('Stanze'); 
$rooms = $map;


$panels_header = Tools\Utils::translate('Pannelli');
$panels = $myNav->getPanels($myPanel->findPanels($currentRoomId),$currentRoomName,$currentPanel);

// LAYOUT
$self = 'index';

if  ($altLayout == ''){
  if (isset($_SESSION['layout']))
    $layout = $_SESSION['layout'];
  else	
    $layout =  $configManager->getConfigValue('layout');		
} else {
  $layout =  $altLayout ;
  $_SESSION['layout'] = $layout;
}

 
$htmlObj = new View\Page($self, $layout, $template);

// 'Automagic' mapping: fields name in template are the same as variable names, so we have to add some 
$fields_array = $htmlObj->getFieldNames();
if (!empty($fields_array)){
	$page_data = @compact($fields_array);
}	     

$htmlObj->setBody($page_data);

// Rendering: 
$htmlObj->output('page');
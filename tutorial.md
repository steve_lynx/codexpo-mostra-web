# Mini-tutorial CodeShow

CodeShow è una semplice applicazione web scritta in PHP di tipo flat-file; non ha un vero database, ma si poggia su tre elementi:

+ un indice, che può essere  un file JSON (config/json/mostra.json) o un XML, che contiene i nomi delle stanze, la loro posizione relativa e i nomi dei pannelli;
+ una serie di file html (i pannelli);
+ eventualmente, una serie di immagini associate ai pannelli.

Ciò che permette di navigare tra le stanze e visualizzare i pannelli è il loro *nome* e la loro *posizione*. Su questa base, l'applicazione crea i percorsi e carica testi e immagini relative.


## Stanze

Ogni stanza è identificata da un id progressivo e da un nome. Ogni stanza  deve avere almeno un pannello con lo stesso nome della stanza, ma può avere o no dei pannelli ulteriori (che sono i testi veri e propri). 

Le stanze vengono visualizzate in forma di mappa in base alle proprietà di posizione relativa. Per ogni stanza si può specificare quali sono le quattro stanze adiacenti nelle quattro direzioni cardinali (nord, sud, est, ovest). Questa disposizione è anche usata per muoversi tra le stanze tramite le frecce.

I pannelli vengono visualizzati nell'ordine in cui sono elencati nell'indice JSON.
La funzione "prossimo pannello" segue questo ordine; una volta arrivati all'ultimo pannallo dei una stanza si passa alla prossima stanza, in cui "prossima" è definito dall'ordine delle stanze.

## Testi

I testi dei pannelli devono essere scritti in HTML senza intestazioni (html, head, meta, ...), cioè in pratica solo la parte che normalmente è contenuta tra `<body>` e `</body>`.

Il file HTML deve essere collocato *all'interno della cartella della stanza relativa, sotto la lingua scelta*. Ad esempio, un testo relativo a Arnaud, nella stanza degli Attori, scritto in Italiano, deve andare qui:

`/html/it/Attori/Arnaud.html`

Al file HTML può corrispondere un file JPG con lo stesso nome, che deve stare nella cartella corrispondente alla stanza (le immagini NON dipendono dalla lingua):

`/html/img/Attori/Arnaud.JPG`

Se il file non esiste, viene usata l'immagine standard della stanza, che deve avere lo stesso nome della stanza:

`/html/img/Attori/Attori.JPG`

Ogni file deve iniziare col titolo, che può essere una versione più lunga del nome del file. Ad esempio, se il file si chiama "Hopper.html", il titolo potrebbe essere:

`<h1>Grace Hopper</h1>`

E' una buona pratica assegnare un id univoco all'elemento h1:

`<h1 id="Grace_Murray_Hopper">Grace Hopper</h1>`

Perché i pannelli vengano visualizzati nell'indice della stanza e trovati con la ricerca, il loro nome deve anche essere inserito nelll'indice, ovvero nel file "mostra.json", in corrispondenza dell'elemento relativo:

```
{
	"Name":"Attori",
	"Id":3,
	"Link":{"est":"Contesti","sud":"Genere"},
	"html":["Attori","Hopper"]
}
```

In caso contrario, i pannelli possono comunque essere linkati nel testo ma *non* verrano indicizzati né verranno trovati con la ricerca.

Se si crea un pannello nuovo, si può inserire in fondo al testo il proprio nome:

`<pre>A cura di: tuo nome</pre>`
## Link


Se il testo è lungo, si può inserire in fondo al testo un link che riporta su:

`<a href="#top">Torna su</a>`

Sempre se il testo è lungo, e si vuole separare in sezioni, si possono usare le àncore e inserire subito sotto al titolo un "indice" delle àncore:

`<a href="#prima">Prima parte</a>&nbsp;|&nbsp;<a href="#seconda">Seconda parte</a>`


Per i link interni, se si tratta di pannelli nella stessa stanza è sufficiente inserire il nome del pannello come HREF:

`<a href="pannello">Link al pannello</a>`

Se invece si tratta di un pannello di altra stanza occorre specificarlo:

`<a href="../stanza/pannello">Link al pannello di un'altra stanza</a>`

Esiste però una maniera più semplice, vedi sotto (Espansioni).

Se è necessario inserire link esterni (cioè a siti web diversi dalla mostra), ricordarsi di aggiungere l'attributo *target="_blank"* per evitare che l'utente si ritrovi in un altro sito e si perda.


## Espansioni

Una maniera semplice di creare i link interni  è quello di attivare i link automatici. In pratiac, posi link vengono creati automaticamente ogni volta che si incontra nel testo un nome di stanza o di pannello:

`il commodoro Hopper è ricordato per`

diventa:

`il commodoro <a href=search.php?key=Hopper>Hopper</a> è ricordato per`

ovvero si traducono in una ricerca generica del termine  "Hopper" all'interno dell'indice (attenzione: non vengono cercati i pannelli non indicizzati). La ricerca è effettuata sia a livello di stanze che di pannelli. 

_Attenzione al fatto che le parole vengono trasformate in link solo se sono isolate, cioè non contigue a segni di interpunzione._ 

Inoltre questa funzione di autolink può creare problemai se ci sono nomi di stanze o pannelli all'interno di immagini o di altri link. Per default, questa funzione è disabilitata; per riabilitarla è necessario intervenire sul file di configurazione.

Se si vuole creare manualmente un link ad un pannello  si può usare la forma contratta:

`il commodoro <codeshow>Hopper</codeshow> è ricordato per`

che viene espansa al momento del caricamente del file in:

`il commodoro <a href=search.php?key=Hopper>Hopper</a> è ricordato per`

E' possibile anche inserire manualmente un link contratto verso una pagina di Wikipedia italiana:

`<wikipedia>Grace_Murray_Hopper</wikipedia>`

che viene espanso a runtime in un link del tipo:

`<a href="https://it.wikipedia.org/wiki/Grace_Murray_Hopper>Grace_Murray_Hopper</a>`

Anche un questo caso, il risultato è una ricerca all'interno di Wikipedia.

Se si preferisce linkare la parola alla pagina della versione in lingua inglese di Wikipedia (perché quella italiana non esiste o è ancora in bozza), si può usare invece:

`<wikipediaEN>Grace_Murray_Hopper</wikipediaEN>`

Altra espansione comodo è quella per i video su Youtube. Il codice seguente:

`<youtube>1IewbyuOVlI</youtube>`

viene espanso in un iframe che punta al player Youtube integrato:

```
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/${1}" title="Youtube" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

Un'altra espansione possibile è relativa alle immagini. Il collegamento alle immagini interne, che devono trovarsi nella cartella *html/img/nome della stanza*, può essere inserito nella forma contratta:

`<img src="bug.jpg" alt="Primo errore" title="http://myoldmac.net/">`

che viene espansa automaticamente in base alla stanza in cui ci si trova:

`<img src="html/img/Attori/bug.jpg" alt="Primo errore" title="http://myoldmac.net/">`

e inoltre viene inserito all'interno di un elemento *figure*:

```
<figure>
    <img src="html/img/Attori/bug.jpg"
         alt="Primo errore">
    <figcaption>Fonte: http://myoldmac.net/</figcaption>
</figure>
```
Lo stesso vale per le immagini esterne:

`<img src="https://www.girlgeeklife.com/wp-content/uploads/2012/10/500px-Ada_Lovelace_color.svg_.png" alt="ADA Lovelace initiative" title="https://www.girlgeeklife.com">`

diventa 
```
<figure>
    <img src="https://www.girlgeeklife.com/wp-content/uploads/2012/10/500px-Ada_Lovelace_color.svg_.png"
         alt="ADA Lovelace initiative">
    <figcaption>Fonte: https://www.girlgeeklife.com</figcaption>
</figure>
```
 in cui il contenuto della caption deriva dall'attributo *title* .
## Media

Se si desidera inserire dei video, ad esempio da Youtube, si può creare un iframe, così:

`<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LR8fQiskYII?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`


## PDF

E' possibile esportare ogni pannello in PDF; i file PDF una volta renderizzati vengono anche salvati nella cartella PDF per alleggerire il carico di elaborazione.

Il PDF viene generato usando un template e un foglio stile appositi, che possono essere modificati per ottenere una diversa impaginazione.


## Cache

E' disponibile una cache delle pagine. Se è attiva, è possibile che le modifiche effettuate ai file dei pannelli non vengano visualizzate immediatamente, a seconda della modalità di cache selezioanta. 

## Indice lessicale

La ricerca normale viene effettuata sul NOME delle stanze o dei pannelli, e non sul loro contenuto. Se si desidera utilizzare la funzione di ricerca full-text occorre ricostruire l'indice ad ogni modifica o aggiunta di un nuovo pannello.

La ricostruzione si ottiene chiamando il servizio spider.php che sovrascrive l'indice e mostra il risultato a schermo.

Un esempio di ricerca full-text si può vedere [qui](https://www.codeshow.it/search.php).

La ricerca restituisce una lista di stanze e pannelli che contengono la parola ricercata. La parola deve essere più lunga di 4 lettere. 
Questa pagina utilizza le API (vedi sotto).

## API

Sono disponibili delle web API per utilizzare i contenuti della mostra con un'interfaccia diversa o con una logica di esplorazione diversa.
Tutti i metodi appartengono ad un unico endpoint, che è /API/v1. 
Sono disponibile attualmente questi metodi:

- /API/v1  oppure API/v1/rooms Restituisce l'elenco delle stanze attuali, e per ognuna le stanza adiacenti
- /API/v1/Concetti  oppure /API/v1/Concetti/panels Restituisce le informazioni su una stanza, compresi i pannelli della stanza (in questo caso, Concetti)
- /API/v1/Concetti/Programmi</a> Restituisce il contenuto di un pannello (in questo caso, Programmi della stanza Concetti)
- /API/v1/Search/Hop Restituisce i risultati di una ricerca del NOME di una stanza o di un pannello che contenga la stringa "Hop"
- /API/v1/Find/Hopper Restituisce i risultati di una ricerca della stringa "Hopper" nel TESTO di tutti i pannelli 
- /API/v1/randomRoom Restituisce l'indirizzo di una stanza e un pannello a caso;

I dati sono restituiti in JSON. Ad esempio: 
```
{
	"version": "1.0",
	"host": "http:\/\/localhost\/codexpo",
	"results": [
		"http:\/\/localhost\/codexpo\/Linguaggi\/COBOL.html",
		"http:\/\/localhost\/codexpo\/Genere\/Un_lavoro_da_donne.html",
		"http:\/\/localhost\/codexpo\/Genere\/Le_donne_sotto_i_riflettori.html",
		"http:\/\/localhost\/codexpo\/Attori\/Hopper.html"
	]
}
```

Gli eventuali contenuti dei pannelli sono restituiti in HTML, esattamente come sono, senza nessuna formattazione speciale. Sta al progettista dell'app decidere come presentarli al visitatore.


Un esempio di uso delle API è il gioco [CodeCAVE](https://www.stefanopenge.it/codecave) che usa le API per ottenere la struttura della mostra, la conformazione delle stanze e il contenuto dei pannelli, e usa il tutto per un gioco che è un omaggio alle avventure testuali degli anni '70.'



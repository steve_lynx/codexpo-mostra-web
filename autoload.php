<?php
/** 
 * @name Autoload
 * @package IPOCAD
 * @author Stefano Penge
 * @copyright IPOCAD
 * 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 

 
/**
 * LoadClass
 *
 * @param  mixed $className
 *
 * @return void
 */
function loadClass($className)
{
    // Generic classes
    $classPath = ADAFMW_CLASSES_DIR .str_replace('\\', '/', $className);
    $classFile = $classPath . '.php'; 

    if (file_exists($classFile))
        require $classFile;
    else
        return false;
 }
 spl_autoload_register('loadClass'); 
 
 
/**
 * LoadAction
 *
 * @param  mixed $className
 *
 * @return void
 */

function loadAction($className)
{
    // Action classes
    $actionPath =  APP_ACTIONS_DIR .str_replace('\\', '/', $className) ;
    $actionFile = $actionPath. '.php';

    if (file_exists($actionFile))
      require $actionFile; 
    else
        return false;
}
spl_autoload_register('loadAction'); 



 
/**
 * LoadAction
 *
 * @param  mixed $className
 *
 * @return void
 */

function loadFilter($className)
{
    // Action classes
    $filterPath =  APP_ACTIONS_DIR.'Plugin/'.$className;
    $filterFile = $filterPath. '.php';

    if (file_exists($filterFile))
        require $filterFile; 
    else
        return false;
}
spl_autoload_register('loadFilter'); 

 
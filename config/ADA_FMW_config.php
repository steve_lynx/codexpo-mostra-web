<?php
// ADA FMW Config


// Template field format & regex pattern

// Twig style:
define ('REPLACE_FIELD_CODE', "{{field_name}}");
define('REGEX_PATTERN','/\{\{(.*?)\}\}/');

define('JS_AUTOLOAD', TRUE);
define('CSS_AUTOLOAD', FALSE);

define ('ADA_FMW_VERSION', "1.2");
define ('DATE_FORMAT',  "%d/%m/%Y");
define ('SESSION_MODE', 'auto');


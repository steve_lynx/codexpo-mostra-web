<?php
// Export Config

// File format
define('EXPORT_FORMATS', array ('pdf','html'));
define ('DEFAULT_EXPORT_FORMAT','pdf');

// Layout family
define ('LAYOUT-PDF', 'monochromed-pdf');

// Default PDF template file
define ('TEMPLATE-PDF', 'topdf');

// Cache pdf dir
define ('PDF_CACHE_DIR',  "pdf");
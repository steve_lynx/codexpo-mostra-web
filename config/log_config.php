<?php
// Log is implemented though Log class (vendor/adafmw/src/Tools/log.php)
// Don't change the constant below

// LOG_LEVEL Values

define ('NO_LOG', 0); 		// no log at all
define ('LOG_ERRORS', 2);	// only errors catched
define ('LOG_DB', 3);	// any access to storage
define ('LOG_ACCESS', 5);		// all access 
define ('LOG_LEVEL_VALUES', array(LOG_ERRORS,LOG_ACCESS,LOG_DB));

// LOG_TYPE Values
define ('LOG_TO_NULL', 0);
define ('LOG_TO_SCREEN', 2);
define ('LOG_TO_FILE', 3);
define ('LOG_TO_DB', 5); // not implemented
define ('LOG_TYPE_VALUES', array(LOG_TO_SCREEN, LOG_TO_FILE, LOG_TO_DB));

//LOG file values
define ('LOG_DIR',  '/'.APACHE_DIR.'/'.APP_NAME.'/log/');
define ('LOG_ACCESS_FILENAME','log.txt');
define ('LOG_ERROR_FILENAME','error.txt');


// Configurations:

// Default develop configuration: log only errors to file

define ('LOG_LEVEL', LOG_ERRORS);
define ('LOG_TYPE', LOG_TO_FILE);

/*
// Alternate  develop configuration: log storage actions and  errors to screen and file
define ('LOG_LEVEL', LOG_ERRORS*LOG_DB);
define ('LOG_TYPE', LOG_TO_SCREEN*LOG_TO_FILE);
*/


// Counter configuration: log access to a file
/*
 define ('LOG_LEVEL', LOG_ACCESS);
define ('LOG_TYPE', LOG_TO_FILE);
*/

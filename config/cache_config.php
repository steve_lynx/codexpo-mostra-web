<?php

// Cache

// CACHE_MODE: there are 4 possibilities:
define ('NO_CACHE',    		0);		// = always  read contents from  source and dynamically build pages
									// recommended while in development mode
define ('READONLY_CACHE', 	1); 	// = read only: the file is always loaded but never rewritten
									// this is the fastest mode to be set after a while
define ('UPDATE_CACHE',   	2); 	// = static rw: the  content is read from file only if lifetime is > ic_lifetime,  otherwise it is read from source and then written back to file
									// could be a good solution 
define ('FORCE_UPDATE_CACHE', 3); 	// = static rw: the content  is read from DB and then written back to file
									// this is the typical initial mode

// If you decide to activate the cache,  you have to edit the next line: 
//define ('CACHE_MODE',  FORCE_UPDATE_CACHE);
define ('CACHE_MODE',  NO_CACHE);
// This is the time (in seconds) before considering the cached page too old (make sense only with UPDATE_CACHE mode)
define ('IC_LIFE_TIME', 	(24*3600)); // 1 day

define ('NOT_CACHABLE_ACTIONS', Array('upload','search'));
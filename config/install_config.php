<?php

// Version
define ('VERSION','1.2');
// Layout family
define ('LAYOUT', 'monochromed');

// Default template file (if none found with the name of the Action)
define ('TEMPLATE', 'default');

// Routing
//define ('ROUTING', FALSE);
define ('ROUTING', TRUE);

// Default Language
define ('LANGUAGE', "it");

// Default room (if none specified in the URL)
define ('DEFAULT_ROOM',      "Ingresso");

// Default panel (if none specified in the URL)
define ('DEFAULT_PANEL',     "Ingresso");

// Show edit links?
define('EDITING_ACTIVE', TRUE);

// Autolinking feature (wikimedia like)
define('AUTOLINK',FALSE);

// Develop mode = TRUE is used to see debuginfo; should be set to FALSE in production environement
define('DEVELOP_MODE', TRUE);

// Maintenance  mode = TRUE is used to see only a message
define('MAINTENANCE_MODE', FALSE);

define('MAINTENANCE_MODE_MESSAGE','Il sistema &egrave; attualmente in manutenzione.');

// Load from Git instead of local files
define('GIT_ENABLED', FALSE);

// Default image type
define ('DEFAULT_IMAGE_TYPE',     "jpg");

// DeepL API
define('DEEPL_AUTHKEY','10138155-2718-a0e9-92e2-747e3c6083ae:fx');

// DeepL Ignore Tags
define ('DEEPL_IGNORE_TAGS',array('codeshow','h1','pre','wikipediaEn','wikipedia'));
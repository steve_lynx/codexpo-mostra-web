<?php
// You can safely edit these constants
// Header:
define ('TITLE', "CodeShow");
define ('TITLE1', "La prima Mostra del Codice Sorgente");
define ('SUBTITLE', "La prima Mostra del Codice Sorgente");
define ('COPY',"&copy; Codexpo.org - Content released under Creative Commons 4.0 BY/SA/NC");
define ('META', "Codexpo, source code, programming, languages, Codefest");
define ('KEYWORDS', "Codexpo, source code, programming, languages, Codefest");
//Body:
define ('FOOTER',  '<img src="'.HTTP_ROOT_DIR.'/img/codefest.png" width="400px" /><img src="'.HTTP_ROOT_DIR.'/img/logo_codexpo.png" width="400px" />');
define ('FOOTER_IMG',  HTTP_ROOT_DIR.'/img/copertina.jpg');
define ('HEADERLINE',  "Mappa");
define ('HEADERCONTENT', "Info");	
define ('DESCRIPTION', "La prima mostra del Codice Sorgente");
define ('DOMAIN', '<a href="'.HTTP_ROOT_DIR.'" target=_blank>CodeShow</a>');
define ('FRONTPAGE', '<a href="'.HTTP_ROOT_DIR.'" target=_blank><img src="'.HTTP_ROOT_DIR.'/img/logo.png" width=200px/></a>');
define ('CREDITS','<p>Idea originale, testi e materiale iconografico: <a href="http://www.codexpo.org">Codexpo.org</a> <br>
Questa versione è stata realizzata in occasione del <a href="https://codefe.st" target="_blank">CodeFest 2021</a>.<br>
<a href="'.HTTP_ROOT_DIR.'/Ingresso/Credits">Crediti</a></p>');

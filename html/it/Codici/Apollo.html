<h1 id="Apollo" data-time="1968" data-q="YUL, il linguaggio con cui è stato scritto il codice dell'Apollo Guidance Computer, era un dialetto di..." data-a="Assembly">Apollo Guidance Computer</h1>
<p>Il programma Apollo è stato un gigantesco sforzo scientifico e tecnico degli USA che negli anni sessanta e settanta ha portato tredici uomini sulla Luna.<br>
Naturalmente per controllare un missile, una navicella spaziale o un modulo di allunaggio - soprattutto quando le onde radio non raggiungono più la base terra, come ci hanno insegnato tanti film di fantascienza  non bastano i piloti umani, ci vogliono i computer.<br>
E i computer hanno bisogno di programmi, che devono essere scritti con molta attenzione perché un errore stupido può portare al disastro.<br>
La storia dell'Apollo Guidance Computer, il sistema di controllo del modulo di comando (Comanche) e del modulo di allunaggio (Luminary) dell'Apollo 11, viene raccontata anche per questo.<br>
Margaret <codeshow>Hamilton</codeshow> aveva già lavorato nei progetti Apollo, prima come programmatrice e poi come team leader. Fin dall'inizio, quando era la più giovane del gruppo, aveva sviluppato un interesse per il tema degli errori. In una delle prime missioni senza piloti era stata incaricata di scrivere un programma che avrebbe dovuto gestire il caso in cui la missione fosse fallita. Siccome questa eventualità non era giudicata significativa, il programma era stato chiamato "Forget It", cioè "Diménticatelo". Naturalmente successe che la missione abortì e il controllo del rientro fu delegato completamente al software scritto da Hamilton.<br>
Per questo, durante la messa a punto del codice sorgente per l'AGC dell'Apollo 11, Hamilton insiste, contro il parere degli altri, per inserire controlli ulteriori per casi speciali, come un errore del pilota o un sovraccarico del computer. Le dicono che gli astronauti non sbagliano mai e che il computer può eseguire 8 processi alla volta - è il computer più avanzato del mondo. <br>
Al momento dell'allunaggio, succede esattamente quello che non doveva succedere: il controllo del segnale radar (che in quella situazione era completamente inutile) occupa un parte importante delle risorse del computer e l'AGC non riesce a reagire agli altri segnali. <br>
Per fortuna, o per bravura, scatta proprio quel controllo inserito da Hamilton e l'AGC esclude il radar, libera le risorse e ricomincia a funzionare.<br>
Hamilton continua a occuparsi di teoria degli errori nel software e in seguito, lasciata la NASA, apre una propria società dedicata proprio a questo studio.<br>
Per saperne di più sull'AGC un ottimo punto di partenza è questo <a href="https://www.ibiblio.org/apollo/AGC_Poster_3%20copia.pdf">Poster</a> di Fabrizio Bernardini, in italiano.<br>
Il codice dell'AGC era sepolto in  cavi metallici (tessuti a mano), che erano stati scelti perché non sarebbero stati modificati dalle condizioni estreme del viaggio nello spazio. E' stato recuperato grazie al lavoro certosino di volontari entusiasti del Virtual AGC e del MIT Museum, che l'hanno prima salvato come immagine, poi convertito in testo, poi corretto e integrato  fino a pubblicarlo su <a href="https://github.com/chrislgarry/Apollo-11" target="_blank">GitHub</a>. In questo modo è diventato possibile andarlo a leggere direttamente, studiarlo, e addirittura costruire un simulatore funzionante dell'AGC, come <a href="https://svtsim.com/moonjs/agc.html" target="_blank">questo</a>, che è una versione in Javascript dell'emulatore originale scritto in C da Ronald Burkey.<br>
La pubblicazione del codice dell'AGC è stata anche l'occasione in cui molti giornalisti hanno potuto scoprire nei commenti al codice delle battute umoristiche, che sono state subito diffuse via Web, come questa:
<pre>
P63SPOT3		CA	BIT6		# IS THE LR ANTENNA IN POSITION 1 YET
			EXTEND
			RAND	CHAN33
			EXTEND
			BZF	P63SPOT4	# BRANCH IF ANTENNA ALREADY IN POSITION 1

			CAF	CODE500		# ASTRONAUT:	PLEASE CRANK THE
			TC	BANKCALL	#	SILLY THING AROUND
			CADR	GOPERF1
			TCF	GOTOPOOH	# TERMINATE
			TCF	P63SPOT3	# PROCEED	SEE IF HE'S LYING

P63SPOT4	TC	BANKCALL		# ENTER INITIALIZE LANDING RADAR
			CADR	SETPOS1

			TC	POSTJUMP	# OFF TO SEE THE WIZARD...
			CADR	BURNBABY
</pre>
Nel linguaggio <a href="https://www.ibiblio.org/apollo/assembly_language_manual.html">YUL</a>, un tipo di Assembly inventato per l'occasione al MIT, la prima colonna rappresenta un blocco di codice, la seconda un istruzione (per esempio, Copy and Add: CA; Jump to address: TCF; Call a subroutine: TC), la terza l'argomento dell'istruzione (per esempio un registro del processore o un indirizzo di memoria, o l'etichetta di un altro blocco, come P63SPOT3). <br>
Dopo il carattere # possono esserci dei commenti che - vista la difficoltà di leggere direttamente il codice - spiegano in inglese cosa fa ogni riga, come promemoria. Ma anche come gesto apotropaico, per scacciare la jella o la paura: i programmatori sapevano bene che gli astronauti che sarebbero saliti sul LEM avrebbe messo la vita nelle loro mani.<br>
E quindi: "Astronauta, per favore gira quella stupida cosa lì intorno"... "Vediamo se sta mentendo"... "Andiamo fuori a vedere il mago". Più sotto si trova: "Astronauta, ora guarda dove sei andato a finire".<br>
Il fatto che i codici sorgenti siano pieni di commenti che testimoniano l'<codeshow>humour</codeshow> dei programmatori non è cosa rara, anzi; ma è sicuramente ignota al mondo esterno, che vede la programmazione come un rito religioso, serissimo, un sacrificio al dio computer. <br>
Naturalmente la quasi totalità dei commenti sono seri, utilissimi sia in fase di scrittura (aiutano il lavoro di squadra, ma sono anche dei promemoria per il singolo autore) che in fase di verifica, di revisione o di sviluppo ulteriore. Quello dei commenti è uno spazio strano: sono scritti in una lingua naturale, spesso l'inglese per poter essere letti dal maggior numero di persone; fanno parte del codice sorgente ma non del programma eseguibile (al momento della compilazione vengono rudemente buttati via). In sostanza, i commenti sono battute di un dialogo di un umano con se stesso o con altri programmatori lontani nello spazio o nel tempo.  <br>
Don Eyles, uno degli sviluppatori del codice di AGC, ha confessato che a quei tempi non gli era mai passato per la testa che qualcuno potesse leggere il codice sorgente. Il fatto che la porzione di programma che avviava i motori si chiamasse "Burn, baby burn!" non sembrava un problema per nessuno.<br>
<br>Don è ancora disposto a raccontare cosa voleva dire scrivere codice per un computer completamente nuovo che non poteva sbagliare. Qui sotto  trovate uno dei suoi racconti, in occasione del CodeFest 2021.<br>
<youtube>ONbDmL2ou1M</youtube>
<a href="#top">Torna su</a>
</p>

<h1 id="Hello, World" data-time="1972" data-q="Qual è il linguaggio in cui per la prima volta viene usato come esempio Hello,world?" data-a="B">Hello, World!</h1>
<p>“Hello, world!” è uno dei <em>luoghi comuni</em> dell’universo della programmazione, nel senso di uno dei topic fondamentali, conosciuti magari solo superficialmente, ma su cui si ritorna in continuazione. Raccontare dell’origine di “Hello, world!”, e poi andare a cercarne le presenze nella cultura contemporanea, è un buon modo per fare un tuffo in questo mondo e rendersi conto di quanto sia ricco, complesso, e non così arido come immagina chi ne rimane fuori.</p>
<p>Banalmente, “Hello, world” (nel seguito: HW) è il nome convenzionale che si dà ad ogni programma che come unico risultato alla fine della sue esecuzione scrive sullo schermo la frase: “Hello, world!”.</p>
<p>E’ di solito il primo esempio che si fa per mostrare le caratteristiche di base di un linguaggio di programmazione all’interno di un manuale, un tutorial, un corso su quel linguaggio. Da un punto di vista puramente quantitativo permette di mostrare <em>quanto</em> codice deve essere scritto per ottenere un risultato in apparenza molto semplice. Perché a seconda del linguaggio scelto,  questa quantità può essere molto diversa. D’altra parte anche per le lingue naturali è noto che ci sono lingue prolisse e lingue concise.<br>Ad esempio, in Pascal – che è un linguaggio didattico, inventato nel 1970 dallo svizzero Niklaus <codeshow>Wirth</codeshow> per insegnare la programmazione strutturata – occorre dichiarare un programma, con un nome, e al suo interno un blocco con un inizio e una fine:</p>
<pre>
 program hello;
 uses crt;
 begin
  writeln('Hello, world');
  readln
 end.
</pre>
<p>La stessa cosa succede in Java – uno dei linguaggi più usati al mondo, inventato dal canadese James Gosling nel 1994 – , dove ancora più informazioni sono necessarie per dichiarare l’usabilità del codice in contesti più grandi (public, static), o il riuso di librerie esistenti (System.out):</p>
<pre>
   public class Hello {
       public static void main(String[] args) {
           System.out.println("Hello, world");
      }
   }
</pre>
<p>Invece nei linguaggi interpretati, come in Perl – creato dallo statunitense Larry Wall nel 1987 con lo scopo di essere pratico, più che elegante – di solito non è necessaria tutta questa sovrastruttura, ma è sufficiente scrivere una sola istruzione, che in memoria dei tempi in cui terminali erano della stampanti si chiama “print”:</p>
<pre>
   print "Hello, world\n"
</pre>
<p>Insomma: c’è più di un modo di fare la stessa cosa. Che è anche il motto del Perl: TIMTOWTDI (<em>There Is More Than One Way To Do It</em>). A dimostrazione che la programmazione non è un’attività ripetitiva, ma un’arte.</p>
<p>Questo “task” permette anche di far vedere come vengono trattate le sequenze di simboli, che è una parte fondamentale di tutti i linguaggi di programmazione, e come può essere gestito l’output verso lo schermo o verso altri dispositivi.</p>
<p>Per spiegare il versante più algoritmico dei linguaggi di solito si usano altri compiti, legati al mondo dei numeri, come quello di calcolare l’i-esimo numero di Fibonacci; oppure quello di scrivere tutto il testo della canzoncina da scout “<a rel="noreferrer noopener" href="http://99-bottles-of-beer.net/" target="_blank">99 bottles of beer</a>“, tenendo conto delle varianti, con il minore numero di istruzioni.</p>
<p>Perché proprio “Hello, world!” e non “123 prova” o “tanto va la gatta al lardo”?</p>
<p>Non è affatto un testo casuale. C’è dietro una storia, che non è nota a tutti, anche tra i programmatori.</p>
<p>Nel 1972 Brian Kernighan, un fisico canadese che lavorava ai Laboratori Bell, si trovò a scrivere un tutorial sul linguaggio B, inventato proprio lì.</p>
<p>Nei primi capitoli del tutorial, dopo aver presentato gli operatori aritmetici, passa alla funzione “putchar” che scrive sul terminale l’argomento passato, in questo caso una costante:</p>
<pre>
 main( ){
	auto a;
	a= ’hi!’;
	putchar(a);
	putchar(’*n’ );
 }
</pre>
<p>Il valore della costante è in questo caso ‘hi!’, ciao.</p>
<p>Nell’ultima riga viene dimostrato come facilitare la formattazione usando dei codici particolari: ‘*n’ non sono due lettere da stampare, ma un codice unico che indica alla funzione putchar () che al termine deve aggiungere un ritorno a capo.</p>
<p>Poco più avanti, volendo mostrare come si creano e usano le variabili comuni, Kernighan ha bisogno di una frase più lunga, e abbastanza naturalmente da ‘hi!’ passa a ‘hello, world!’:</p>
<pre>
 main( ) {
    extern a, b, c;
    putchar(a); putchar(b); putchar(c); putchar('!*n');
 }

 a 'hell';
 b 'o, w';
 c 'orld';
</pre>
<p>Il motivo per cui ci sono tre variabili anziché una sola è che il massimo numero di caratteri che una costante nel linguaggio B può contenere era 4.</p>
<p>Ancora più avanti la frase “hello, world” viene riusata per introdurre le stringhe, che invece possono essere più lunghe di 4 caratteri.</p>
<p>Sei anni dopo, lo stesso Kernighan riusa esattamente la stessa frase quando si trova a scrivere il manuale del linguaggio C (che era un erede del B):</p>
<pre>
 main( ) {
        printf("hello, world\n");
 }
</pre>
<p>In questa versione scompare il punto esclamativo (probabilmente perché ha un significato preciso nel linguaggio: è un operatore unario, una negazione, e lo studente potrebbe esserne confuso).</p>
<img src="https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Hello_World_Brian_Kernighan_1978.jpg" alt="" class="wp-image-895" srcset="https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Hello_World_Brian_Kernighan_1978.jpg 584w, https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Hello_World_Brian_Kernighan_1978-274x300.jpg 274w" sizes="(max-width: 456px) 100vw, 456px" width="456" height="500" title="">
<p>Perché sceglie “hello, world”? Evidentemente questa frase faceva parte della cultura popolare statunitense, tanto che in un’intervista di quarant’anni dopo Kernighan sostiene di non ricordare esattamente perché l’ha scelta, ma di avere in mente un cartone animato con un pulcino che dice “Hello, world”. In ogni caso, questa frase era usata negli anni cinquanta da uno speaker radiofonico, William B. Williams, come suo saluto (un po’ come “Good morning, Vietnam!” o “Cari amici vicini e lontani”). Era quindi un saluto, un’espressione orale, colloquiale. Un buongiorno, la prima frase che si dice quando si incontrano delle persone.Siccome il manuale di Kernighan è stato tradotto in 20 lingue ed è considerato unanimemente “il” riferimento per il linguaggio C, la versione “hello, world” divenne quella più conosciuta da migliaia o milioni di studenti e apprendisti programmatori. </p>
<p>Talmente famosa da diventare un oggetto artistico: la versione scritta a mano del codice è stata battuta ad un’asta nel 2015 per 5.000 dollari.</p>
<p></p>
<p>Da quel momento, l’uso di HW come task per introdurre un linguaggio è diventato una specie di standard de facto, un omaggio al lavoro dello stesso Kerninghan, e implicitamente a quello di Dennis Ritchie (l’autore del linguaggio C) e di Ken Thomson (l’autore del linguaggio B).</p>
<p>L’omaggio non può che essere piuttosto rigido, nel senso che sarebbe considerato di pessimo gusto utilizzare come primo esempio nel manuale di un nuovo linguaggio un codice sorgente che stampi “Hey Jude”.</p>
<p>Talmente è diffusa questa tradizione che qualcuno ha pensato di raccogliere esempi di programmi HW scritti in centinaia di linguaggi di programmazione diversi, come ha fatto Wolfram Rösler a partire dal 1994:</p>
<p><a href="http://helloworldcollection.de/" target="_blank" rel="noreferrer noopener">http://helloworldcollection.de/</a></p>
<p>La prima versione dei codici si atteneva ad un singolo modello (“Hello World!”), in cui anche la parola World, essendo un sostantivo, viene scritta in maiuscolo come è corretto fare in lingua tedesca da Lutero in poi. Mano a mano che la raccolta è cresciuta – 603 linguaggi censiti oggi – i codici si sono allontanati dal testo originale. Per essere esatti, la tradizione consente queste sole varianti al testo originale:</p>
<ul>
<li>si usa la H maiuscola (corretta in Inglese ad inizio di frase)</li>
<li>si può usare la w o la W (benché la maiuscola sia un errore in Inglese a meno che World non si intenda come nome proprio)</li>
<li>si può omettere la virgola (che nell’originale serviva a mostrare l’uso dei segni di interpunzione all’interno delle costanti e fuori)</li>
<li>si può omettere il punto esclamativo finale, in omaggio alla versione in C</p>
<li>si può omettere l’acapo finale (\n)</li>
</ul>
<p>Ma a dimostrare la posizione particolare di HW all’interno dell’universo culturale dei programmatori non ci sono solo le sillogi di codice.</p>
<p>Il maestro Nicola Campogrande per l’apertura di <a rel="noreferrer noopener" href="https://www.stefanopenge.it/codefest" target="_blank">Codefest 2021</a>, il festival del codice sorgente organizzato dall’Università di Torino e da <a rel="noreferrer noopener" href="http://codexpo.org" target="_blank">Codexpo.org</a>, ha composto e diretto quattro lieder sul testo HW, scegliendo quattro linguaggi tra quelli proposti in helloworld.de. E’ un caso lampante di uso del codice sorgente al di là della sua funzione primaria. D’altra parte, anche Franco Berardi (Bifo) nel 2001 aveva effettuato una performance singolare leggendo a voce alta il codice sorgente di un virus scritto per l’occasione dal collettivo [epidemiC]. In fondo, anche le partiture musicali e le ricette si possono leggere a voce alta e, perché no, cantare. D’altra parte, ci sono stati casi di <em>poesie</em> scritte in linguaggi di programmazione, da quelle in ALGOL a quelle in Perl.</p>
<p>Un altro omaggio dal mondo esterno è quello di Tomohiko Itō che nel 2019 ha diretto un anime dal titolo originale  “Harō Wārudo” , che significa proprio quello che pensate. In questo mix di fantascienza e sentimenti, il mondo viene registrato in un supercomputer dal nome evocativo, <em>Alltales</em>.</p>
<p>Ma per tornare nel campo della molteplicità dei linguaggi di programmazione, che è facile definire come nuova Babele visto che ce ne sono 8000, ci sono <em>artisti del codice</em> che hanno dato vita a veri pezzi di bravura, come questo HW che può essere correttamente compilato/eseguito in 8 linguaggi diversi e produce lo stesso risultato:</p>
<p><a href="https://ideology.com.au/polyglot/polyglot.txt" target="_blank" rel="noreferrer noopener">https://ideology.com.au/polyglot/polyglot.txt</a></p>
<p>Per divertirsi un po’ – non solo se si è programmatori – si può andare a leggere questo testo riportato sul sito della Free Software Foundation, in cui vengono raccolte e presentate sedici maniere diverse di scrivere HW, organizzate in base all’età, alla competenza professionale e al ruolo dell’autore, utilizzando linguaggi diversi (BASIC, Pascal, Lisp, C, C++, Perl,…) e contemporaneamente prendendo in giro le caratteristiche di ogni figura: dal giovane programmatore che vuole impressionare il datore di lavoro all’hacker che ricompila un codice già scritto, al guru che usa meno caratteri possibili, al manager che scrive una mail per farsi fare il lavoro da un sottoposto fino al CE che non è in grado di fare nemmeno quello.</p>
<p><a href="https://www.gnu.org/fun/jokes/helloworld.html" target="_blank" rel="noreferrer noopener">https://www.gnu.org/fun/jokes/helloworld.html</a></p>
<p>Un’altra magnifica prova di umorismo è quella di Benno Evers, un programmatore di Amburgo, che descrive dai diversi punti di vista di un novizio, un apprendista, un avvocato, un pedante, un idealista, un ideologo, un ingegnere, un fisico e un illuminato cosa succede quando viene eseguito una variante in C++ di HW:</p>
<pre>
 #include &lt;iostream&gt;
 int main() {
    std::cout &lt;&lt; "Hello, world!" &lt;&lt; std::endl;
 }
</pre>
<p><a href="https://gist.github.com/lava/f8732a6802988fe8d0a41bd7979a4d54" target="_blank" rel="noreferrer noopener">https://gist.github.com/lava/f8732a6802988fe8d0a41bd7979a4d54</a></p>
<p>Il codice sorgente dovrebbe essere sempre leggibile, per permettere ad altri di imparare e correggere. Ma siccome i programmatori sono tendenzialmente dei <em>nerd</em> e tendono a sfidarsi sul terreno della bravura, a volte si divertono a scrivere codice illeggibile solo per il gusto di far vedere che sono capaci di farlo. Non tutti sanno che ogni anno, dal 1984, si tiene una gara di abilità tra programmatori C in cui vengono premiati i programmi <em>meno leggibili:</em> si tratta dell’International Obfuscated C Code Context. Uno dei codici vincitori della prima edizione era appunto un HW (che usa la versione originale del testo):</p>
<pre>
 int i;main(){for(;i["]&lt;i;++i){--i;}"];read('-'-'-',i+++"hello, wor\
 ld!\n",'/'/'/'));}read(j,i,p){write(j/p+p,i---j,i/i);}
</pre>
<p>Ma non è l’unico caso: ci sono state delle sfide aperte, come quella  su codegolf.stackexchange.com che invitava a scrivere un HW senza usare nessuno dei caratteri seguenti:</p>
<p>h, l, w, d , e, o, r, 0, 1, 2 e 7</p>
<p>Per quanto possa sembrare strana, questa è una delle soluzioni  (in Javascript) che fa anche un occhiolino all’ASCII Art: </p>
<img loading="lazy" src="https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Schermata-da-2021-06-15-23-01-27.png" alt="" class="wp-image-891" srcset="https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Schermata-da-2021-06-15-23-01-27.png 574w, https://www.stefanopenge.it/wp/wp-content/uploads/2021/06/Schermata-da-2021-06-15-23-01-27-300x275.png 300w" sizes="(max-width: 574px) 100vw, 574px" width="574" height="526" title="https://codegolf.stackexchange.com/questions/307/obfuscated-hello-world">
<p>Infine, HW è entrato anche nel mondo dei linguaggi (non solo dei codici sorgenti) dal 2001, l’anno in cui Cliff Biffle ha inventato <codeshow>HQ9+</codeshow>.
<a href="#top">Torna su</a>
</p>

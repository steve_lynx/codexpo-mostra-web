<?php
// You may need to  edit these constants when deploying 
define ('APACHE_DIR', '/home/museumq/www/');
define ('APP_NAME', 'codeshow');
define ('HTTP_ROOT', 'https://www.museumofcode.net');

// Relative paths
define ('ADA_FMW_DIR', 'vendor/adafmw');
define ('HTML_DIR', 'html');
define ('DOCS_DIR', 'doc');
define ('UPLOADS_DIR',  'uploads');	
define ('CACHE_DIR',  "cache");
define ('INDEX_DIR',  "config/json"); // could be XML or YAML

// Don't change these constants if you don't know exactly what you are doing
// Shortcuts
define ('HTTP_ROOT_DIR', HTTP_ROOT.'/'.APP_NAME);
define ('ROOT', '/'.APACHE_DIR.'/'.APP_NAME);
define ('ROOT_DIR', ROOT.'/'.ADA_FMW_DIR);
define ('HTTP_LAYOUT_DIR',  HTTP_ROOT_DIR.'/'.ADA_FMW_DIR);

// API
define('API_ROOT',HTTP_ROOT_DIR.'/API/v1');

// Classes
define ('APP_CLASSES_DIR', '/'.APACHE_DIR.'/'.APP_NAME.'/src/');
define ('ADAFMW_CLASSES_DIR', '/'.APACHE_DIR.'/'.APP_NAME.'/'.ADA_FMW_DIR.'/src/');

// define ('ACTIONS_DIR',  '/'.APACHE_DIR.'/'.APP_NAME.'/src/');	
//define ('EXPLORE_ACTIONS_DIR',  ACTIONS_DIR);
//define ('RESERVED_ACTIONS_DIR',  ACTIONS_DIR);

// Home
define ('HOME', HTTP_ROOT_DIR."/index.php");

// Index:
define ('INDEX_FILE',INDEX_DIR.'/mostra.json');
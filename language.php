<?php

/* Codexpo 
 * language.php
 * 
 * 
 * Copyright 2021 stefano <steve@lynxlab.com>
 * 
 */
 

session_start();
// Configuration files are included from a single meta-config file
require 'config/main_config.php'; 

// Display errors
if (DEVELOP_MODE){
    ini_set('display_errors',  "1"); 
    error_reporting(E_ALL & ~E_NOTICE);	
} else  {
    ini_set('display_errors',  "0"); 
    error_reporting(E_ERROR);			
} 
// Autoload classes
require 'autoload.php';
    
// Create the Config Manager object so we can use properties (settable at runtime)) instead of constants
$definedConstantsAr = get_defined_constants(true); 
$configManager = new \Tools\ConfigManager($definedConstantsAr['user']);

if ((isset($_GET['language'])) AND (!is_null($_GET['language']))){
    $_SESSION['language'] = $_GET['language'];
}
header("Location: ".HTTP_ROOT_DIR);
exit;

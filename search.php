<?php

/* Codexpo 
 * search.php
 * 
 * 
 * Copyright 2021 stefano <steve@lynxlab.com>
 * 
 */
?> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="Stefano" >
        <title>Search</title>
    </head>
    <style>
        *, *::before, *::after {box-sizing: border-box;}
        body {
            background-color: black;color: white;
        }
        input {
            border: none;
            -webkit-appearance: none;
            -ms-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: #f2f2f2;
            padding: 1em;
            margin-top: 1em;
            border-radius: 3px;
        }

    </style>
    <body>
    <h1>CodeSHOW API Demo</h1>
		<h2>Ricerca</h2>
			<p>Questa pagina dimostrativa ha lo scopo di esemplificare l'uso delle web API di <ahref='https://www.codeshow.it>CodeSHOW</a> per interagire da remoto con la Mostra.<br>
			In questo caso, la ricerca del termine inserito nella form di ricerca viene effettuata chiamando il metodo /API/v1/Find, il quale cerca la parola in un indice costruito a partire da tutti i pannelli della Mostra.<br>

<?php
 session_start();
 // Configuration files are included from a single meta-config file
 require 'config/main_config.php'; 
 
 // Display errors
 if (DEVELOP_MODE){
     ini_set('display_errors',  "1"); 
     error_reporting(E_ALL & ~E_NOTICE);	
 } else  {
     ini_set('display_errors',  "0"); 
     error_reporting(E_ERROR);			
 } 
  // Autoload classes
    require 'autoload.php';
      
    // Create the Config Manager object so we can use properties (settable at runtime)) instead of constants
    $definedConstantsAr = get_defined_constants(true); 
    $configManager = new \Tools\ConfigManager($definedConstantsAr['user']);
 
    
if (isset($_POST['term'])){
    $method = '/Find/'.$_POST['term'];
    $APIURL = API_ROOT;
    $response = json_decode(file_get_contents($APIURL.$method),true); 
    if ((!isset($response['error']))AND (!is_null($response['results']))){
        $output = '<p>Risultati:</p>';
        $output.= '<ul>';
        foreach($response['results'] as $result){
        		$result = substr($result,0,-5);// strips off .html
            $fileUrl = explode('/',$result); 
            $room = $fileUrl[count($fileUrl)-2];
            $panel = $fileUrl[count($fileUrl)-1]; 
            $output.= "<li><a href='$result'>Stanza: $room Panello: $panel</a></li>\n";
        }
        $output.= '</ul>';
    } else  
        $output= \Tools\Utils::translate("Termine non trovato");
    
    echo $output;    
        
} elseif (isset($_GET['key'])) {
       
    $routing = $GLOBALS['configManager']->getConfigValue('routing');
    // setup  
    $indexFile =  $configManager->getConfigValue('index_file');
    $myExpo = new \Expo\Expo($indexFile,$language);
    $myPanel = new \Expo\Panel($myExpo->index,'','');
    $room = $_SESSION['room'];
    
    $panelOptions = "";
    $found = $myPanel->findRoomAndPanels($_GET['key']);
    if (is_array($found)){
        $room = $found[0];
        if (!is_null($found[1])){
            $panel = $found[1];
            $panelOptions = "&panel=$panel";
        }
    }

    if ($routing == TRUE)
        header("Location: $room/$panel");
    else
        header("Location: index.php?room=$room$panelOptions");
    exit;
}
?>
<form method="post" enctype="application/x-www-form-urlencoded" target="_self">
   <input type ='text' id='term' name='term' placeholder='Parola da cercare' autofocus minlength="5">
   <input type="submit"> 
</form>
    </body>
</html>


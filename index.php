<?php
/*
* @name index
* @package Codexpo
* @author Stefano Penge
* @copyright Codexpo.org
*
*/

use API\v1\Search;
// use Tools;
// use Expo;
// use View;

/*
 *  1. SETUP
 *
 * */

session_start();

// All configuration files are included from a single meta-config file
require_once 'config/main_config.php';

// Display errors
if (DEVELOP_MODE == TRUE){
    ini_set('display_errors',  "1");
    error_reporting(E_ALL & ~E_NOTICE);
} else  {
    ini_set('display_errors',  "0");
    error_reporting(E_ERROR);
}


// Autoload classes
// ADA FMW
require 'autoload.php';

// DOMPDF
require ROOT.'/vendor/dompdf/autoload.inc.php';


// Load configuration data

// Create the Config Manager object so we can use properties (settable at runtime) instead of constants if we need to
$definedConstantsAr = get_defined_constants(true);
$configManager = new Tools\ConfigManager($definedConstantsAr['user']);
// We get ALL constants as variable using:
extract ($configManager->config);


// Index: a JSON or an XML file
$indexFile =  $configManager->getConfigValue('index_file');
// Source from Git repo?
$git_enabled = $configManager->getConfigValue('git_enabled');
$git_path = $configManager->getConfigValue('git_path');

$layout =  $configManager->getConfigValue('layout');
$template = $configManager->getConfigValue('template');

if (isset($_SESSION['language'])) 
	$language = $_SESSION['language'];  // overriding main config value

$myTranslator = new \Tools\Translator($language);
// END OF SETUP

// Special cases:
	// 1. static index

$path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
if ($path == "/") {
	$static_page = 'index-'.$language.'.html';
	header("Location: $http_root_dir/$static_page",307);
	exit();
}

	// 2. maintenance 
if ($maintenance_mode){
	$self = 'index';
	$panel = $default_panel;

	$htmlObj = new View\Page($self, $layout, $template, $panel);
	$fields_array = $htmlObj->getFieldNames();
	if (!empty($fields_array)){
		$page_data = compact($fields_array);
	}
	$page_data['field_body'] = $maintenance_mode_message;
	$htmlObj->setBody($page_data);
	$htmlObj->output('page');
	exit();
}

/*************
 * 2. CONTENT
 *
 * */
$myExpo = new Expo\Expo($indexFile,$language);
$myRoom = new Expo\Room($myExpo->index);
$myPanel = new Expo\Panel($myExpo->index);

// Routing (if active: codexpo.org/languages/Java)
$myRoute = new Tools\Router($myExpo);

// $mode = $myRoute->getMode();
$routeError = $myRoute->getError();
$routes = $myRoute->getRoute();
$altLayout =  $myRoute->getLayout();
$currentRoomName =  $myRoute->getRoom();
$currentRoomId =  $myRoute->getRoomId();
$currentPanel = $myRoute->getPanel();
if ($currentPanel == '')
	$currentPanel = $currentRoomName;
// $field_content_header = $myRoute->getRoomTitle();
$output_mode = $myRoute->getOutputMode();
$parameter =  $myRoute->getParameter();
// $value = $myRoute->getValue(); not used
$action = $myRoute->getAction();
$format = $myRoute->getFormat();
$mode = $myRoute->getMode();

$_SESSION['room'] = $currentRoomName;
$_SESSION['panel'] = $currentPanel;


// Log

Tools\Log::doLog(LOG_ACCESS,$currentRoomName,$currentPanel);


if ($mode == 'API'){

	if ($routeError!=NULL){

		$actionClass = "API\\v1\\API";
		$actObj = new $actionClass();
		$actObj->setError($routeError);
		$body = $actObj->formatError();
	} else {
		$actionClass = "API\\v1\\".ucfirst($action);
		$actObj = new $actionClass($action);
		$body = $actObj->getData($format,$currentRoomName,$currentPanel); // using main method getData()
	}
	exit();
}


// Cache

$cacheObj = new Tools\CacheManager();

if (@$cacheObj->getCachedData()){
	exit();
}

// Dynamic page content

// URL:
$field_url =  HTTP_ROOT_DIR.'/'.$currentRoomName.'/'.$currentPanel;


// Image:
$field_image = $myExpo->getImageElement($currentRoomName,$currentPanel);

// Text:
$field_body = $myPanel->getText($currentRoomName,$currentPanel);

// Timeline

$tlObj = new Expo\Timeline($myExpo, $currentRoomName,$currentPanel);
$field_timeline = $tlObj->getTimeline();


// Geo map

$geoObj = new Expo\Geo($myExpo,$currentRoomName,$currentPanel);
$field_geo = $geoObj->getGeo('small');

// Header

$field_content_header =  $myTranslator->getTranslation($currentPanel);

/*************
 *  3. NAVIGATION
 *
 */
// Navigation
// We have THREE navigation tools: Menu, Map and Arrows
// and we can mix them

$menuMode = Array('Menu','Map','Arrows');
//$menuMode = Array('Menu','Arrows');

$myNav = new Expo\Navigator($myExpo);
$myMap = new Expo\Map($myExpo,$currentRoomName,'table'); // could also be 'div'

// Menu (also for mobile style!)

if (in_array('Menu',$menuMode))
	$menu = $myNav->MainMenu($currentRoomName,0);

// Map (=Elevator buttons)
if (in_array('Map',$menuMode))
	$map = $myMap->getTable();

// Arrows
if (in_array('Arrows',$menuMode))
	$arrows = $myNav->getDirections($myRoom->findRoomLinks($currentRoomId),$currentRoomName);



$field_rooms_header = $myTranslator->getTranslation("Stanze");
$field_rooms = $map;

$field_nav = $menu; 
$field_directions = "";

$field_arrows = $arrows;

// Panels or search results:
if (isset($_POST['key'])){
	$field_panels_header =$myTranslator->getTranslation("Risultati");
	$field_panels = $myExpo->getSearchresults($_POST['key']);
} else {
	$field_panels_header =$myTranslator->getTranslation("Pannelli");
	$field_panels = $myNav->getPanels($myPanel->findPanels($currentRoomId),$currentRoomName,$currentPanel);
}

$nextPanelLink = $myPanel->getNextPanelLink($currentRoomId, $currentPanel);
$field_body.= $nextPanelLink;

// PDF version for printing
$field_print =$myTranslator->getTranslation("Scarica in ")."<a href='$http_root_dir/$currentRoomName/$currentPanel/pdf'>PDF</a><br>";

// Version
$field_file_version = $myPanel->getFileVersion()."<br>";

/*
// Dimension ?
$field_textDimension =$myTranslator->getTranslation("Parole:").' '.$myExpo->getTextDimension($body)."<br>";
$field_file_version.=$textDimension;
*/

// Random panels
$formatString =$myTranslator->getTranslation("Ci sono %d stanze e %d pannelli");
$field_randomPanelsTitle = sprintf($formatString,$myExpo->roomsCount,$myExpo->panelsCount)."<br>";
$field_randomPanelsTitle.=$myTranslator->getTranslation("Visita una stanza a caso...");
$field_randomPanels = $myPanel->getRandomPanels(8);

// Search form:
$field_searchForm = $myExpo->getSearchForm($http_root_dir);

// Change language link:
$field_changeLanguage = "<h3>".$myTranslator->getTranslation("Scegli una lingua:")."</h3>".$myExpo->getChangeLanguageLink();

// home link
$field_home = $home;

/*************
 *  4. VIEW
 *
 */

// Layout
$self = 'index';

switch($output_mode) {
	case 'pdf':
		$layout =  $configManager->getConfigValue('layout-pdf'); // could be different: eg colors.
		$template = $configManager->getConfigValue('template-pdf');
		break;
	case 'html':
	default:
		if  ($altLayout == ''){
			if (isset($_SESSION['layout']))
				$layout = $_SESSION['layout'];
			else
				$layout =  $configManager->getConfigValue('layout');
		} else {
			$layout =  $altLayout ;
			$_SESSION['layout'] = $layout;
		}
}


$field_layout = $layout;
$field_template = $template;
$field_title = $title;
$field_subtitle = $subtitle;
$field_copy = "<p>".COPY."</p>";
// $field_content_header = "$currentRoomName::$currentPanel";

// "Automagic" mapping: fields name in template are the same as variable names
$htmlObj = new View\Page($self, $layout, $template);
$fields_array = $htmlObj->getFieldNames();
$page_data = @compact($fields_array);

$htmlObj->setBody($page_data);

// Rendering:
switch($output_mode) {
	case 'pdf':
		// check if file already exist // FIXME: should be a class like cache manager
		$filename = "$currentRoomName-$currentPanel.pdf";
		$path_to_pdf = "$root/$pdf_cache_dir/$filename";
		if ((file_exists($path_to_pdf)) AND	((time()-filemtime($path_to_pdf))<IC_LIFE_TIME)){
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Accept-Ranges: bytes');
			@readfile($path_to_pdf);
		} else
			$htmlObj->output('pdf',$filename);
		break;
	case 'html':
	default:
		$htmlObj->output('page');
		$cacheObj->writeCachedData($htmlObj->output('file'));
}

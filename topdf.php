<?php
/*
* @name topdf
* @package Codexpo
* @author Stefano Penge
* @copyright Codexpo.org
* 
*/ 
$path = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH); 
if ($path == "/") {
	header("Location: https://www.codeshow.it/index.html",307);
	exit();
}

/*
 *  1. SETUP 
 * 
 * */

session_start();

// All configuration files are included from a single meta-config file
require 'config/main_config.php'; 

// Display errors
if (DEVELOP_MODE){
    ini_set('display_errors',  "1"); 
    error_reporting(E_ALL & ~E_NOTICE);	
} else  {
    ini_set('display_errors',  "0"); 
    error_reporting(E_ERROR);			
} 


// Autoload classes
require 'autoload.php';
require ROOT.'/vendor/dompdf/autoload.inc.php';


// Load configuration data

// Create the Config Manager object so we can use properties (settable at runtime) instead of constants if we need to
$definedConstantsAr = get_defined_constants(true); 
$configManager = new \Tools\ConfigManager($definedConstantsAr['user']);
// We can get ALL constants as variable using:
extract ($configManager->config);

// END OF SETUP


/*************
 * 2. CONTENT
 * 
 * */


if ($maintenance_mode){
	$self = 'index'; 
	$panel = $default_panel;
	$htmlObj = new View\Page($self, $layout, $template, $panel);

	$fields_array = $htmlObj->getFieldNames();
	if (!empty($fields_array)){
		$page_data = compact($fields_array);
	}
	$page_data['body'] = $maintenance_mode_message;
	$htmlObj->setBody($page_data);
	$htmlObj->output('page');
	exit();
}
 
// Index:a JSON or an XML file 
$indexFile =  $configManager->getConfigValue('index_file');
if (isset($_SESSION['language'])) 
	$language = $_SESSION['language'];  // overriding main config value
$myExpo = new \Expo\Expo($indexFile,$language);

// Source from Git repo?
$git_enabled = $configManager->getConfigValue('git_enabled');
$git_path = $configManager->getConfigValue('git_path');

// Routing (if active: codexpo.org/languages/Java)
$myRoute = new \Tools\Router($myExpo);

// $format = $myRoute->getFormat(); 
// $mode = $myRoute->getMode();
$routes = $myRoute->getRoute(); 
$currentRoomName = 'Ingresso';// $myRoute->getRoom();
$currentRoomId =  $myRoute->getRoomId();
$currentPanel = 'Ingresso'; //$myRoute->getPanel();
$content_header =  'Ingresso';// $myRoute->getRoomTitle();

$_SESSION['room'] = $currentRoomName;
$_SESSION['panel'] = $currentPanel;


// Log

\Tools\Log::doLog(LOG_ACCESS,$currentRoomName,$currentPanel);

// Cache??
/*

$cacheObj = new \Tools\CacheManager(); 

if (@$cacheObj->getCachedData()){
	exit();
} 
*/  



/*	CONTENT */
// Fixed page content:
$credits = file_get_contents(ROOT."/$html_dir/$language/credits.html");

// Dynamic page content
// Images: 
$image = $myExpo->getImageElement($currentRoomName,$currentPanel);

// Text:
$body = $myPanel->getText($currentRoomName,$currentPanel);


// END OF CONTENT



/*************
 *  4. VIEW 
 * 
 */

// Layout
$self = 'index';

$layout =  $configManager->getConfigValue('layout'); 
$template = $configManager->getConfigValue('template-pdf'); 
$htmlObj = new View\Page($self, $layout, $template);

// "Automagic" mapping: fields name in template are the same as variable names, so we have to add some 


//$url = $_SERVER['QUERY_STRING'];



$fields_array = $htmlObj->getFieldNames();
if (!empty($fields_array)){
	$page_data = compact($fields_array);
}	     


$htmlObj->setBody($page_data);

// Rendering: 

$htmlObj->output('pdf');

//$cacheObj->writeCachedData($htmlObj->output('file'));

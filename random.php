<?php

/* Codexpo 
 * random.php
 * 
 * 
 * Copyright 2021 stefano <steve@lynxlab.com>
 * 
 */
 

session_start();
// Configuration files are included from a single meta-config file
require 'config/main_config.php'; 

// Display errors
if (DEVELOP_MODE){
    ini_set('display_errors',  "1"); 
    error_reporting(E_ALL & ~E_NOTICE);	
} else  {
    ini_set('display_errors',  "0"); 
    error_reporting(E_ERROR);			
} 


// Autoload classes
require 'autoload.php';


// Create the Config Manager object so we can use properties (settable at runtime)) instead of constants
$definedConstantsAr = get_defined_constants(true); 
$configManager = new \Tools\ConfigManager($definedConstantsAr['user']);
$routing = $GLOBALS['configManager']->getConfigValue('routing');
// setup  
$indexFile =  $configManager->getConfigValue('index_file');
$myExpo = new \Expo\Expo($indexFile,$language);
$myPanel = new \Expo\Panel($myExpo->index);
$randomRoomAndPanel = $myPanel->getRandomPanel();
if ($routing == TRUE)
    header("Location: $randomRoomAndPanel");
else{
    $roomAndPanel = explode($randomRoomAndPanel,'/');
    $room = $roomAndPanel[0];
    $panel = $roomAndPanel[1];
    $panelOptions = "room=$room&panel=$panel";
    header("Location: index.php?$panelOptions");
}


exit;

